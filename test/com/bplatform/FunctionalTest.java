/**
 * 
 */
package com.bplatform;


import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.bplatform.server.DAO.APIs.BpTreeAPI;
import com.bplatform.server.DAO.models.BpTree;



/**
 * @author VolovodA
 * 
 */
public class FunctionalTest {

	private BpTreeAPI f;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
    
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws SQLException {
		f = new BpTreeAPI();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createTreeTest() {

		try {

			f.createTree("t1");
		} catch (Exception e) {
			fail("createTree failed");
		}

	}

	@Test
	public void deleteTreeTest() {
		try {
			f.deleteTree("t1");
		} catch (Exception e) {
			fail("deleteTree failed");
		}

	}

	@Test
	public void getListOfTreesTest() {
		List<String> listOfNames = f.getListOfTrees("");
		for (String name : listOfNames) {
			System.out.println(name);
		}
	}

	@Test
	public void addToTreeTest() {
		String id = f.addToTree("", "name1", "0", "", "");
		// and it
		assertTrue(true);
		// end of change
	}

	@Test
	public void setToTreeTest() {

	}

	@Test
	public void getFromTreeByListTest() {
		try {
			LinkedList<Long> list = new LinkedList<>();
			list.add(3l);
			list.add(2l);
			list.add(4l);
			list.add(1l);
			List<BpTree> res = f.getFromTree("", list);
			for(BpTree node : res){
				if(!list.contains(node.getId())){
					fail("getFromTree by list failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("getFromTree by list failed");
		}

	}
	
	@Test
	public void getFromTreeByTCodeTest() {
		try {
			String tCode = "2";
			List<BpTree> res = f.getFromTree("", tCode);
			for(BpTree node : res){
				if( !( (tCode == null && node.getTCode() == null) || (tCode.equalsIgnoreCase(node.getTCode() ))) ){
					fail("getFromTree by tCode failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("getFromTree by tCode failed");
		}
	}

	
	@Test
	public void setToTreeByTree(){
		try{
			BpTree tree = new BpTree("3", "some name", "0", "tCode", "descr");
			BpTree res = f.setToTree("", tree);
			if(!tree.equals(res)){
				fail("setToTree by tree failed");
			}
		}catch(Exception e){
			e.printStackTrace();
			fail("setToTree by tree failed");
		}
	}
	
	@Test
	public void setToTreeByParametrs(){
		try{
			BpTree tree = new BpTree("4", "some name", "0", "tCode", "descr");
			BpTree res = f.setToTree("", tree.getId(), tree.getName(), tree.getParentId(), tree.getTCode(), tree.getConnectedTo());
			if(!tree.equals(res)){
				fail("setToTree by tree failed");
			}
		}catch(Exception e){
			e.printStackTrace();
			fail("setToTree by tree failed");
		}
	}
	
}