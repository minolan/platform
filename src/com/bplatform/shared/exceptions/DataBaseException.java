package com.bplatform.shared.exceptions;

import java.io.Serializable;

/**
 * 
 * @author Anastasia Smakovska
 *
 * 12 ����. 2013
 *
 *	This exception should be thrown to user when something unpredictable goes wrong with persistence
 */

public class DataBaseException extends RuntimeException implements Serializable{
	private static final long serialVersionUID = -5091913231873691000L;
	
	public DataBaseException(String message){
		this.message = message;
	}
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
