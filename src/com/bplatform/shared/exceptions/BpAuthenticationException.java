package com.bplatform.shared.exceptions;

public class BpAuthenticationException extends RuntimeException{
	
	private static final long serialVersionUID = -7074164201597516423l;
	
	public Reason getReason(){
		return reason;
	}

	public BpAuthenticationException(Reason reason) {
		this.reason = reason;
	}
	
	private Reason reason;
	
	public enum Reason{
		LOGIN_MUST_BE_SET,
		PASSWORD_MUST_BE_SET,
		INVALID_LOGIN,
		INVALID_PASSWORD,
		INCORRECT_LOGIN,
		INCORRECT_PASSWORD, 
		UNRESOLVED
	}
	
}
