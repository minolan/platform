package com.bplatform.shared.exceptions;

/**
 * @author RomanL
 *
 */
public class DuplicateNameException extends Exception{
	
	public DuplicateNameException() {
		super();
	}

	public DuplicateNameException(String s) {
		super(s);
	}

	private static final long serialVersionUID = 1L;

}
