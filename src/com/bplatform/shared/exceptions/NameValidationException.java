package com.bplatform.shared.exceptions;

/**
 * @author RomanL
 *
 */
public class NameValidationException extends Exception{
	
	public NameValidationException() {
		super();
	}

	public NameValidationException(String s) {
		super(s);
	}

	private static final long serialVersionUID = 1L;

}
