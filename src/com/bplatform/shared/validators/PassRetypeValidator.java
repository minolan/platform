package com.bplatform.shared.validators;

import java.util.List;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.sencha.gxt.widget.core.client.form.validator.AbstractValidator;

public class PassRetypeValidator extends AbstractValidator<String>{
	
	public PassRetypeValidator() {}
	public PassRetypeValidator(String passRetype) {
		this.passRetype = passRetype;
	}
	
	private String passRetype = null; 
	
	@Override
	public List<EditorError> validate(Editor<String> field, String value) {
		List<EditorError> errors = null;
		if (value != null && (!value.equals(passRetype))) {
			String message = "Passwords not matches!";
			errors = createError(field, message, value);
		}
		return errors;
	}

}
