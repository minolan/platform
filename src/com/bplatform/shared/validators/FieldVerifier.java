package com.bplatform.shared.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * FieldVerifier validates that the name the user enters is valid.
 * </p>
 * <p>
 * This class is in the <code>shared</code> package because we use it in both
 * the client code and on the server. On the client, we verify that the name is
 * valid before sending an RPC request so the user doesn't have to wait for a
 * network round trip to get feedback. On the server, we verify that the name is
 * correct to ensure that the input is correct regardless of where the RPC
 * originates.
 * </p>
 * <p>
 * When creating a class that is used on both the client and the server, be sure
 * that all code is translatable and does not use native JavaScript. Code that
 * is not translatable (such as code that interacts with a database or the file
 * system) cannot be compiled into client side JavaScript. Code that uses native
 * JavaScript (such as Widgets) cannot be run on the server.
 * </p>
 */
public class FieldVerifier {

	/**
	 * Verifies that the specified name is valid for our service.
	 * 
	 * In this example, we only require that the name is at least four
	 * characters. In your application, you can use more complex checks to ensure
	 * that usernames, passwords, email addresses, URLs, and other fields have the
	 * proper syntax.
	 * 
	 * @param name the name to validate
	 * @return true if valid, false if invalid
	 */
//	private static String nameRegex = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+(?:[A-Za-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\\b";
//	private static Pattern namePattern = Pattern.compile(nameRegex);
	
	public static boolean isValidName(String name) {
		if (name == null) {
			return false;
		}
		return name.matches("[-.,_0-9a-zа-яёїіA-ZА-ЯЁЇІ ]{3,30}");
	}
	
	public static boolean isValidLogin(String login) {
		if (login == null) {
			return false;
		}
		return login.matches("[-.,_0-9a-zA-Z ]{3,20}");
	}
	
	public static boolean isValidPassword(String password) {
		if (password == null) {
			return false;
		}
		return (password.matches("^.*(?=.{6,})(?=.*\\d)(?=.*[a-zа-яёїі])(?=.*[A-ZА-ЯЁЇІ])(?=.*[№;:?/!-_*()|@#$%^&+=]).*$") && password.length() < 20) ;
	}
	
	//TODO replace it by standard Hibernate email validator when they implement RFC 822 in stable release
	//Attention: it's not an actual regex, but Java regex, because it contains \\ instead of \.
	private static String emailRegex = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+(?:[A-Za-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\\b";
	private static Pattern emailPattern = Pattern.compile(emailRegex);
	
	public static boolean isValidMail(String email){
		if(email == null){
			return false;
		}
		Matcher matcher = emailPattern.matcher(email);
		return matcher.matches();
	}
	
	
	
}
