package com.bplatform.client.authModule.ui;

import com.bplatform.client.authModule.services.AuthService;
import com.bplatform.client.bpShared.platform.RpcCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.form.TextField;
//import com.bplatform.client.mainModule.ui.SystemProperties;

public class LoginGreeting implements IsWidget {

	private static final int COLUMN_FORM_WIDTH = 680;
	private VerticalPanel vp;
//	private String testName = "TEST_Fail";

	@Override
	public Widget asWidget() {
		if (vp == null) {
			vp = new VerticalPanel();
			vp.setSpacing(100);
			createLogin();
		}
		return vp;
	}

	private void createLogin() {
		FramedPanel panel = new FramedPanel();
		panel.setHeadingText("Please authorize yourself");
		panel.setWidth(COLUMN_FORM_WIDTH / 2);

		HorizontalPanel hp = new HorizontalPanel();

		final TextField login = new TextField();
		login.setAllowBlank(false);

		final TextField pass = new TextField();
		pass.setAllowBlank(false);

		VerticalPanel vpUser = new VerticalPanel();
		vpUser.add(new Label("USER"));
		vpUser.add(login);
		hp.add(vpUser);
		panel.add(hp);

		VerticalPanel vpServer = new VerticalPanel();
		vpServer.add(new Label("PASSWORD"));
		vpServer.add(pass);
		hp.add(vpServer);

		Button ok = new Button("OK");
		ok.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				AuthService.Util.getInstance().userAuth(
						login.getText(), pass.getText(),
						new RpcCallback<String>() {
							@Override
							public void onSuccess(String result) {
								// Window.Location.assign(result);
								Window.open(result, "_self", "");
							}
						});
			}
		});
		panel.addButton(ok);
		Button cancel = new Button("Cancel");
		panel.addButton(cancel);
		Button newUser = new Button("Register");
		panel.addButton(newUser);

		// TEST RPC LOCAL

		//final Button testRPC = new Button(testName);
		/*
		 * testRPC.addClickHandler(new ClickHandler() {
		 * 
		 * @Override public void onClick(ClickEvent event) {
		 * AuthService.Util.getInstance().getLocalPropMap("RU", "test", new
		 * RpcCallback<HashMap<String,String>>(){
		 * 
		 * @Override public void onSuccess(HashMap<String, String> result) {
		 * testName= result.get("test"); testRPC.setText(testName); } });
		 * 
		 * } });
		 */
		//panel.addButton(testRPC);
		
		//Add ACC button
//		SystemProperties acc = new SystemProperties();	
//		panel.addButton(acc.accButton());

		vp.add(panel);

	}

}
