package com.bplatform.client.mainModule.ui;

import java.util.Map;

import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.GroupsService;
import com.bplatform.client.mainModule.services.GroupsServiceAsync;
import com.bplatform.client.mainModule.ui.widgets.RpcWaitingWindow;
import com.bplatform.client.mainModule.ui.resources.UserPropMenuImages;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.dom.ScrollSupport.ScrollMode;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.ContentPanel.ContentPanelAppearance;
import com.sencha.gxt.widget.core.client.Dialog.PredefinedButton;
import com.sencha.gxt.widget.core.client.FramedPanel.FramedPanelAppearance;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent.HideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;

public class UsersManagerComposite extends ResizeComposite {
	
	private UsersListGrid pagingGridExample = new UsersListGrid();
	
	private SplitLayoutPanel splitPanelFull = new SplitLayoutPanel();
	private VerticalLayoutContainer splitPanelLeft = new VerticalLayoutContainer();
	//Temp
	private VerticalLayoutContainer splitPanelRight;
	
	ContentPanel contentPanelTitleLeft = new ContentPanel(GWT.<ContentPanelAppearance> create(FramedPanelAppearance.class));
	ContentPanel contentPanelTitleRight = new ContentPanel(GWT.<ContentPanelAppearance> create(FramedPanelAppearance.class));
	private ScrollPanel scrollPanelLeft = new ScrollPanel();
	
	private TextButton addButton = new TextButton("Add");
	private TextButton refreshButton = new TextButton("Refresh");
	private TextButton removeButton = new TextButton("Remove");
		
	private GroupsServiceAsync groupsPropertiesSvc = GWT
			.create(GroupsService.class);
		
	public UsersManagerComposite() {

		//Fill Left Scroll Panel
		scrollPanelLeft.add(pagingGridExample.asWidget());
		scrollPanelLeft.setHeight("100%");
	
		//Fill Left Content Panel
		contentPanelTitleLeft.add(scrollPanelLeft);
		contentPanelTitleLeft.setHeaderVisible(false);
		// contentPanelTitleLeft.setHeadingText("Groups Tree");
		
		//Fill Left Button Panel 
	    ButtonBar buttonBar = new ButtonBar();
	    buttonBar.setMinButtonWidth(70);
	    buttonBar.getElement().setMargins(0);
	    addButton.setIcon(UserPropMenuImages.INSTANCE.add());
	    refreshButton.setIcon(UserPropMenuImages.INSTANCE.connect());
	    removeButton.setIcon(UserPropMenuImages.INSTANCE.delete());
	    buttonBar.add(addButton);
	    buttonBar.add(removeButton);
	    buttonBar.add(refreshButton);
	    
	    //Fill Left Panel
	    splitPanelLeft.setScrollMode(ScrollMode.NONE);
	    splitPanelLeft.add(buttonBar, new VerticalLayoutData (1, 30));
	    splitPanelLeft.add(contentPanelTitleLeft, new VerticalLayoutData (1, 1));
	    splitPanelLeft.add(pagingGridExample.getToolBar(), new VerticalLayoutData (1, 30));

		//Temp: Fill Right Panel
		// splitPanelRight.add(contentPanelTitleRight, new VerticalLayoutData (1, 1));
		// splitPanelRight.add(buttonBar, new VerticalLayoutData (1, 30));
		
		//Fill Main Panel
		splitPanelFull.setSize("100%", "100%");
//		splitPanelFull.addWest(splitPanelLeft, 235);
//		splitPanelFull.add(splitPanelLeft);
//		splitPanelRight = new VerticalLayoutContainer();
		
		addButton.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				Window userProfileWindow = new UserManagerFormComposite().asWidget();
				userProfileWindow.show();
			}

		});
		
		removeButton.addSelectHandler(new SelectHandler() {
			
			@Override
			public void onSelect(SelectEvent event) {

				if ( true/*checkBoxTree.getSelectedItem() == null*/) {
					
					com.google.gwt.user.client.Window.alert("Please select group!");
					
				} else {
					
				      final ConfirmMessageBox box = new ConfirmMessageBox("Confirm", "Are you sure you want to delete item?");
				      
				      box.addHideHandler(new HideHandler() {
				          public void onHide(HideEvent event) {
				            if (box.getHideButton() == box.getButtonById(PredefinedButton.YES.name())) {
//				            	deleteGroup(checkBoxTree.getSelectedItem().getGroupId());
				            } else if (box.getHideButton() == box.getButtonById(PredefinedButton.NO.name())){
				              // perform NO action
				            }
				          }
				        });
				      
				      box.show();
				      
				}
			}
		});
		
		refreshButton.addSelectHandler(new SelectHandler() {
			
			@Override
			public void onSelect(SelectEvent event) {
				
				startPage();
//				checkBoxTree.refreshStore();
				splitPanelFull.forceLayout();
			}
		});
		
//		splitPanelFull.forceLayout();
		initWidget(splitPanelLeft);
				
	}
	
	/**
	 * GWT delete Group
	 * @param groupId
	 */
	public void deleteGroup (String groupId) {
		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
			
		groupsPropertiesSvc.deleteGroup(groupId,
				new RpcCallback<Map<String, String>>(rpcWait.getBox()) {

					@Override
					public void onSuccess(Map<String, String> result) {
						rpcWait.deleteWindow();	
						if (result.containsKey("ok"))
							{
//									checkBoxTree.getStore().remove(checkBoxTree.getSelectedItem());
//									startPage();
									splitPanelFull.forceLayout();
									com.google.gwt.user.client.Window.alert(result.get("ok"));
							} else if (result.containsKey("no")) {
								com.google.gwt.user.client.Window.alert(result.get("no"));
							} else {
								com.google.gwt.user.client.Window.alert("Some things going wrong!");
							}
					}
				});
	}
	
	public void startPage () {
		ContentPanel startPagePanel = new ContentPanel();
		startPagePanel.setHeadingText("Group properties");
		startPagePanel.setHeaderVisible(false);
		startPagePanel.setSize("100%", "100%");
		HTML text = new HTML("Please, select group");
		text.setHeight("100%");
		startPagePanel.add(text, new MarginData(10));
		
		VerticalLayoutContainer newSplitPanelRight = new VerticalLayoutContainer();
		newSplitPanelRight.add(new HTML(""), new VerticalLayoutData (1, 30));
		newSplitPanelRight.add(startPagePanel, new VerticalLayoutData (1, 1));
		addRightSplitPanel(newSplitPanelRight);
	}
	
	public void addRightSplitPanel (VerticalLayoutContainer newSplitPanelRight) {
		if (splitPanelRight != null) {
			if (splitPanelRight.isAttached()) {
				System.out.println ("isAttached already");
				splitPanelRight.removeFromParent();
			}
			splitPanelRight = newSplitPanelRight;
			splitPanelRight.setScrollMode(ScrollMode.AUTO);
			splitPanelFull.add(splitPanelRight);
		}
	}

}
