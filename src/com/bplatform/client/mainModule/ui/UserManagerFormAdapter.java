package com.bplatform.client.mainModule.ui;

import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;

public class UserManagerFormAdapter extends BpElementWidget {
	
	public UserManagerFormAdapter() {
		
		window.setMaximizable(true);
		window.setMinimizable(true);

		window.setHeadingText("Users Manager");
		window.setWidth(700);
		window.setHeight(400);
		window.add((Widget) GWT.create(UserManagerFormComposite.class));
		
	}
	@Override
	public Widget asWidget() {
		
		return window;
	}

}
