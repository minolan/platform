package com.bplatform.client.mainModule.ui;

import java.util.ArrayList;
import java.util.List;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.bplatform.client.mainModule.services.UsersService;
import com.bplatform.client.mainModule.services.UsersServiceAsync;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.loader.FilterPagingLoadConfig;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.DataProxy;
import com.sencha.gxt.data.shared.loader.FilterPagingLoadConfigBean;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import com.sencha.gxt.widget.core.client.grid.filters.StringFilter;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;

public class UsersListGrid implements IsWidget {

	UsersServiceAsync userPropertiesSvc = GWT.create(UsersService.class);
	PagingToolBar toolBar;
	private int pagesQuantity;
	private boolean autoReloadFilter;
	
	public UsersListGrid () {
		pagesQuantity = 50;
		autoReloadFilter = true;
		
	}
	
	@Override
	public Widget asWidget() {		

//		RpcProxy<PagingLoadConfig, PagingLoadResult<BpUserDTO>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<BpUserDTO>>() {
//			@Override
//			public void load(PagingLoadConfig loadConfig,
//					AsyncCallback<PagingLoadResult<BpUserDTO>> callback) {
//				userPropertiesSvc.getUsers(loadConfig, callback);
//			}
//		};

		
		DataProxy<FilterPagingLoadConfig, PagingLoadResult<BpUserDTO>> proxy = new RpcProxy<FilterPagingLoadConfig, PagingLoadResult<BpUserDTO>>() {
			@Override
			public void load(FilterPagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<BpUserDTO>> callback) {
//				List<? extends SortInfo> sortInfo = loadConfig.getSortInfo();
				userPropertiesSvc.getUsers(loadConfig, callback);
			}
		};
		
//		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
//		rpcWait.showWindow();
//		userPropertiesSvc.getUsers(new RpcCallback<List<BpUserDTO>>(rpcWait.getBox()) {
//
//					@Override
//					public void onSuccess(List<BpUserDTO> result) {
//
//						store.addAll(result);
//						rpcWait.deleteWindow();
//						
//					}
//
//				});
		

		PostProperties props = GWT.create(PostProperties.class);
		
		ListStore<BpUserDTO> store = new ListStore<BpUserDTO>(props.id());

		final PagingLoader<FilterPagingLoadConfig, PagingLoadResult<BpUserDTO>> remoteLoader = 
				new PagingLoader<FilterPagingLoadConfig, PagingLoadResult<BpUserDTO>>(proxy) {
			   @Override
		        protected FilterPagingLoadConfig newLoadConfig() {
		          return new FilterPagingLoadConfigBean();
		        }
		};
		remoteLoader.setRemoteSort(true);
		remoteLoader.addLoadHandler(new LoadResultListStoreBinding<FilterPagingLoadConfig, BpUserDTO, PagingLoadResult<BpUserDTO>>(store));
		
		toolBar = new PagingToolBar(pagesQuantity);
		toolBar.getElement().getStyle().setProperty("borderBottom", "none");
		toolBar.bind(remoteLoader);

//		IdentityValueProvider<BpUserDTO> identity = new IdentityValueProvider<BpUserDTO>();
//		final CheckBoxSelectionModel<BpUserDTO> sm = new CheckBoxSelectionModel<BpUserDTO>(
//				identity) {
//			@Override
//			protected void onRefresh(RefreshEvent event) {
//				// this code selects all rows when paging if the header checkbox
//				// is selected
//				if (isSelectAllChecked()) {
//					selectAll();
//				}
//				super.onRefresh(event);
//			}
//		};

		ColumnConfig<BpUserDTO, String> loginColumn = new ColumnConfig<BpUserDTO, String>(props.login(), 300, "Username");
		ColumnConfig<BpUserDTO, String> fioColumn = new ColumnConfig<BpUserDTO, String>(props.fio(), 300, "FIO");
		ColumnConfig<BpUserDTO, String> emailColumn = new ColumnConfig<BpUserDTO, String>(props.email(), 300, "Email");

		List<ColumnConfig<BpUserDTO, ?>> l = new ArrayList<ColumnConfig<BpUserDTO, ?>>();
//		l.add(sm.getColumn());
		l.add(loginColumn);
		l.add(fioColumn);
		l.add(emailColumn);
		

		ColumnModel<BpUserDTO> cm = new ColumnModel<BpUserDTO>(l);

		final Grid<BpUserDTO> grid = new Grid<BpUserDTO>(store, cm) {
			@Override
			protected void onAfterFirstAttach() {
				super.onAfterFirstAttach();
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {
					@Override
					public void execute() {
						remoteLoader.load();
					}
				});
			}
		};
		
//		grid.setSelectionModel(sm);
		grid.getView().setForceFit(true);
		grid.getView().setAutoFill(true);
		grid.setLoadMask(true);
		grid.setLoader(remoteLoader);
		grid.setBorders(false);
		grid.getView().setStripeRows(true);
		grid.getView().setColumnLines(true);
		
		StringFilter<BpUserDTO> nameFilter = new StringFilter<BpUserDTO>(props.login());
		StringFilter<BpUserDTO> fioFilter = new StringFilter<BpUserDTO>(props.fio());
		StringFilter<BpUserDTO> emailFilter = new StringFilter<BpUserDTO>(props.email());
		GridFilters<BpUserDTO> filters = new GridFilters<BpUserDTO>(remoteLoader);
		filters.initPlugin(grid);
	    filters.setLocal(false);
	    filters.setAutoReload(autoReloadFilter);
	    filters.addFilter(nameFilter);
	    filters.addFilter(emailFilter);
	    filters.addFilter(fioFilter);
	    
	    grid.getSelectionModel().addSelectionHandler(new SelectionHandler<BpUserDTO>() {

			@Override
			public void onSelection(SelectionEvent<BpUserDTO> event) {
				
				Window userProfileWindow;
//				userProfileWindow = new UserManagerFormComposite().asWidget(tree.getSelectionModel().getSelectedItem().getName());
				userProfileWindow = new UserManagerFormComposite().asWidget(grid.getSelectionModel().getSelectedItem().getLogin());
				userProfileWindow.show();	
			}
		});
	    
	    
//		checkBoxTree.getTree().getSelectionModel().addSelectionHandler(
//				new SelectionHandler<BpGroupDTO>() {
//					@Override
//					public void onSelection(SelectionEvent<BpGroupDTO> event) {
//						
//						VerticalLayoutContainer newSplitPanelRight = new VerticalLayoutContainer();
//						GroupManagerForm groupAdminForm = new GroupManagerForm();
//						newSplitPanelRight.add(groupAdminForm.getButtonBar(false), new VerticalLayoutData (1, 30));
//						newSplitPanelRight.add(groupAdminForm.asWidget(checkBoxTree, false), new VerticalLayoutData (1, 1));
//						addRightSplitPanel(newSplitPanelRight);
//						
//					}
//				});
		
		return grid;
	}
	
	public Widget getToolBar() {
		return toolBar;
	}
	
	interface PostProperties extends PropertyAccess<BpUserDTO> {
		 
		  ModelKeyProvider<BpUserDTO> id();
		 
		  ValueProvider<BpUserDTO, String> login();
		  ValueProvider<BpUserDTO, String> fio();
		  ValueProvider<BpUserDTO, String> email();
//		  ValueProvider<BpUserDTO, Set<BpGroupDTO>> groups();
//		  ValueProvider<BpUserDTO, Set<BpPermissionDTO>> permissions();
		}

}
