package com.bplatform.client.mainModule.ui;

import java.util.ArrayList;

import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;

//@BpElementInformation(caption = "Фон", descryption = "Инструмент для персонализации, настройки и управления системы", style = "style-app-2")
public class BackGroundSetting extends BpElementWidget {
	
	private BorderLayoutContainer borderContainer = new BorderLayoutContainer();
	private ArrayList<ListIcon> themes = new ArrayList<ListIcon>();
	private FlowPanel displayConatiner;
	private FlowPanel displayPrewiew;
	private String currentStyle;

	public BackGroundSetting() {
		FlowPanel center = new FlowPanel();

		window.setMaximizable(false);
		window.setResizable(false);
		window.setMinimizable(true);

		window.setHeadingText("Настройка фонового изображения");
//		window.setWidth(400);
//		window.setHeight(600);

		
		window.setWidth(700);
		window.setHeight(400);
		
		displayConatiner = bldPreviewDisplay();

		center.add(displayConatiner);
		//center.add(previewList());

		borderContainer.setSouthWidget(footer(), new BorderLayoutData(35));
		
		borderContainer.setEastWidget(previewList(), new BorderLayoutData(261));
		 
		borderContainer.setCenterWidget(center);

		window.add(borderContainer);
	}

	@Override
	public void show() {
		window.show();
	}

	@Override
	public Widget asWidget() {
		return window;
	}

	private FlowPanel bldPreviewDisplay() {
		FlowPanel panel = new FlowPanel();
		displayPrewiew = new FlowPanel();
		panel.setStyleName("displayConatiner");
		panel.add(displayPrewiew);
		return panel;
	}

	private FlowPanel previewList() {
		FlowPanel panel = new FlowPanel();
		panel.setStyleName("previewList");
		themes.clear();
		themes.add(new ListIcon("theme1", "theme1"));
		themes.add(new ListIcon("theme2", "theme2"));
		themes.add(new ListIcon("theme3", "theme3"));
		themes.add(new ListIcon("theme4", "theme4"));
		themes.add(new ListIcon("theme5", "theme5"));
		themes.add(new ListIcon("theme6", "theme6"));
		themes.add(new ListIcon("theme7", "theme7"));
		themes.add(new ListIcon("theme8", "theme8"));
		themes.add(new ListIcon("theme9", "theme9"));
		themes.add(new ListIcon("theme10", "theme10"));
		themes.add(new ListIcon("theme11", "theme11"));
		themes.add(new ListIcon("theme12", "theme12"));
		themes.add(new ListIcon("theme13", "theme13"));
		themes.add(new ListIcon("theme14", "theme14"));
		themes.add(new ListIcon("theme15", "theme15"));
		themes.add(new ListIcon("theme16", "theme16"));
		themes.add(new ListIcon("theme17", "theme17"));
		themes.add(new ListIcon("theme18", "theme18"));
		themes.add(new ListIcon("theme19", "theme19"));
		themes.add(new ListIcon("theme20", "theme20"));
		
		

		for (ListIcon c : themes) {
			panel.add(c);
		}

		return panel;
	}

	private FlowPanel footer() {
		FlowPanel panel = new FlowPanel();
		panel.setStyleName("footer");
		TextButton apply = new TextButton("Применть");
		TextButton cancel = new TextButton(" Отмена ");
		
		cancel.addStyleName("main-form-buttons");
		apply.addStyleName("main-form-buttons");
				
		cancel.setWidth(100);
		apply.setWidth(100);
		apply.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				RootPanel.get("bm_desktop").setStyleName(currentStyle);

			}
		});
		
		panel.add(apply);
		panel.add(cancel);

		return panel;
	}

	public class ListIcon extends Button {
		String title;
		String className;
		String customImagePath;

		public ListIcon() {
		}

		public ListIcon(String title, String className) {
			this.title = title;
			this.className = className;
			this.addStyleName("listIcon");
			this.addStyleName(className);
			this.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					displayPrewiew.setStyleName(ListIcon.this.className);

					currentStyle = ListIcon.this.className;

					for (ListIcon c : themes) {
						if (event.getSource().equals(c)) {
							ListIcon.this.addStyleName("active");
						} else {
							c.removeStyleName("active");
						}
					}
				}

			});
		}

	}

}
