package com.bplatform.client.mainModule.ui.widgets;

import java.util.ArrayList;
import java.util.List;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.bpShared.DTO.BpUserGroupFoldersDTO;
import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.UsersService;
import com.bplatform.client.mainModule.services.UsersServiceAsync;
import com.bplatform.client.mainModule.ui.resources.UserPropMenuImages;
import com.bplatform.client.mainModule.views.mockModel.State;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.XTemplates;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.info.Info;

public class GroupsComboBox implements IsWidget {
	
	private UsersServiceAsync rpcUserAdminService = GWT
			.create(UsersService.class);
//	private List<BpGroupDTO> listBpGroupDTO;
	private ComboBox<BpGroupDTO> combo;
	GroupsProperties groupsProps = GWT.create(GroupsProperties.class);
	ListStore<BpGroupDTO> groupsLS = new ListStore<BpGroupDTO>(groupsProps.id());
	private String groupId;

	public GroupsComboBox(String groupId) {
		super();
		setGroupId(groupId);
	}
	/**
	 *  Get Combo Box of groups
	 */
	public ComboBox<BpGroupDTO> asWidget() {
		addGroupsToList();
		combo = new ComboBox<BpGroupDTO>(groupsLS,
				groupsProps.name(), new AbstractSafeHtmlRenderer<BpGroupDTO>() {
					final ComboBoxTemplates comboBoxTemplates = GWT
							.create(ComboBoxTemplates.class);

					public SafeHtml render(BpGroupDTO item) {
						return comboBoxTemplates.group(
								UserPropMenuImages.INSTANCE.folder16().getSafeUri(),
								item.getName(), 10 * item.getNestedLevel());
					}
				});
//		addHandlersForEventObservation(combo, groupsProps.name());
		
		combo.setEmptyText("Root Group...");
		combo.setWidth(150);
		combo.setTypeAhead(true);
		combo.setTriggerAction(TriggerAction.ALL);
		return combo;
	}
	
	private void processFolderList(ListStore<BpGroupDTO> groupsLS,
			BpUserGroupFoldersDTO folder, int nestedLevel) {
			nestedLevel++;
			for (BpGroupDTO child : folder.getChildren()) {
				
				boolean addToComboBox = false;
				if (groupId == null) {
					addToComboBox = true;
				} else if (!groupId.equals(child.getId())) {
					addToComboBox = true;
				}
				
				if (addToComboBox) {
					child.setNestedLevel(nestedLevel);
					groupsLS.add(child);
					if (child instanceof BpUserGroupFoldersDTO) {
						processFolderList(groupsLS,
								(BpUserGroupFoldersDTO) child, nestedLevel);
					}
				}
			}
	}
	
	/**
	 * Add Groups from DB to ComboBox List 
	 */
	private void addGroupsToList() {
		
		rpcUserAdminService
				.getGroupsList(new RpcCallback<List<BpUserGroupFoldersDTO>>() {
					@Override
					public void onSuccess(List<BpUserGroupFoldersDTO> result) {
						BpGroupDTO rootTop = new BpGroupDTO("Root Group", null, null);
						groupsLS.add(rootTop);
						for (BpUserGroupFoldersDTO root : result) {
							for (BpGroupDTO base : root.getChildren()) {
								boolean addToComboBox = false;
								if (groupId == null) {
									addToComboBox = true;
								} else if (!groupId.equals(base.getId())) {
									addToComboBox = true;
								}
								if (addToComboBox) {
									base.setNestedLevel(0);
									groupsLS.add(base);
									if (base instanceof BpUserGroupFoldersDTO) {
										processFolderList(groupsLS,
												(BpUserGroupFoldersDTO) base, 0);
									}
								}
						}
							
//						if (listBpGroupDTO != null) {							
//							groupsLS.addAll(listBpGroupDTO);	
//							getItemById (groupId, listBpGroupDTO);
//						} else {
//							System.out
//									.println("========================Null getAllGroups");
//						}

					}
				}
			});
	}
	
	/**
	 * Refresh Groups List in ComboBox
	 */
	public void refreshGroupsList(String groupId) {
		setGroupId(groupId);
		groupsLS.clear();
		addGroupsToList();
	}
	
	 /**
	  * Helper to add handlers to observe events that occur on each combobox
	  */
	  private <T> void addHandlersForEventObservation(ComboBox<T> combo, final LabelProvider<T> labelProvider) {
	    combo.addValueChangeHandler(new ValueChangeHandler<T>() {
	      @Override
	      public void onValueChange(ValueChangeEvent<T> event) {
	        Info.display("Value Changed", "New value: "
	            + (event.getValue() == null ? "nothing" : labelProvider.getLabel(event.getValue()) + "!"));
	      }
	    });
	    combo.addSelectionHandler(new SelectionHandler<T>() {
	      @Override
	      public void onSelection(SelectionEvent<T> event) {
	    	  	    	
	    	  Info.display("State Selected", "You selected "
	            + (event.getSelectedItem() == null ? "nothing" : labelProvider.getLabel(event.getSelectedItem()) + "!"));
	      }
	    });
	  }
	
	interface ComboBoxTemplates extends XTemplates {

		@XTemplate("<div style=\"itemStyle\"><img height=\"11\" src=\"{imageUri}\"> {name}</div>")
		SafeHtml group(SafeUri imageUri, String name);
		
		@XTemplate("<div style=\"margin-left: {itemMargin}px\"><img height=\"11\" src=\"{imageUri}\"> {name}</div>")
		SafeHtml group(SafeUri imageUri, String name, int itemMargin);

		@XTemplate("{icon} {name}")
		SafeHtml group(Image icon, String name);

		@XTemplate("<div qtip=\"{slogan}\" qtitle=\"State Slogan\">{name}</div>")
		SafeHtml state(String slogan, String name);

	}

	interface GroupsProperties extends PropertyAccess<BpGroupDTO> {
		ModelKeyProvider<BpGroupDTO> id();

		LabelProvider<BpGroupDTO> name();
	}

	public BpGroupDTO getSelectedItem() {
		return combo.getCurrentValue();
	}

	public void setDefaultSelectedItem(BpGroupDTO defaultSelectedItem) {
//		combo.Se
	}
	
	
	public ComboBox<BpGroupDTO> getComboBox() {
		return combo;
	}
	
	public BpGroupDTO getItemById (String groupId, List<BpGroupDTO> itemList) {
		for (BpGroupDTO item : itemList) {
			if (item.getId().equals(groupId)) {
				return item;
			}
		}
		return null;
	}
	
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	public String getGroupId() {
		return groupId;
	}
	
}
