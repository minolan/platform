package com.bplatform.client.mainModule.ui.widgets;

import com.bplatform.client.mainModule.ui.desktop.BpShortCut;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

public class 
SectionPanel extends FlowPanel {
	FlowPanel panelTitle = new FlowPanel();
	FlowPanel panelContent = new FlowPanel();

	Label title = new Label();
	
	public SectionPanel() {
		panelTitle.setStyleName("SectionTitle");
		panelTitle.add(title);
		panelContent.setStyleName("SectionContent");
		this.setStyleName("Section");
		this.add(panelTitle);
		this.add(panelContent);
	}
	
	public SectionPanel(String title){
		this();
		this.title.setText(title);
	}
	
	public void setSectionTitle(String title){
		this.title.setText(title);

	}
	public String getSectionTitle(){
		return title.getText();
	}
	
	public void addElement(BpShortCut sc){
		panelContent.add(sc);
	}

}
