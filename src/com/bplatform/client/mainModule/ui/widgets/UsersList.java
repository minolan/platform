package com.bplatform.client.mainModule.ui.widgets;

import com.bplatform.client.mainModule.views.mockModel.NameImageModel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.tree.Tree;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.HTML;
import com.sencha.gxt.widget.core.client.Window;

import java.util.Set;

import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.UsersService;
import com.bplatform.client.mainModule.services.UsersServiceAsync;
import com.bplatform.client.mainModule.ui.UserManagerFormComposite;
import com.bplatform.client.mainModule.ui.resources.UserPropMenuImages;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.IconProvider;
import com.sencha.gxt.widget.core.client.container.MarginData;

public class UsersList implements IsWidget {

	private UsersServiceAsync userPropertiesSvc = GWT.create(UsersService.class);
	private Window vPW;

	@Override
	public Widget asWidget() {
		// TODO Auto-generated method stub
		return null;
	}

	// Show user list
	public Widget usersListWidget(String bpGroupId) {

		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();

		// viewPropPanel.setHeadingText(getSelectedItem().getName());

		final ContentPanel cp = new ContentPanel();
		cp.setAnimCollapse(false);
		cp.setHeadingText("Online Users");
		cp.setHeaderVisible(false);
		cp.setBodyBorder(false);
		cp.setBorders(false);

		// Images of members
		tree.setIconProvider(new IconProvider<NameImageModel>() {
			public ImageResource getIcon(NameImageModel model) {
				if (null == model.getImage()) {
					return null;
				} else if ("user-girl" == model.getImage()) {
					return UserPropMenuImages.INSTANCE.userFemale();
				} else if ("user-kid" == model.getImage()) {
					return UserPropMenuImages.INSTANCE.userKid();
				} else {
					return UserPropMenuImages.INSTANCE.user();
				}
			}
		});

		userPropertiesSvc.getUsersListFromGroup(bpGroupId,
				new RpcCallback<Set<String>>(rpcWait.getBox()) {

					@Override
					public void onSuccess(Set<String> result) {

						if (result.size() > 0) {
							// final NameImageModel m =
							// newItem(getSelectedUsers().getName(), null);
							// store.add(m);

							for (String i : result) {
								// store.add(m, newItem(i, "user"));
								final NameImageModel m = newItem(i, "user");
								store.add(m);
								// tree.setExpanded(m, true);
							}

						} else {

							// HTML text = new HTML("The group \"" +
							// getSelectedItem().getName() +
							// "\" has no members");
							HTML text = new HTML("This group has no users");
							text.setHeight("200px");
							cp.add(text, new MarginData(5));

						}
						rpcWait.deleteWindow();
					}
				});

		// ------------ Handler Edit User Profile
		tree.getSelectionModel().addSelectionHandler(
				new SelectionHandler<NameImageModel>() {

					@Override
					public void onSelection(SelectionEvent<NameImageModel> event) {
						vPW = new UserManagerFormComposite().asWidget(tree
								.getSelectionModel().getSelectedItem()
								.getName());
						vPW.show();
					}
				});
		// --------------End Handler Edit User Profile

		cp.add(tree);
		return cp;

	}

	private NameImageModel newItem(String text, String iconStyle) {
		return new NameImageModel(text, iconStyle);
	}
	
	private TreeStore<NameImageModel> store = new TreeStore<NameImageModel>(
			NameImageModel.KP);

	private Tree<NameImageModel, String> tree = new Tree<NameImageModel, String>(
			store, new ValueProvider<NameImageModel, String>() {

				@Override
				public String getValue(NameImageModel object) {
					return object.getName();
				}

				@Override
				public void setValue(NameImageModel object, String value) {
				}

				@Override
				public String getPath() {
					return "name";
				}

			});
	public Tree<NameImageModel, String> getTree() {
		return tree;
	}
}
