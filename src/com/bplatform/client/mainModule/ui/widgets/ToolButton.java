package com.bplatform.client.mainModule.ui.widgets;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.ui.Button;
/*
 * @author Blind Pew
 * @version 0.0.1
 * 
 * Buttons for form or window panels
 * just design 
 * 
 * 2013.09.17
 * 
 * 
 * @see "setButtonStyle"
 * TODO: Blind Pew: I forgot about it, What I meant? 
 * */
public class ToolButton extends Button {

	public ToolButton() {
		this.setStyleName("toolButton");
	}

	public ToolButton(SafeHtml html) {
		super(html);
	}

	public ToolButton(String html) {
		super(html);
	}

	public ToolButton(Element element) {
		super(element);
	}

	public ToolButton(SafeHtml html, ClickHandler handler) {
		super(html, handler);
	}

	public ToolButton(String html, ClickHandler handler) {
		super(html, handler);
	}
	
	public void setButtonStyle(int style){
		/*style 0-only text, 1-only icon, 2 - icon and text */
		/*TODO: Blind Pew: I forgot about it, What I meant? */
	}

}
