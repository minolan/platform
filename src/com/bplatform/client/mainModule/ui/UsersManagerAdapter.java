package com.bplatform.client.mainModule.ui;

import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

public class UsersManagerAdapter extends BpElementWidget {
	
	public  UsersManagerAdapter() {
		window.setMaximizable(true);
		window.setMinimizable(true);
		window.setHeadingText("Groups Manager");
		window.setWidth(700);
		window.setHeight(400);
		
		window.add((Widget) GWT.create(UsersManagerComposite.class));
	}
	
}
