package com.bplatform.client.mainModule.ui;

import com.bplatform.client.glass.client.box.GlassMessageBox;
import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutPack;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.FormPanel.Method;

/**
 * �������� ����� � �������
 * 
 * @author Melkior
 * 
 */
public class FileDownloadForm extends BpElementWidget {
/*	private Window window;*/
	public final static String SERVLET_NAME = "services/downloadFileServlet";

	@Override
	public void show() {
		window.show();

	}
	public FileDownloadForm(){
		createDownPan();
	}
	@Override
	public Widget asWidget() {
		return window;
	}

	private void createDownPan() {
		/*window = new Window();
		setWindow(window);
*/
		window.setPixelSize(300, 150);
		window.setHeadingText("File Download");
		window.setButtonAlign(BoxLayoutPack.CENTER);

		final FormPanel form = new FormPanel();
		form.setAction(GWT.getHostPageBaseURL() + SERVLET_NAME);
		form.setMethod(Method.GET);
		// form.setEncoding(Encoding.MULTIPART);

		window.add(form);
		final VerticalPanel p = new VerticalPanel();
		form.add(p);

		TextButton btn = new TextButton("Download");

		btn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				if (!form.isValid()) {
					return;
				}
				form.submit();

				MessageBox box = new MessageBox("Your file was downloaded.");
				box.setIcon(GlassMessageBox.ICONS.info());
				box.show();

				MessageBox boxA = new MessageBox("Your file was downloaded.");
				boxA.setIcon(GlassMessageBox.ICONS.error());
				// boxA.set
				boxA.show();

				MessageBox boxQ = new MessageBox("Your file was downloaded.");
				boxQ.setIcon(GlassMessageBox.ICONS.question());
				boxQ.show();

				MessageBox boxW = new MessageBox("Your file was downloaded.");
				boxW.setIcon(GlassMessageBox.ICONS.warning());
				boxW.show();

				// GlassMessageBox box2 = new
				// GlassMessageBox("Your file was downloaded.");

				// box2.setIcon(MessageBox.ICONS.info());

				// box2.show();

				// box.setIcon(com.bplatform.client.glass.client.box.MessageBox.ICONS.info());

				// ��� ������� ����� �������� ����

				// com.bplatform.client.glass.client.box.MessageBox box2
				// = new
				// com.bplatform.client.glass.client.box.MessageBox("Your file was downloaded.");
				// box2.show();

			}
		});
		window.addButton(btn);

	}

}
