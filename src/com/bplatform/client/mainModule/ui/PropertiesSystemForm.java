package com.bplatform.client.mainModule.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.BPService;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.core.client.util.Padding;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.box.AutoProgressMessageBox;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutData;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer.HBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer.VBoxLayoutAlign;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.TextField;

//import com.sencha.gxt.explorer.client.model.Example.Detail;
/**
 * ����� ��� ������ � ��������� �������� ������
 * 
 * @author Melkior
 * 
 */
public class PropertiesSystemForm implements IsWidget {

	private HashMap<String, Map<String, Map<String, String>>> allLocal = new HashMap<String, Map<String, Map<String, String>>>();
	private HashMap<String, Map<String, Map<String, String>>> defaulteAllLocal = new HashMap<String, Map<String, Map<String, String>>>();
	private HashMap<String, List<FieldLabel>> labelListMap = new HashMap<String, List<FieldLabel>>();

	private ContentPanel panel = new ContentPanel();
	private ToggleGroup toggleGroup = new ToggleGroup();
	private ContentPanel lccenter;
	private VBoxLayoutContainer lcwest;

	private BoxLayoutData vBoxData;
	private ScrollPanel con;

	// private

	@Override
	public Widget asWidget() {

		con = new ScrollPanel();
		con.getElement().getStyle().setMargin(10, Unit.PX);

		panel.setHeaderVisible(false);
		panel.setPixelSize(600, 500);
		panel.setResize(true);

		BorderLayoutContainer border = new BorderLayoutContainer();
		panel.setWidget(border);

		lcwest = new VBoxLayoutContainer();
		lcwest.setPadding(new Padding(5));
		lcwest.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCH);

		BorderLayoutData west = new BorderLayoutData(150);
		west.setMargins(new Margins(5));
		west.setSplit(true);

		border.setWestWidget(lcwest, west);

		vBoxData = new BoxLayoutData(new Margins(5, 5, 5, 5));
		vBoxData.setFlex(1);

		lccenter = new ContentPanel();
		lccenter.setDeferHeight(true);
		lccenter.setHeaderVisible(false);

		MarginData center = new MarginData(new Margins(5));

		border.setCenterWidget(lccenter, center);
		lcwest.setLayoutData(vBoxData);

		initLocalMap();

		con.add(panel);
		return con;

	}

	private void initLocalMap() { // �������� �������� ������ � �������
		BPService.Util.getInstance().getAllLocalPropMap(new RpcCallback<Map<String, Map<String, Map<String, String>>>>() {

			@Override
			public void onSuccess(Map<String, Map<String, Map<String, String>>> result) {

				allLocal.putAll(result);
				defaulteAllLocal.putAll(result); // ����� ��� ���������� ������
													// � ��������� ���������

				resourceAdd();//

			}

		});

	}

	// ����������� �������� � ��������� �������� �� ��������
	private void resourceAdd() {

		Set<String> locSetKeys = allLocal.keySet();

		for (String locKey : locSetKeys) {
			HashMap<String, Map<String, String>> resLocMap = (HashMap<String, Map<String, String>>) allLocal.get(locKey);
			Set<String> resSetKeys = resLocMap.keySet();
			for (String resKey : resSetKeys) {
				HashMap<String, String> resource = (HashMap<String, String>) resLocMap.get(resKey);
				addToLcwest(locKey, resKey, resource);
			}
		}

	}

	private void addToLcwest(final String locName, final String resName, final HashMap<String, String> resource) {

		final HashMap<String, String> resourceLoc = new HashMap<String, String>();
		resourceLoc.putAll(resource);

		final ArrayList<FieldLabel> arr = new ArrayList<FieldLabel>();

		ToggleButton button = createToggleButton("BP_" + locName + "_" + resName, new ValueChangeHandler<Boolean>() { // ��������
																														// ��������
																														// �
																														// ���������
																														// ����_������
					HashMap<String, String> res = new HashMap<String, String>();
					ArrayList<TextField> textFieledArr = new ArrayList<TextField>();

					public void onValueChange(ValueChangeEvent<Boolean> event) {

						if (event.getValue()) {
							final HBoxLayoutContainer c = new HBoxLayoutContainer();
							c.setPadding(new Padding(5));
							c.setHBoxLayoutAlign(HBoxLayoutAlign.TOP);

							Set<String> keySet = resourceLoc.keySet();

							for (String keyValue : keySet) { // ����������
																// �������� ��
																// ����������������
																// ������� ��
																// ������ ������
																// ����_������
								String resourceValue = resourceLoc.get(keyValue);

								TextField resName = new TextField();
								resName.setName(keyValue);
								resName.setValue(resourceValue);
								textFieledArr.add(resName);
								resName.setAllowBlank(false);
								FieldLabel label = new FieldLabel(resName, keyValue);
								arr.add(label);
								c.add(label, new BoxLayoutData(new Margins(0, 5, 0, 0)));

							}
							labelListMap.put("BP_" + locName + "_" + resName, arr);

							// ������ ��� ���������� ��������� � ��������� �
							// ���� ��������
							Button save = new Button();
							save.addClickHandler(new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									final AutoProgressMessageBox box = new AutoProgressMessageBox("Progress", "Saving your data, please wait...");
									box.setProgressText("Saving...");
									box.auto();
									box.show();
									final Timer t = new Timer() {

										@Override
										public void run() {
											box.hide();
										}
									};

									for (TextField textField : textFieledArr) {
										System.out.println(textField.getName() + "=" + textField.getValue()); //

										res.put(textField.getName(), textField.getValue());
									}

									String fileName = locName + "_" + resName;

									BPService.Util.getInstance().setNewProp(fileName, res, new RpcCallback<Boolean>(box) {

										@Override
										public void onSuccess(Boolean result) { // ����������
																				// �����
																				// ��������

											t.schedule(1000);

											// --------------------------------------------------------------------------------
											BPService.Util.getInstance().getLocalPropMap(locName, resName, new RpcCallback<HashMap<String, String>>() {

												@Override
												public void onSuccess(HashMap<String, String> result) { // ������
																										// �����
																										// ��������
																										// ����_������
													resourceLoc.putAll(result);

												}
											});
											// --------------------------------------------------------------------------------
										}
									});

								}
							});
							c.add(save, new BoxLayoutData(new Margins(0, 5, 10, 0)));
							addToCenter(c);
						}
					}

				});
		lcwest.add(button, vBoxData);

		// ������� �������� ������������ ��������

		// lccenter.sync(true);
		// lccenter.show();
		// lccenter.forceLayout();
		// lccenter.setVisible(true);
		// lccenter.setLayoutData(lccenter);
		// lccenter.setLayoutData(lcwest);
		// lccenter.setLayoutData(vBoxData);
		// lccenter.onResize();

		// panel.sync(true);
		// panel.show();
		// panel.forceLayout();
		// panel.setVisible(true);
		// panel.setLayoutData(lccenter);
		// panel.setLayoutData(lcwest);
		// panel.setLayoutData(vBoxData);
		// panel.onResize();
		// this.panel.forceLayout();
		//
		lccenter.forceLayout();
		con.onResize();
		con.setVisible(true);

		// lcwest.sync(true);
		// lcwest.show();
		// lcwest.forceLayout();
		// lcwest.setVisible(true);
		// lcwest.onResize();
		// lcwest.setVisible(true);

	}

	private ToggleButton createToggleButton(String name, ValueChangeHandler<Boolean> valueChangeHandler) {
		ToggleButton button = new ToggleButton(name);
		toggleGroup.add(button);
		button.addValueChangeHandler(valueChangeHandler);
		button.setAllowDepress(false);
		return button;
	}

	private void addToCenter(Widget c) {
		lccenter.add(c);
		// lccenter.forceLayout();

	}

}
