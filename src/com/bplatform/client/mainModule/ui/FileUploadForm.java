package com.bplatform.client.mainModule.ui;



import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.bplatform.shared.constants.PlatformServerConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutPack;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FileUploadField;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.FormPanel.Encoding;
import com.sencha.gxt.widget.core.client.form.FormPanel.Method;
import com.sencha.gxt.widget.core.client.info.Info;

/**
 * ���� ��� �������� ����� �� ������
 * 
 * @author Melkior
 * 
 */
public class FileUploadForm extends BpElementWidget {

/*	private VerticalPanel vp;
	private VerticalPanel p = new VerticalPanel();
	private HashMap<Integer, HorizontalPanel> lines = new HashMap<Integer, HorizontalPanel>();
*/
	public final static String SERVLET_NAME = "services/uploadFileServlet";

	public FileUploadForm(){
		createUploadPan();
	}
	
	@Override
	public Widget asWidget() {
		return window;
	}

	private void createUploadPan() {
		
		window = new Window();
		setWindow(window);
		
		window.setPixelSize(300, 150);
		window.setHeadingText("File Upload");
		window.setButtonAlign(BoxLayoutPack.CENTER);

		final FormPanel form = new FormPanel();
		form.setAction(GWT.getHostPageBaseURL() + SERVLET_NAME);
		form.setMethod(Method.POST);
		form.setEncoding(Encoding.MULTIPART);

		window.add(form);
		final VerticalPanel p = new VerticalPanel();
		form.add(p);

		HorizontalPanel hp = new HorizontalPanel();

		final FileUploadField file = new FileUploadField();
		file.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				Info.display("File Changed", "You selected " + file.getValue());
			}
		});
		file.setName(PlatformServerConstants.ATTACHINFO.UPLOAD);
		file.setAllowBlank(false);

		Button add = new Button("+");
		add.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// TO DO ������������� ���������� ����� ������� addNewLine()
			}

		});

		hp.add(add);
		hp.add(new FieldLabel(file, "File"));

		p.add(hp);

		TextButton btn = new TextButton("Reset");
		btn.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				form.reset();
				file.reset();
			}
		});

		window.addButton(btn);

		btn = new TextButton("Submit");
		btn.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				if (!form.isValid()) {
					return;
				}
				form.submit();
				MessageBox box = new MessageBox("Your file was uploaded.");
				box.setIcon(MessageBox.ICONS.info());
				box.show();
			}
		});
		window.addButton(btn);

	}

	/*
	 * private void addNewLine() { int i = 0;
	 * 
	 * final FileUploadField file1 = new FileUploadField();
	 * file1.addChangeHandler(new ChangeHandler() {
	 * 
	 * @Override public void onChange(ChangeEvent event) {
	 * Info.display("File Changed", "You selected " + file1.getValue()); } });
	 * file1.setName("uploadedfile"); file1.setAllowBlank(false);
	 * 
	 * final HorizontalPanel hpp = new HorizontalPanel(); hpp.add(new
	 * FieldLabel(file1, "File")); Button rem = new Button("-");
	 * rem.addClickHandler(new ClickHandler() {
	 * 
	 * @Override public void onClick(ClickEvent event) {
	 * 
	 * } });
	 * 
	 * lines.put(++i, hpp);
	 * 
	 * }
	 */

	@Override
	public void show() {
		window.show();

	}
}
