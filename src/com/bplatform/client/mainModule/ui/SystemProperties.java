/**
 * System Properties Interface
 * @author RomanL
 */
package com.bplatform.client.mainModule.ui;

import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.bplatform.client.mainModule.ui.resources.UserPropMenuImages;
import com.bplatform.client.mainModule.views.mockModel.NameImageModel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.core.client.resources.ThemeStyles;
import com.sencha.gxt.data.shared.IconProvider;
import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.ContentPanel.ContentPanelAppearance;
import com.sencha.gxt.widget.core.client.FramedPanel.FramedPanelAppearance;
import com.sencha.gxt.widget.core.client.container.AccordionLayoutContainer;
import com.sencha.gxt.widget.core.client.container.AccordionLayoutContainer.AccordionLayoutAppearance;
import com.sencha.gxt.widget.core.client.container.AccordionLayoutContainer.ExpandMode;
import com.sencha.gxt.widget.core.client.tree.Tree;

public class SystemProperties extends BpElementWidget {

	public ContentPanel viewPropPanel;
	private SplitLayoutPanel sp;

	// private SystemPropertiesServiceAsync systemPropertiesSvc = GWT
	// .create(SystemPropertiesService.class);

	public SystemProperties () {
		sp = new SplitLayoutPanel();
		viewPropPanel = new ContentPanel(
				GWT.<ContentPanelAppearance> create(FramedPanelAppearance.class));
		window.setMaximizable(true);
		window.setHeadingText("System Proporties Menu rly GTX3 glass");
		window.setWidth(600);
		window.setHeight(300);
		window.setMinimizable(true);
		
		sp.addWest(createPropPanel(), 200);
		sp.add(viewPropPanel);
		window.add(sp);

		viewPropPanel.setHeadingText("Welcome!");
		viewPropPanel.add(new HTML("Please, select necessary menu item"));
	}
	
	@Override
	public Widget asWidget() {
		
		return window;
	}

	public Widget createPropPanel() {

		AccordionLayoutContainer con = new AccordionLayoutContainer();
		con.setExpandMode(ExpandMode.SINGLE_FILL);
		AccordionLayoutAppearance appearance = GWT
				.<AccordionLayoutAppearance> create(AccordionLayoutAppearance.class);

		final ContentPanel cp = new ContentPanel(appearance);
		cp.setHeadingText("Online Users");
		cp.getHeader().addStyleName(ThemeStyles.getStyle().borderTop());
		con.add(cp);
		con.setActiveWidget(cp);

		TreeStore<NameImageModel> store = new TreeStore<NameImageModel>(
				NameImageModel.KP);

		final Tree<NameImageModel, String> tree = new Tree<NameImageModel, String>(
				store, new ValueProvider<NameImageModel, String>() {

					@Override
					public String getValue(NameImageModel object) {
						return object.getName();
					}

					@Override
					public void setValue(NameImageModel object, String value) {
					}

					@Override
					public String getPath() {
						return "name";
					}

				});

		// Images of members
		tree.setIconProvider(new IconProvider<NameImageModel>() {
			public ImageResource getIcon(NameImageModel model) {
				if (null == model.getImage()) {
					return null;
				} else if ("user-girl" == model.getImage()) {
					return UserPropMenuImages.INSTANCE.userFemale();
				} else if ("user-kid" == model.getImage()) {
					return UserPropMenuImages.INSTANCE.userKid();
				} else {
					return UserPropMenuImages.INSTANCE.user();
				}
			}
		});

		NameImageModel m = newItem("User Administration", null);
		store.add(m);

		store.add(m, newItem("Edit Own Profile", "user-kid"));
		store.add(m, newItem("Users/Groups", "user-kid"));
		store.add(m, newItem("Other1", "user-kid"));
		store.add(m, newItem("Other2", "user-kid"));

		tree.setExpanded(m, true);

		m = newItem("Preferences", null);
		store.add(m);

		store.add(m, newItem("Properties", "user-kid"));
		store.add(m, newItem("Other4", "user-kid"));

		tree.setExpanded(m, true);

		cp.add(tree);

		tree.getSelectionModel().addSelectionHandler(
				new SelectionHandler<NameImageModel>() {

					@Override
					public void onSelection(SelectionEvent<NameImageModel> event) {
						if (tree.getSelectionModel().getSelectedItem().getName().equals("Edit Own Profile")) {
//							viewPropPanel.setHeadingText(tree.getSelectionModel().getSelectedItem().getName());
//							viewPropPanel.add(new UserAdminForm().asWidget());
//							sp.add(new UserAdminForm().asWidget());
						} else if (tree.getSelectionModel().getSelectedItem().getName().equals("Users/Groups")) {
//							sp.remove(viewPropPanel);
//							sp.add(new CheckBoxTree().groupsWidgetSplit());
//							viewPropPanel.setHeaderVisible(false));
//							viewPropPanel.setStyleName("splitPanelSysProp");
							viewPropPanel.setHeadingText(tree.getSelectionModel().getSelectedItem().getName());
							viewPropPanel.add(new GroupsManagerComposite());
						} else if (tree.getSelectionModel().getSelectedItem()
								.getName().equals("Properties")) {
							viewPropPanel.setHeadingText(tree
									.getSelectionModel().getSelectedItem()
									.getName());
							viewPropPanel.add(new PropertiesSystemForm()
									.asWidget());
						} else {
							viewPropPanel.setHeadingText(tree
									.getSelectionModel().getSelectedItem()
									.getName());
							viewPropPanel.add(new HTML(tree.getSelectionModel()
									.getSelectedItem().getImage()));
						}
					}
				});

		return con;

	}

	private NameImageModel newItem(String text, String iconStyle) {
		return new NameImageModel(text, iconStyle);
	}

	@Override
	public void show() {
		window.show();
	}

}