package com.bplatform.client.mainModule.ui;

import com.bplatform.client.mainModule.ui.desktop.BpDesktop;
import com.bplatform.client.mainModule.ui.desktop.BpElementInformation;
import com.bplatform.client.mainModule.ui.desktop.BpShortCut;
import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.bplatform.client.mainModule.ui.desktop.impl.BpElementInformationImpl;
import com.bplatform.client.mainModule.ui.desktop.impl.BpShortCutImpl;
import com.bplatform.client.mainModule.ui.widgets.SectionPanel;
import com.bplatform.client.mainModule.ui.widgets.ToolButton;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.dom.ScrollSupport.ScrollMode;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;
import com.sencha.gxt.widget.core.client.container.FlowLayoutContainer;

//@BpElementInformation(caption = "Панель управления", descryption = "Инструмент для персонализации, настройки и управления системы", style = "style-app-0")
public class Settings extends BpElementWidget {


	private BorderLayoutContainer borderContainer = new BorderLayoutContainer();

	public Settings() {

		super();
		window.setMaximizable(true);
		window.setMinimizable(true);

		window.setHeadingText("Панель управления");
		window.setWidth(700);
		window.setHeight(400);

		borderContainer.setStyleName("view-tile");
	}

	public Settings(BpDesktop bpDesktop) {
		this();
		this.bpDesktop = bpDesktop;
	}

	@Override
	public Widget asWidget() {
		return window;
	}

	private void BuildWindowContent() {
		SectionPanel section1 = new SectionPanel("Персональные настройки");
		SectionPanel section2 = new SectionPanel("Администрирование");
		SectionPanel section3 = new SectionPanel("Настройка системы");

		// BpElement beSettings = GWT.create(SystemProperties.class);
		// BpElement beBackGroundSetting = GWT.create(BackGroundSetting.class);

		// TODO: реально тупо, с менеджером и ярлыками надо навести порядок

		if (this.getBpMenuButton() != null) {
//			BpWindowManager wm = this.getBpMenuButton().getWindowManager();
/*			BpElementInformation info0 = new BpElementInformationImpl("Язык системы",
					"Региональные настройки и препочтительный язык системы и приложений",
					"setting-language");
			BpElementInformation info1 = new BpElementInformationImpl("Оформление системы",
					"Изменения настроек оформления системы, цветоврй палитры и размеров элементов интерфеса",
					"setting-color");
*/
			BpElementInformation info10 = new BpElementInformationImpl("Фон рабочего стола",
					"Изменения настроек фона рабочего стола, его положения и поведения",
					"setting-wallpaper");

			BpElementInformation info3 = new BpElementInformationImpl("Настройка синхронизации",
					"",
					"setting-sync");

			BpElementInformation info4 = new BpElementInformationImpl("Изменение пароля",
					"",
					"setting-password");

			BpElementInformation info5 = new BpElementInformationImpl("Настройка поиска",
					"",
					"all-settings");

			BpElementInformation info6 = new BpElementInformationImpl("Производительность",
					"",
					"gnome-do");

			BpElementInformation info7 = new BpElementInformationImpl("Конфигрурирование системы",
					"",
					"config");

			BpElementInformation info8 = new BpElementInformationImpl("Группы пользователей",
					"",
					"susers");

			BpElementInformation info9 = new BpElementInformationImpl("Системный сканер",
					"",
					"zenmap");
			
			BpElementInformation info2 = new BpElementInformationImpl("Язык системы",
					"",
					"setting-language");
			
//			BpShortCut bmSettings = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info0);
//			BpShortCut bmBackGroundSetting = new BpShortCut(BackGroundSetting.class, bpDesktop, info1);
			BpShortCut bmSettings0 = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info2);
			BpShortCut bmSettings1 = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info3);
			BpShortCut bmSettings2 = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info4);
			BpShortCut bmSettings3 = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info5);
			BpShortCut bmSettings4 = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info6);
			BpShortCut bmSettingPassword = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info7);
			BpShortCut bmSettingsSync = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info8);
			BpShortCut btnSettingLanguage = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info9);
			BpShortCut btnSettingWallpaper = new BpShortCutImpl(BackGroundSetting.class, bpDesktop, info10);

			// btnSettingLanguage.asWidget().removeStyleName("menuShortCut");
			// btnSettingLanguage.asWidget().setStyleName("wShortCut");
			//
			// btnSettingLanguage.setCaption("Язык системы");
			// btnSettingLanguage
			// .setDescryption("Региональные настройки и препочтительный язык системы и приложений");
			//
			// btnSettingLanguage.setButtonStyle("setting-language");
			//
			// ShortCut btnSettingDesktop = new ShortCut(beSettings);
			// btnSettingDesktop.asWidget().removeStyleName("menuShortCut");
			// btnSettingDesktop.asWidget().addStyleName("wShortCut");
			// btnSettingDesktop.setCaption("Оформление системы");
			// btnSettingDesktop
			// .setDescryption("Изменения настроек оформления системы, цветоврй палитры и размеров элементов интерфеса");
			// btnSettingDesktop.setButtonStyle("setting-color");
			//
			// btnSettingWallpaper.asWidget().removeStyleName("menuShortCut");
			// btnSettingWallpaper.asWidget().addStyleName("wShortCut");
			// btnSettingWallpaper.setCaption("Фон рабочего стола");
			// btnSettingWallpaper
			// .setDescryption("Изменения настроек фона рабочего стола, его положения и поведения");
			//
			// btnSettingWallpaper.setButtonStyle("setting-wallpaper");
			//
			// bmSettingPassword.setCaption("Изменение пароля");
			// bmSettingPassword.asWidget().removeStyleName("menuShortCut");
			// bmSettingPassword.setButtonStyle("setting-password");
			// bmSettingPassword.asWidget().addStyleName("wShortCut");
			//
			// bmSettingsSync.asWidget().removeStyleName("menuShortCut");
			// bmSettingsSync.asWidget().addStyleName("wShortCut");
			// bmSettingsSync.setCaption("Настройка синхронизации");
			// bmSettingsSync.setButtonStyle("setting-sync");
			//
			// bmSettings0.asWidget().removeStyleName("menuShortCut");
			// bmSettings1.asWidget().removeStyleName("menuShortCut");
			// bmSettings2.asWidget().removeStyleName("menuShortCut");
			// bmSettings3.asWidget().removeStyleName("menuShortCut");
			// bmSettings4.asWidget().removeStyleName("menuShortCut");
			//
			// bmSettings0.asWidget().addStyleName("wShortCut");
			// bmSettings1.asWidget().addStyleName("wShortCut");
			// bmSettings2.asWidget().addStyleName("wShortCut");
			// bmSettings3.asWidget().addStyleName("wShortCut");
			// bmSettings4.asWidget().addStyleName("wShortCut");
			//
			// bmSettings0.setCaption("Настройка поиска");
			// bmSettings1.setCaption("Производительность");
			// bmSettings2.setCaption("Конфигрурирование системы");
			// bmSettings3.setCaption("Группы пользователей");
			// bmSettings4.setCaption("Системный сканер");
			//
			// bmSettings0.setButtonStyle("all-settings");
			// bmSettings1.setButtonStyle("gnome-do");
			// bmSettings2.setButtonStyle("config");
			// bmSettings3.setButtonStyle("susers");
			// bmSettings4.setButtonStyle("zenmap");

			section1.addElement(bmSettingPassword);
			section1.addElement(bmSettingsSync);
			section1.addElement(btnSettingWallpaper);
//			 section1.addElement(btnSettingDesktop);
			section1.addElement(btnSettingLanguage);

			section2.addElement(bmSettings3);
			section2.addElement(bmSettings4);

			section3.addElement(bmSettings0);
			section3.addElement(bmSettings1);
			section3.addElement(bmSettings2);
		}
		//

		FlowLayoutContainer vp = new FlowLayoutContainer();
		vp.setScrollMode(ScrollMode.AUTOY);
		// vp.setSize("100%", "80%");
		FlowPanel contentFake = new FlowPanel();
//		AbsolutePanel ap = new AbsolutePanel();
		vp.add(contentFake);

		vp.add(section1);
		vp.add(section2);
		vp.add(section3);

		// content.add(ap);

		// content.add(toolBar());
		// content.add(vp);
		// window.add(content);

		FlowPanel toolBar = toolBar();
		// toolBar.setHeight("50px");

		borderContainer.setNorthWidget(toolBar, new BorderLayoutData(50));
		// borderContainer.getNorthWidget().setHeight("10px");
		// borderContainer.isAutoHeight();
		// borderContainer.

		borderContainer.setCenterWidget(vp);

		// borderContainer.getNorthWidget().setHeight("50px");
		// borderContainer.getNorthWidget().getElement().getStyle().setProperty("height",
		// "50px");

		window.add(borderContainer);
	}

	private FlowPanel toolBar() {
		FlowPanel panel = new FlowPanel();
		panel.setStyleName("toolBar");

		ToolButton btnViewSettings = new ToolButton();
		ToolButton btnShowList = new ToolButton();
		ToolButton btnShowIcons = new ToolButton();
		ToolButton btnShowTile = new ToolButton();

		ToolButton btnHome = new ToolButton();

		btnViewSettings.addStyleName("btnViewSettings");
		btnShowIcons.addStyleName("btnShowIcons");
		btnShowTile.addStyleName("btnShowTile");
		btnShowList.addStyleName("btnShowList");

		btnShowList.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				borderContainer.setStyleName("view-list");

			}
		});

		btnShowTile.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				borderContainer.setStyleName("view-tile");

			}
		});

		btnShowIcons.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				borderContainer.setStyleName("view-icon");

			}
		});

		btnHome.addStyleName("btnHome");

		TextBox searchBox = new TextBox();

		searchBox.setStyleName("toolSearchBox");

		panel.add(btnHome);
		panel.add(searchBox);

		panel.add(btnViewSettings);

		panel.add(btnShowList);
		panel.add(btnShowIcons);
		panel.add(btnShowTile);

		return panel;
	}

	@Override
	public void show() {
		GWT.log("show settings window");
		ClearWindowContent();
		BuildWindowContent();
		window.show();
	}

	private void ClearWindowContent() {
		window.clear();
	}
}
