/**
 * Create user profile Interface  
 * @author RomanL
 */
package com.bplatform.client.mainModule.ui;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.GroupsService;
import com.bplatform.client.mainModule.services.GroupsServiceAsync;
import com.bplatform.client.mainModule.ui.widgets.GroupsTree;
import com.bplatform.client.mainModule.ui.widgets.GroupsComboBox;
import com.bplatform.client.mainModule.ui.widgets.RpcWaitingWindow;
import com.bplatform.client.mainModule.ui.widgets.UsersList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer.BoxLayoutData;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.RegExValidator;
import com.bplatform.client.mainModule.ui.resources.UserPropMenuImages;

/**
 * @author RomanL
 * 
 */
public class GroupManagerForm implements IsWidget {

	private Window window;
	private ContentPanel userSettingsOwn;
	private TextField name;
	private GroupsTree groupsTreeLeftMenu;
	private GroupsServiceAsync groupAdminSvc = GWT
			.create(GroupsService.class);
	private UsersList userlist = new UsersList();
	private BpGroupDTO selectedGroup;
	private ButtonBar buttonBar;
	private TabPanel tabs;
	private ScrollPanel scrollPanel;
	private VerticalLayoutContainer p;
	private boolean readOnly= true;

	private TextButton save;
	private TextButton editModeToRO;
	private TextButton editModeToEdit;
	private TextButton cancel;
	private GroupsComboBox groupsComboBoxWidget;
	private int parentGroupId;
	boolean newGroup = true;
	BpGroupDTO bpGroupForSave = new BpGroupDTO();

	public int getParentGroupId() {
		return parentGroupId;
	}

	public void setParentGroupId(int parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	// Show own user Properties
	@Override
	public Widget asWidget() {

		createTabPanel(null);
		return userSettingsOwn;

	}

	// Form Group Properties
	public Widget asWidget(GroupsTree groupsTreeLeftMenu, boolean newGroup) {
		this.newGroup = newGroup;
		this.selectedGroup = groupsTreeLeftMenu.getSelectedItem();
		this.groupsTreeLeftMenu = groupsTreeLeftMenu;
		if (selectedGroup == null || newGroup == true) {
			createTabPanel(null);
		} else {
			createTabPanel(this.selectedGroup.getId());
		}
		return tabs;
	}

	private void createTabPanel(String groupId) {

		userSettingsOwn = new ContentPanel();
		userSettingsOwn.setHeaderVisible(false);

		tabs = new TabPanel();
		// Group Setting
		p = new VerticalLayoutContainer();
		scrollPanel = new ScrollPanel();
		scrollPanel.setHeight("100%");
		scrollPanel.add(p);
		scrollPanel.setLayoutData(new MarginData(8));
		p.setLayoutData(new MarginData(8));
		tabs.add(scrollPanel, "Group Settings");
		tabs.addStyleName("listStyle");
		tabs.setSize("100%", "100%");

		// Name field
		name = new TextField();
		name.setReadOnly(readOnly);
		name.setAutoValidate(true);
		name.addValidator(new MinLengthValidator(3));
		name.addValidator(new MaxLengthValidator(30));
		name.addValidator(new RegExValidator("^[^!#$%&'*+/=?^`{|}~]+$",
				"Field match illegal symbol"));
		p.add(new FieldLabel(name, "Name"), new VerticalLayoutData(400, 1));

		// Member of Group
		groupsComboBoxWidget = new GroupsComboBox(groupId);
		p.add(new FieldLabel(groupsComboBoxWidget.asWidget(), "Parent Group"), new VerticalLayoutData(400, 1));
		
		if (selectedGroup != null && groupsTreeLeftMenu.getStore().getParent(selectedGroup) != null) {	
			groupsComboBoxWidget.getComboBox().setValue(groupsTreeLeftMenu.getStore().getParent(selectedGroup));
		}
		
		groupsComboBoxWidget.getComboBox().setReadOnly(readOnly);
		
		// Deprecated: member of Group
		// p = new VerticalLayoutContainer();
		// scrollPanel = new ScrollPanel();
		// scrollPanel.setHeight("100%");
		// scrollPanel.add(p);
		// p.setLayoutData(new MarginData(8));
		// tabs.add(scrollPanel, "Member of Groups");
		// p.add(groupsTreeTabMenu.asWidget(groupId, false, true));
		// groupsTreeTabMenu.getTree().getSelectionModel().setLocked(true);
		// groupsTreeTabMenu.getTree().setEnabled(!notEditable);

		// Users from Group
		if (groupId != null) {
			p = new VerticalLayoutContainer();
			scrollPanel = new ScrollPanel();
			scrollPanel.setHeight("100%");
			scrollPanel.add(p);
			p.setLayoutData(new MarginData(8));
			tabs.add(scrollPanel, "Users from Group");
			p.add(userlist.usersListWidget(groupId));
			getGroupProperties(groupId);
			userlist.getTree().getSelectionModel().setLocked(true);
		}
	}

	// Get Group properties
	public void getGroupProperties(String groupId) {

		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();

		if (groupAdminSvc == null) {
			groupAdminSvc = GWT.create(GroupsService.class);
		}

		groupAdminSvc.getGroupProperties(groupId, new RpcCallback<BpGroupDTO>(
				rpcWait.getBox()) {

			@Override
			public void onSuccess(BpGroupDTO result) {

				name.setText(result.getName());
				rpcWait.deleteWindow();

			}
		});

	}

	/**
	 * Save Group properties
	 * 
	 * @param name
	 */
	public void saveGroupProperties(final String name) {

		if (groupAdminSvc == null) {
			groupAdminSvc = GWT.create(GroupsService.class);
		}

		try {
			bpGroupForSave.setName(name);
			
			if (groupsComboBoxWidget.getSelectedItem() == null) {
				bpGroupForSave.setParentId(null);
			} else {
				bpGroupForSave.setParentId(groupsComboBoxWidget.getSelectedItem().getId());
			}
			
			if (newGroup != true && selectedGroup.getId() != null) {
				bpGroupForSave.setId(selectedGroup.getId());
			} 

			final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
			rpcWait.showWindow();

			groupAdminSvc.addOrUpdateGroup(bpGroupForSave,
					new RpcCallback<BpGroupDTO>(rpcWait.getBox()) {

						@Override
						public void onSuccess(BpGroupDTO result) {
							if (result.getId() != null && bpGroupForSave.getId() == null) {
								bpGroupForSave.setId(result.getId());
							}
							rpcWait.deleteWindow();
							com.google.gwt.user.client.Window.alert(result.getResultOfTransaction());
							groupsTreeLeftMenu.refreshStore();
							groupsComboBoxWidget.refreshGroupsList(bpGroupForSave.getId());
						}
					});
		} catch (Exception e) {
			com.google.gwt.user.client.Window
					.alert("Something went wrong! Please try again later...");
			e.printStackTrace();
		}

	}

	public BpGroupDTO getParentByChildId(String groupChildId) {
		return null;
	}

	public Widget getButtonBar(boolean newGroup) {

		editModeToRO = new TextButton("Read Only Mode");
		editModeToEdit = new TextButton("Edit Mode");

		buttonBar = new ButtonBar();
		save = new TextButton("Save");
		cancel = new TextButton("Cancel");

		editModeToRO.setIcon(UserPropMenuImages.INSTANCE.menu_show());
		editModeToEdit.setIcon(UserPropMenuImages.INSTANCE.menu_show());
		cancel.setIcon(UserPropMenuImages.INSTANCE.list_items());
		save.setIcon(UserPropMenuImages.INSTANCE.folder());

		if (newGroup) {
			buttonBar.add(save, new BoxLayoutData());
		} else {
			buttonBar.add(editModeToRO);
			buttonBar.add(editModeToEdit);
			buttonBar.add(save, new BoxLayoutData());
			buttonBar.add(cancel, new BoxLayoutData());
			editModeToRO.setVisible(false);
			cancel.setVisible(false);
			save.setVisible(false);
		}

		save.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if ((name.getText() == null) || (name.getText().isEmpty())
						|| !name.isValid()) {
					name.isValid(false);
					com.google.gwt.user.client.Window
							.alert("Please type Group Name!");
					name.setAllowBlank(false);
				} else {
					saveGroupProperties(name.getText());
				}
			}
		});

		editModeToEdit.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if (readOnly) {
					setNotEditable(false);
					//Buttons
					setButtonsStatus (readOnly);
					//Fields
					setFieldsStatus(readOnly);
				}
			}
		});

		editModeToRO.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if (!readOnly) {
					setNotEditable(true);
					//Buttons
					setButtonsStatus (readOnly);
					//Fields
					setFieldsStatus(readOnly);
				}
			}
		});

		cancel.addSelectHandler(new SelectHandler() {
			@Override
			public void onSelect(SelectEvent event) {
				if (!readOnly) {
					setNotEditable(true);
					//Buttons
					setButtonsStatus (readOnly);
					//Fields
					setFieldsStatus(readOnly);
				}
			}
		});

		return buttonBar;
	}

	public void setNotEditable(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public void show() {
		window.show();
	}
	
	public void setFieldsStatus (boolean readOnly) {
		//Fields
		name.setReadOnly(readOnly);
		userlist.getTree().getSelectionModel().setLocked(readOnly);
//		groupsTreeTabMenu.getTree().setEnabled(false);
		groupsComboBoxWidget.getComboBox().setReadOnly(readOnly);
		tabs.forceLayout();
	}
	
	public void setButtonsStatus (boolean readOnly) {
		//Fields
		editModeToEdit.setVisible(readOnly);
		editModeToRO.setVisible(!readOnly);
		save.setVisible(!readOnly);
		cancel.setVisible(!readOnly);
		buttonBar.forceLayout();
	}
}
