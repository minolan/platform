/**
 * Sencha GXT 3.0.1 - Sencha for GWT
 * Copyright(c) 2007-2012, Sencha, Inc.
 * licensing@sencha.com
 *
 * http://www.sencha.com/products/gxt/license/
 */
package com.bplatform.client.mainModule.ui.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface UserPropMenuImages extends ClientBundle {

  public UserPropMenuImages INSTANCE = GWT.create(UserPropMenuImages.class);

  @Source("UserPropMenu/add16.gif")
  ImageResource add16();
  
  @Source("UserPropMenu/folder16.png")
  ImageResource folder16();
  
  @Source("UserPropMenu/folder48.png")
  ImageResource folder48();
  
  @Source("UserPropMenu/folder16gray.png")
  ImageResource folder16gray();

  @Source("UserPropMenu/add24.gif")
  ImageResource add24();

  @Source("UserPropMenu/add32.gif")
  ImageResource add32();
  
  @Source("UserPropMenu/table.png")
  ImageResource table();

  @Source("UserPropMenu/application_side_list.png")
  ImageResource side_list();
  
  @Source("UserPropMenu/list.gif")
  ImageResource list();

  @Source("UserPropMenu/application_form.png")
  ImageResource form();

  @Source("UserPropMenu/connect.png")
  ImageResource connect();

  @Source("UserPropMenu/user_add.png")
  ImageResource user_add();

  @Source("UserPropMenu/user_delete.png")
  ImageResource user_delete();

  @Source("UserPropMenu/accordion.gif")
  ImageResource accordion();

  @Source("UserPropMenu/add.gif")
  ImageResource add();

  @Source("UserPropMenu/delete.gif")
  ImageResource delete();

  @Source("UserPropMenu/calendar.gif")
  ImageResource calendar();

  @Source("UserPropMenu/menu-show.gif")
  ImageResource menu_show();

  @Source("UserPropMenu/list-items.gif")
  ImageResource list_items();

  @Source("UserPropMenu/album.gif")
  ImageResource album();

  @Source("UserPropMenu/text.png")
  ImageResource text();

  @Source("UserPropMenu/plugin.png")
  ImageResource plugin();
  
  @Source("UserPropMenu/music.png")
  ImageResource music();
  
  
  @Source("UserPropMenu/user.png")
  ImageResource user();
  
  @Source("UserPropMenu/user_kid.png")
  ImageResource userKid();
  
  @Source("UserPropMenu/user_female.png")
  ImageResource userFemale();
  
  @Source("UserPropMenu/css.png")
  ImageResource css();
  
  @Source("UserPropMenu/java.png")
  ImageResource java();
  
  @Source("UserPropMenu/text.png")
  ImageResource json();
  
  @Source("UserPropMenu/html.png")
  ImageResource html();
  
  @Source("UserPropMenu/xml.png")
  ImageResource xml();
  
  @Source("UserPropMenu/folder.png")
  ImageResource folder();
}
