package com.bplatform.client.mainModule.ui;

import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.gargoylesoftware.htmlunit.protocol.about.Handler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;

public class QueueSelector extends BpElementWidget {

	public QueueSelector() {
		window.setHeight(665);
		window.setWidth(805);
		window.add(buildQueuePanel());
	}

	private FlowPanel buildQueuePanel() {
		FlowPanel panel = new FlowPanel();
		for (int i = 0; i<8; i++){
			panel.add(new QueueButton("Услуга №"+i, "q"+i));	
		}
		return panel;
	}
	
	private class QueueButton extends Button{
		QueueButton(){
			super();
			this.addStyleName("QueueButton");
		}
		QueueButton(String caption, String style){
			this();
			this.setText(caption);
			this.addStyleName(style);
		}
		
	}
}
