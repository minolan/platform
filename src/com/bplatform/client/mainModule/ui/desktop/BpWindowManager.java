package com.bplatform.client.mainModule.ui.desktop;

import java.util.List;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;

public interface BpWindowManager {

	public abstract FlowPanel getDesktop();

	public abstract void setDesktop(FlowPanel desktop);

	public abstract void register(Widget widget);

	public abstract List<Widget> getWindows();

	public abstract void bringToFront(Window window);

}