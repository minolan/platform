package com.bplatform.client.mainModule.ui.desktop;

import com.google.gwt.user.client.ui.Widget;

public interface BpMenuButton {

	public abstract Boolean getFixed();

	public abstract void setFixed(Boolean fixed);

	public abstract Widget asWidget();

	public abstract void onButtonPress();

	public abstract void setBtnImg();

	public abstract void setButtonStyle(String style);

	public abstract void setVisibleCaption(Boolean isVisible);

	public abstract void setCaption(String caption);

	public abstract void setDescryption(String descryption);

	public abstract BpWindowManager getWindowManager();

	public abstract void setWindowManager(BpWindowManager windowManager);
	
	public abstract void setBpDesktop(BpDesktop bpDesktop);


}