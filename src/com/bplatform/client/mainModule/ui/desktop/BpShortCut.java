package com.bplatform.client.mainModule.ui.desktop;

import com.google.gwt.user.client.ui.IsWidget;

public interface BpShortCut extends IsWidget {

	public abstract BpElement getBpElement();

	public abstract void setBpElement(BpElement bpElement);

	public abstract BpElementInformation getBpElementInformation();

	public abstract void setBpElementInformation(BpElementInformation bpElementInformation);

	public abstract BpDesktop getBpDesktop();

	public abstract void setBpDesktop(BpDesktop bpDesktop);

}