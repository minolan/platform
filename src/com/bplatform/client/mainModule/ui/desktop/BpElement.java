package com.bplatform.client.mainModule.ui.desktop;

import com.google.gwt.user.client.ui.IsWidget;
import com.sencha.gxt.widget.core.client.Window;

public interface BpElement extends IsWidget {

	public abstract void show();

	public abstract BpMenuButton getBpMenuButton();

	public abstract void setBpMenuButton(BpMenuButton bpMenuButton);

	public abstract void setActive();

	public abstract void setDisActive();

	public abstract Window getWindow();

	public abstract BpDesktop getBpDesktop();

	public abstract void setBpDesktop(BpDesktop bpDesktop);
	public abstract void maximize()	;
	public abstract void minimize();
	public abstract void restore();
	public abstract void fixed();
}