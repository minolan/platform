package com.bplatform.client.mainModule.ui.desktop;

import com.google.gwt.user.client.ui.FlowPanel;

public interface BpDesktop  {

	public abstract FlowPanel getWorkflow();

	public abstract void setWorkflow(FlowPanel workflow);

	public abstract BpMainPanel getBpMainPanel();

	public abstract void setBpMainPanel(BpMainPanel bpMainPanel);

	public abstract BpWindowManager getWindowManager();

	public abstract void setWindowManager(BpWindowManager windowManager);

}