package com.bplatform.client.mainModule.ui.desktop;

public interface BpQuickLaunch {

	public abstract BpWindowManager getWindowManager();

	public abstract void setWindowManager(BpWindowManager windowManager);

	public abstract BpDesktop getBpDesktop();

	public abstract void setBpDesktop(BpDesktop bpDesktop);

}