package com.bplatform.client.mainModule.ui.desktop;

public interface BpTaskBar {

	public abstract BpWindowManager getWindowManager();

	public abstract void setWindowManager(BpWindowManager windowManager);

	public abstract void add(BpMenuButton bpMenuButton);

}