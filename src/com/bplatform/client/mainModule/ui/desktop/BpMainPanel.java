package com.bplatform.client.mainModule.ui.desktop;

public interface BpMainPanel {

	public abstract BpWindowManager getWindowManager();

	public abstract void setWindowManager(BpWindowManager windowManager);

	public abstract BpQuickLaunch getBpQuickLunch();

	public abstract void setBpQuickLunch(BpQuickLaunch bpQuickLunch);

	public abstract BpTaskBar getBpTaskBar();

	public abstract void setBpTaskBar(BpTaskBar bpTaskBar);

	public abstract BpDesktop getBpDesktop();

	public abstract void setBpDesktop(BpDesktop bpDesktop);

}