package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.desktop.BpWindowManager;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.WindowManager;
/*
 * @author Blind Pew
 * @version 0.0.1
 * 
 * 2013.09.17
 * 
 * */

public class BpWindowManagerImpl extends WindowManager implements BpWindowManager {
	private  FlowPanel desktop;
	public BpWindowManagerImpl(FlowPanel workflow) {
		desktop=workflow;
	}

	@Override
	public FlowPanel getDesktop() {
		return desktop;
	}

	@Override
	public void setDesktop(FlowPanel desktop) {
		this.desktop = desktop;
	}
	
	public void register(Widget widget) {
		super.register(widget);
	}

	@Override
	public void bringToFront(Window window) {
		super.bringToFront(window);
		
	}
}
