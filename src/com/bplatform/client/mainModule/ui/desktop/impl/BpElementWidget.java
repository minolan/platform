package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.desktop.BpDesktop;
import com.bplatform.client.mainModule.ui.desktop.BpElement;
import com.bplatform.client.mainModule.ui.desktop.BpElementInformation;
import com.bplatform.client.mainModule.ui.desktop.BpMenuButton;
import com.bplatform.client.mainModule.ui.desktop.BpWindowManager;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.event.ActivateEvent;
import com.sencha.gxt.widget.core.client.event.ActivateEvent.ActivateHandler;
import com.sencha.gxt.widget.core.client.event.BeforeHideEvent;
import com.sencha.gxt.widget.core.client.event.BeforeHideEvent.BeforeHideHandler;
import com.sencha.gxt.widget.core.client.event.BeforeShowEvent;
import com.sencha.gxt.widget.core.client.event.BeforeShowEvent.BeforeShowHandler;
import com.sencha.gxt.widget.core.client.event.DeactivateEvent;
import com.sencha.gxt.widget.core.client.event.DeactivateEvent.DeactivateHandler;
import com.sencha.gxt.widget.core.client.event.MaximizeEvent;
import com.sencha.gxt.widget.core.client.event.MaximizeEvent.MaximizeHandler;
import com.sencha.gxt.widget.core.client.event.MinimizeEvent;
import com.sencha.gxt.widget.core.client.event.MinimizeEvent.MinimizeHandler;
import com.sencha.gxt.widget.core.client.event.RestoreEvent;
import com.sencha.gxt.widget.core.client.event.RestoreEvent.RestoreHandler;
import com.sencha.gxt.widget.core.client.event.ShowEvent;
import com.sencha.gxt.widget.core.client.event.ShowEvent.ShowHandler;

/**
 * Implement in widgets that are elements of platform and should be accessed
 * through main menu.
 * 
 * @author Minolan, Blind Pew
 * 
 */
public abstract class BpElementWidget extends Widget implements BpElement {

	protected BpDesktop bpDesktop;
	private BpElementInformation bpElementInformation;

	private BpMenuButton bpMenuButton;

	protected BpWindowManager bpWindowManager;
	protected Window window;

	public BpElementWidget() {
		setWindow(new Window()) ;
	}

	@Override
	public Widget asWidget() {
		return window;
	}
	
	@Override
	public void show() {
		window.show();
	}
	
	private void correctSize() {
		// if (windowFrame.isMaximized()) {
		// windowFrame.setPosition(68, 2);
		// windowFrame.setWidth(windowFrame.getElement().getWidth(false) - 4);
		// windowFrame.setHeight(RootLayoutPanel.get().getOffsetHeight() +
		// "px");
		// }
	}

	public void fixed() {

	}

	public BpDesktop getBpDesktop() {
		return bpDesktop;
	}

	public BpElementInformation getBpElementInformation() {
		return bpElementInformation;
	}

	public BpMenuButton getBpMenuButton() {
		return bpMenuButton;
	}

	public Window getWindow() {
		return window;
	}

	public void maximize() {
		window.maximize();
	}

	public void minimize() {
		window.minimize();
	}

	public void restore() {
		window.restore();
	}

	public void setActive() {
		window.setActive(true);
	}

	public void setBpDesktop(BpDesktop bpDesktop) {
		this.bpDesktop = bpDesktop;
		if (window != null) {
			window.setContainer(bpDesktop.getWorkflow().getElement());
		}
	}

	public void setBpElementInformation(BpElementInformation bpElementInformation) {
		this.bpElementInformation = bpElementInformation;
	}

	public void setBpMenuButton(BpMenuButton bpMenuButton) {
		this.bpMenuButton = bpMenuButton;
		this.bpWindowManager = bpMenuButton.getWindowManager();
	}

	public void setDisActive() {
		window.setActive(false);
	}

	protected void setWindow(Window window) {
		this.window = window;
		window.setHeight(300);
		window.setWidth(485);
//		windowFrame.setContainer(bpDesktop.getWorkflow().getElement());

//		Button btnWindowMenu = new Button();
//		window.getHeader().addTool(btnWindowMenu);
//		
		window.setMaximizable(true);
		window.setShadow(true);
		window.setBlinkModal(true);
		
		window.getDraggable().setUseProxy(true);
		window.addRestoreHandler(new RestoreHandler() {

			@Override
			public void onRestore(RestoreEvent event) {
				GWT.log("onRestore");
				correctSize();

			}
		});

		window.addBeforeHideHandler(new BeforeHideHandler() {
			@Override
			public void onBeforeHide(BeforeHideEvent event) {
				GWT.log("onBeforeHide");
				if (BpElementWidget.this.bpMenuButton.getFixed()) {
					BpElementWidget.this.bpMenuButton.asWidget().removeStyleName("active");
					BpElementWidget.this.bpMenuButton.asWidget().removeStyleName("focused");
				} else {
					BpElementWidget.this.bpMenuButton.asWidget().removeFromParent();
				}
			}
		});

		window.addBeforeShowHandler(new BeforeShowHandler() {

			@Override
			public void onBeforeShow(BeforeShowEvent event) {
				GWT.log("BeforeShowEvent");
				BpElementWidget.this.bpMenuButton.asWidget().addStyleName("active");
				correctSize();
			}
		});

		window.addShowHandler(new ShowHandler() {

			@Override
			public void onShow(ShowEvent event) {
				GWT.log("ShowHandler");
				correctSize();
			}
		});

		window.addActivateHandler(new ActivateHandler<Window>() {

			@Override
			public void onActivate(ActivateEvent<Window> event) {
				GWT.log("onActivate");
				BpElementWidget.this.bpMenuButton.asWidget().addStyleName("focused");
				correctSize();
			}

		});

		window.addDeactivateHandler(new DeactivateHandler<Window>() {

			@Override
			public void onDeactivate(DeactivateEvent<Window> event) {
				GWT.log("onDeactivate");
				BpElementWidget.this.bpMenuButton.asWidget().removeStyleName("focused");

			}
		});

		window.addMinimizeHandler(new MinimizeHandler() {

			@Override
			public void onMinimize(MinimizeEvent event) {
				GWT.log("onMinimize");
				BpElementWidget.this.window.setVisible(false);
				BpElementWidget.this.bpMenuButton.asWidget().addStyleName("active");
			}
		});

		window.addMaximizeHandler(new MaximizeHandler() {

			@Override
			public void onMaximize(MaximizeEvent event) {
				GWT.log("onMaximize");
				correctSize();
			}
		});

	}



}
