package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.desktop.BpDesktop;
import com.bplatform.client.mainModule.ui.desktop.BpElement;
import com.bplatform.client.mainModule.ui.desktop.BpMenuButton;
import com.bplatform.client.mainModule.ui.desktop.BpWindowManager;
import com.bplatform.client.mainModule.ui.desktop.impl.BpButtonContextMenuPanel.CommandType;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class BpMenuButtonImpl implements IsWidget, BpMenuButton {
	private BpElement elm;
	private BpWindowManager windowManager;

	private BpButtonContextMenuPanel contextMenu = new BpButtonContextMenuPanel();

	FlowPanel shortCut = new FlowPanel();
	FlowPanel marker = new FlowPanel();
	Label caption = new Label();
	Label descryption = new Label();

	// TODO: to enum, fixe
	Boolean fixed = false;
	private Button btn = new Button();

	@Override
	public Boolean getFixed() {
		return fixed;
	}

	@Override
	public void setFixed(Boolean fixed) {
		this.fixed = fixed;
	}

	@Override
	public Widget asWidget() {

		

		return shortCut;

	}

	public <T extends BpMenuButton> BpMenuButtonImpl(BpElement elm) {
		super();
		elm.asWidget();
		this.elm = elm;
	}

	public BpMenuButtonImpl(BpElement bpElement, BpWindowManager bpWindowManager) {
		super();
	//	bpElement.asWidget();
		this.elm = bpElement;
		this.windowManager = bpWindowManager;

		
		shortCut.addStyleName("menuShortCut");
		marker.setStyleName("marker");
		btn.addStyleName("bg");
		caption.addStyleName("caption");
		shortCut.add(btn);
		descryption.addStyleName("descryption");

		btn.addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent event) {
				
				
				if (event.getNativeButton() == NativeEvent.BUTTON_RIGHT) {
					showContextMenu();
				} else {
					onButtonPress();
				}
//				GWT.log(event.getSource().toString());
			}
		});

		shortCut.add(marker);
		shortCut.add(caption);
		shortCut.add(descryption);
		shortCut.add(contextMenu);

		btn.addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
//				GWT.log("Blur --- " + event.toString());
				hideContext();
//				GWT.log("Blur --- " + event.toString());
			}
		});



		btn.addDomHandler(new ContextMenuHandler() {
			@Override
			public void onContextMenu(ContextMenuEvent event) {
				event.preventDefault();
				event.stopPropagation();

			}
		}, ContextMenuEvent.getType());
		
		contextMenu.addMenuItem("Свернуть", new MouseUpHandler() {

			@Override
			public void onMouseUp(MouseUpEvent event) {
				elm.minimize();

			}
		}, CommandType.Window);

		contextMenu.addMenuItem("Развернуть", new MouseUpHandler() {

			@Override
			public void onMouseUp(MouseUpEvent event) {
				elm.maximize();

			}
		}, CommandType.Window);

		contextMenu.addMenuItem("Восстановить", new MouseUpHandler() {
			
			@Override
			public void onMouseUp(MouseUpEvent event) {
				elm.restore();
				
			}
		}, CommandType.Window);
		
		contextMenu.addMenuItem("Прикрепить", new MouseUpHandler() {

			@Override
			public void onMouseUp(MouseUpEvent event) {
				elm.fixed();

			}
		}, CommandType.Panel);
		
		
		
	}

	
	public void onButtonPress() {
//		GWT.log("on button press 1");
//		elm.getWindow().setContainer(windowManager.getDesktop().getElement());
		
		if (!windowManager.getWindows().contains(elm.getWindow())) {
			windowManager.register(elm.getWindow());
		}
		elm.show();
		windowManager.bringToFront(elm.getWindow());
		//elm.getWindow().forceLayout();
	}

	protected void showContextMenu() {
		contextMenu.addStyleName("showPopUp");
	}

	protected void hideContext() {
		contextMenu.removeStyleName("showPopUp");
	}

	<T extends BpElement> void setBpElement(T elm) {
		this.elm = elm;
	}

	public void setBtnImg() {

	}

	public void setButtonStyle(String style) {
		shortCut.addStyleName(style);
	}

	public void setVisibleCaption(Boolean isVisible) {
		caption.setVisible(isVisible);
	}

	public void setCaption(String caption) {
		this.caption.setText(caption);

	}

	public void setDescryption(String descryption) {
		this.descryption.setText(descryption);
	}

	public BpWindowManager getWindowManager() {
		return windowManager;
	}

	public void setWindowManager(BpWindowManager windowManager) {
		this.windowManager = windowManager;
	}

	@Override
	public void setBpDesktop(BpDesktop bpDesktop) {

	}

}
