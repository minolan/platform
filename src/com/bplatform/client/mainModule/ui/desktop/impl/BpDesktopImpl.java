package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.desktop.BpDesktop;
import com.bplatform.client.mainModule.ui.desktop.BpMainPanel;
import com.bplatform.client.mainModule.ui.desktop.BpWindowManager;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Window;

/*
 * @author Blind Pew
 * @version 0.0.1
 * 
 * 2013.09.17
 * 
 * */

public class BpDesktopImpl extends DockLayoutPanel implements BpDesktop {
	private FlowPanel workflow;
	private BpMainPanel bpMainPanel;
	private BpWindowManager windowManager;

	public BpDesktopImpl() {
		super(Unit.PX);
		workflow = new FlowPanel();
		
		windowManager = new BpWindowManagerImpl(workflow);
		windowManager.setDesktop(workflow);
		bpMainPanel = new BpMainPanelImpl(windowManager, this);
		
		workflow.add(new BpWatch());
		
		/*TODO: maybe moved to the class*/
		workflow.setStyleName("workflow");

//		this.setWestWidget((IsWidget) bpMainPanel, new BorderLayoutData(66));
//		this.setCenterWidget(workflow);	
		
		
		this.addWest((Widget) bpMainPanel, 66);
		this.add(workflow);
		
		 
		/*
		workflow.setHeight(RootLayoutPanel.get().getOffsetHeight()+"px");
		
		workflow.addStyleName(RootLayoutPanel.get().getOffsetHeight()+"px");
		*/
		
		this.addStyleName("bm_desktop_container");
		
		/*this.getElement().removeAttribute("style");*/
	}

//	public BpDesktopImpl(BorderLayoutAppearance appearance) {
//		super(appearance);
//	}

	
	public FlowPanel getWorkflow() {
		return workflow;
	}


	
	public void setWorkflow(FlowPanel workflow) {
		this.workflow = workflow;
	}

	
	
	
	@Override
	public void onResize() {
		super.onResize();
		for (Widget widget:windowManager.getWindows()){
			if (widget.getAbsoluteLeft()+widget.getOffsetWidth()>workflow.getOffsetWidth()){
				widget.getElement().setAttribute("left", (workflow.getOffsetWidth()-widget.getOffsetWidth())+"px" );
				
				
			}
			GWT.log("---");
		}
	}

	@Override
	public BpMainPanel getBpMainPanel() {
		return bpMainPanel;
	}

	
	public void setBpMainPanel(BpMainPanelImpl bpMainPanel) {
		this.bpMainPanel = bpMainPanel;
	}

	
	@Override
	public BpWindowManager getWindowManager() {
		return windowManager;
	}

	
	@Override
	public void setWindowManager(BpWindowManager windowManager) {
		this.windowManager = windowManager;
	}

	@Override
	public void setBpMainPanel(BpMainPanel bpMainPanel) {
		// TODO Auto-generated method stub
		
	}

	
	
}
