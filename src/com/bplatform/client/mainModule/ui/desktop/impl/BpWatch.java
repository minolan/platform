package com.bplatform.client.mainModule.ui.desktop.impl;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class BpWatch implements IsWidget {
	private FlowPanel container = new FlowPanel();
	private Label lTime = new Label();
	private	Label lDay =  new Label();
	private	Label lDate = new Label();
	private	Label lWDay = new Label();
	public BpWatch() {
		container.setStyleName("bp-widget watch");
		
		lTime.setStyleName("time");
		lDay.setStyleName("dayNum");
		lWDay.setStyleName("dayWeek");
		lDate.setStyleName("date");
		
//		Date currentDateTime=new Date();

//		Date date = new Date();

		
		lDate.setText(DateTimeFormat.getFormat("MMMM yyyy").format( new Date()));
		lDay.setText(DateTimeFormat.getFormat("dd").format( new Date()));
		lWDay.setText(DateTimeFormat.getFormat("EEEE").format( new Date()));
		lTime.setText(DateTimeFormat.getFormat("HH:mm").format( new Date()));
		
		container.add(lTime);
		container.add(lDay);
		container.add(lDate);
		container.add(lWDay);
		
		final Timer timer = new Timer() {
	        @Override
	        public void run() {
	        	refresh();
	        }
	    };
	 
	    timer.scheduleRepeating(5000);
	}

	@Override
	public Widget asWidget() {
		return container;
	}
	
	public void refresh(){
		lDate.setText(DateTimeFormat.getFormat("MMMM yyyy").format( new Date()));
		lDay.setText(DateTimeFormat.getFormat("dd").format( new Date()));
		lWDay.setText(DateTimeFormat.getFormat("EEEE").format( new Date()));
		lTime.setText(DateTimeFormat.getFormat("HH:mm").format( new Date()));
	}

}
