package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.desktop.BpMenuButton;
import com.bplatform.client.mainModule.ui.desktop.BpTaskBar;
import com.bplatform.client.mainModule.ui.desktop.BpWindowManager;
import com.google.gwt.user.client.ui.FlowPanel;


public class BpTaskBarImpl extends FlowPanel implements BpTaskBar {
	/*
	 * @author Blind Pew
	 * @version 0.0.1
	 * 
	 * TaskBar - 
	 * for temporary buttons
	 * 
	 * 2013.09.17
	 * 
	 * */
	private BpWindowManager windowManager;

	public BpTaskBarImpl() {
	}

	public BpTaskBarImpl(BpWindowManager windowManager) {
		super();
		this.setWindowManager(windowManager);
	}

	@Override
	public BpWindowManager getWindowManager() {
		return windowManager;
	}
	
	@Override
	public void setWindowManager(BpWindowManager windowManager) {
		this.windowManager = windowManager;
	}

	@Override
	public void add(BpMenuButton bpMenuButton) {
		super.add(bpMenuButton.asWidget());
	}
	
	

}
