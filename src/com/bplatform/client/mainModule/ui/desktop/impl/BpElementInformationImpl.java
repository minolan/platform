package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.desktop.BpElementInformation;

/*
 * @author Blind Pew
 * @version 0.0.1
 * 
 * Class instead annotations
 * If you know better way, you are welcome do it
 * 
 * 2013.09.17
 * 
 * */

public class BpElementInformationImpl implements BpElementInformation {
	String caption;
	String descryption;
	String style;

	public BpElementInformationImpl(String caption, String descryption, String style) {
		super();
		this.caption = caption;
		this.descryption = descryption;
		this.style = style;
	}

	@Override
	public String getCaption() {
		return caption;
	}

	@Override
	public void setCaption(String caption) {
		this.caption = caption;
	}

	@Override
	public String getDescryption() {
		return descryption;
	}

	@Override
	public void setDescryption(String descryption) {
		this.descryption = descryption;
	}

	@Override
	public String getStyle() {
		return style;
	}

	@Override
	public void setStyle(String style) {
		this.style = style;
	}
}
