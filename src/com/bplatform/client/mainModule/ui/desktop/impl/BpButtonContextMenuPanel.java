package com.bplatform.client.mainModule.ui.desktop.impl;

import java.util.ArrayList;

import com.bplatform.client.mainModule.ui.desktop.BpElement;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.widget.client.TextButton;

public class BpButtonContextMenuPanel extends FlowPanel {

	ArrayList<Button> alPanelCommands;
	ArrayList<Button> alWindowCommands;
	ArrayList<Button> alApplicationCommands;

	BpElement bpElement;
	
	FlowPanel fpPanelCommands = new FlowPanel();
	FlowPanel fpWindowCommands = new FlowPanel();
	FlowPanel fpApplicationCommands = new FlowPanel();

	public BpButtonContextMenuPanel() {
		this.setStyleName("BpButtonContextMenu");
		fpPanelCommands.setStyleName("section top");
		fpApplicationCommands.setStyleName("section middle");
		fpWindowCommands.setStyleName("section bottom");

		
		
		this.add(fpPanelCommands);
		this.add(fpApplicationCommands);
		this.add(fpWindowCommands);
	}

	public void addMenuItem(String caption, EventHandler mouseUpHandler, CommandType commandType) {
		TextButton item = new TextButton();
		item.setWidth("140px");
		item.setText(caption);
		item.addMouseUpHandler((MouseUpHandler) mouseUpHandler);
		item.addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent event) {
				BpButtonContextMenuPanel.this.removeStyleName("showPopUp");
			}
		});
		
		switch (commandType) {
		case Application:
			fpApplicationCommands.add(item);
			break;
		case Panel:
			fpPanelCommands.add(item);
			break;
		case Window:
			fpWindowCommands.add(item);
			break;
		default:
			break;
		}
		
	}

	public enum CommandType {
		Window, Panel, Application
	}
}
