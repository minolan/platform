package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.desktop.BpDesktop;
import com.bplatform.client.mainModule.ui.desktop.BpMainPanel;
import com.bplatform.client.mainModule.ui.desktop.BpQuickLaunch;
import com.bplatform.client.mainModule.ui.desktop.BpTaskBar;
import com.bplatform.client.mainModule.ui.desktop.BpWindowManager;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class BpMainPanelImpl extends FlowPanel implements BpMainPanel {
	/*
	 * @author Blind Pew
	 * 
	 * Main left panel, was divided for two parts
	 * QuickLuch - fixed buttons, 
	 * TaskBar - for temporary buttons
	 * WindowsManager - responsible for relationship with window and they events, close and set focus
	 * 
	 * 2013.09.17
	 * 
	 * */
	private BpWindowManager windowManager;
	private BpQuickLaunch bpQuickLunch;
	private BpTaskBar bpTaskBar;
	
	private BpDesktop bpDesktop;
	
	
	public BpMainPanelImpl() {
	}

	public BpMainPanelImpl(BpWindowManager windowManager, BpDesktop bpDesktop) {
		super();
		this.setWindowManager(windowManager);
		this.setStyleName("bm_menu");
		this.getElement().removeAttribute("style");
		this.setHeight("100%");
		this.getElement().setPropertyString("height:", "100%");
		
		
		
		bpQuickLunch = new BpQuickLaunchImpl(windowManager, bpDesktop);
		bpTaskBar = new BpTaskBarImpl(windowManager);
		
		this.add((Widget) bpQuickLunch);
		this.add((Widget) bpTaskBar);
		
	}


	@Override
	public BpWindowManager getWindowManager() {
		return windowManager;
	}


	@Override
	public void setWindowManager(BpWindowManager windowManager) {
		this.windowManager = windowManager;
	}


	public BpQuickLaunch getBpQuickLunch() {
		return bpQuickLunch;
	}

	public void setBpQuickLunch(BpQuickLaunch bpQuickLunch) {
		this.bpQuickLunch = bpQuickLunch;
	}


	public BpTaskBar getBpTaskBar() {
		return bpTaskBar;
	}


	public void setBpTaskBar(BpTaskBar bpTaskBar) {
		this.bpTaskBar = bpTaskBar;
	}

	@Override
	public BpDesktop getBpDesktop() {
		return bpDesktop;
	}

	@Override
	public void setBpDesktop(BpDesktop bpDesktop) {
		this.bpDesktop = bpDesktop;
	}


	
	
	
}
