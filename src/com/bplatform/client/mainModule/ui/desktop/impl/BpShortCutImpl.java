package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.BackGroundSetting;
import com.bplatform.client.mainModule.ui.FileDownloadForm;
import com.bplatform.client.mainModule.ui.FileUploadForm;
import com.bplatform.client.mainModule.ui.Settings;
import com.bplatform.client.mainModule.ui.SystemProperties;
import com.bplatform.client.mainModule.ui.desktop.BpDesktop;
import com.bplatform.client.mainModule.ui.desktop.BpElement;
import com.bplatform.client.mainModule.ui.desktop.BpElementInformation;
import com.bplatform.client.mainModule.ui.desktop.BpMenuButton;
import com.bplatform.client.mainModule.ui.desktop.BpShortCut;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

/*
 * @author Blind Pew
 * @version 0.0.1
 * 
 * 2013.09.17
 * 
 * */

public class BpShortCutImpl extends FlowPanel implements BpShortCut {

	
	private FlowPanel shortCut = new FlowPanel();
	private FlowPanel marker = new FlowPanel();
	
	private Button btn = new Button();
	
	private Label caption = new Label();
	private Label descryption = new Label();
	private String styleClass="";
	
	private BpElement bpElement; 
	private BpMenuButton bpMenuButton;
	private Class<?> classBpElement;
	private BpElementInformation bpElementInformation;
	
	private BpDesktop bpDesktop;
	
	public BpShortCutImpl(Class<?> bpElementClass, BpDesktop bpDesktop, BpElementInformation bpElementInformation) {
		super();
		//String caption, String descryption, String styleClass,
		
		classBpElement = bpElementClass;
		
		this.bpElementInformation = bpElementInformation;
//		GWT.log("classBpElement" + classBpElement.toString());
//		BpElementInformation info = classBpElement.getAnnotation(BpElementInformation.class);
		
		caption.setText(bpElementInformation.getCaption());
		descryption.setText(bpElementInformation.getDescryption());
		styleClass = bpElementInformation.getStyle();
	
		shortCut.addStyleName("wShortCut "+ styleClass);
		marker.setStyleName("marker");
		btn.addStyleName("bg");
		caption.addStyleName("caption");
		this.bpDesktop = bpDesktop;
		
		
		descryption.addStyleName("descryption");
		
//		btn.setHeight("150px");
//		btn.setWidth("150px");
		

		
		btn.addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent event) {
//				GWT.log("onButtonPress");
				onButtonPress();
			}
		});
		
		shortCut.add(btn);	
		shortCut.add(marker);
		shortCut.add(caption);
		shortCut.add(descryption);
		
		this.add(shortCut);
	
	}

	private void onButtonPress(){

		bpElement =  BpElementGWT(classBpElement);
//		bpElement = new BackGroundSetting();
		bpElement.setBpDesktop(bpDesktop);
		
		bpMenuButton = new BpMenuButtonImpl(bpElement, bpDesktop.getWindowManager());
		bpMenuButton.setCaption(bpElementInformation.getCaption());
		bpMenuButton.setDescryption(bpElementInformation.getDescryption());
		bpMenuButton.setButtonStyle(bpElementInformation.getStyle());
		
		bpElement.setBpMenuButton(bpMenuButton);
		
		bpDesktop.getBpMainPanel().getBpTaskBar().add(bpMenuButton);
		
		bpElement.show();
	}

	/*
	 * Blind Pew
	 * 
	 * 	  
	 * */
	private BpElement BpElementGWT(Class<?> cls){
		BpElement element = null;
		if (cls.equals(Settings.class)){
			element = GWT.create(Settings.class);
		}else if(cls.equals(SystemProperties.class)){
			element = GWT.create(SystemProperties.class);
		}else if(cls.equals(SystemProperties.class)){
			element = GWT.create(SystemProperties.class);
		}else if(cls.equals(BackGroundSetting.class)){
			element = GWT.create(BackGroundSetting.class);
		}else if(cls.equals(FileDownloadForm.class)){
				element = GWT.create(FileDownloadForm.class);
		}else if(cls.equals(FileUploadForm.class)){
			element = GWT.create(FileUploadForm.class);
		}
		return element;
	}
	
	
	public BpElement getBpElement() {
		return bpElement;
	}

	public void setBpElement(BpElement bpElement) {
		this.bpElement = bpElement;
	}

	public BpElementInformation getBpElementInformation() {
		return bpElementInformation;
	}

	public void setBpElementInformation(BpElementInformation bpElementInformation) {
		this.bpElementInformation = bpElementInformation;
	}

	public BpDesktop getBpDesktop() {
		return bpDesktop;
	}

	public void setBpDesktop(BpDesktop bpDesktop) {
		this.bpDesktop = bpDesktop;
	}
	
	
	



}
