package com.bplatform.client.mainModule.ui.desktop.impl;

import com.bplatform.client.mainModule.ui.BackGroundSetting;
import com.bplatform.client.mainModule.ui.FileDownloadForm;
import com.bplatform.client.mainModule.ui.FileUploadForm;
import com.bplatform.client.mainModule.ui.GroupsManagerAdapter;
import com.bplatform.client.mainModule.ui.Navigator;
import com.bplatform.client.mainModule.ui.QueueSelector;
import com.bplatform.client.mainModule.ui.Settings;
import com.bplatform.client.mainModule.ui.SystemProperties;
import com.bplatform.client.mainModule.ui.UserManagerFormAdapter;
import com.bplatform.client.mainModule.ui.UsersManagerAdapter;
import com.bplatform.client.mainModule.ui.desktop.BpDesktop;
import com.bplatform.client.mainModule.ui.desktop.BpElement;
import com.bplatform.client.mainModule.ui.desktop.BpElementInformation;
import com.bplatform.client.mainModule.ui.desktop.BpMenuButton;
import com.bplatform.client.mainModule.ui.desktop.BpQuickLaunch;
import com.bplatform.client.mainModule.ui.desktop.BpWindowManager;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.FlowPanel;

/*
 * @author Blind Pew
 * 
 * QuickLuch - 
 * 	flow panel for fixed buttons
 * 
 *  2013.09.17
 *  
 * */
public class BpQuickLaunchImpl extends FlowPanel implements BpQuickLaunch {
	private BpWindowManager windowManager;
	private BpDesktop bpDesktop;

	public BpQuickLaunchImpl() {
	}

	public BpQuickLaunchImpl(BpWindowManager windowManager, BpDesktop bpDesktop) {
		super();
		this.setWindowManager(windowManager);
		this.setBpDesktop(bpDesktop);

		this.add(newQuickListItem((BpElement) GWT.create(BackGroundSetting.class), "BackGround", "", "setting-wallpaper").asWidget());
		this.add(newQuickListItem((BpElement) GWT.create(Settings.class), "Settings", "",
				"style-app-0").asWidget());
		this.add(newQuickListItem((BpElement) GWT.create(SystemProperties.class), "System Proporties", "",
				"style-app-1").asWidget());
		this.add(newQuickListItem((BpElement) GWT.create(GroupsManagerAdapter.class), "Groups Manager", "",
				"user-grup-app1").asWidget());
//		this.add(newQuickListItem((BpElement) GWT.create(UserManagerFormAdapter.class), "User Manager", "",
//				"user-grup-app2").asWidget());
		this.add(newQuickListItem((BpElement) GWT.create(UsersManagerAdapter.class), "Users Manager", "",
				"user-grup-app2").asWidget());
		this.add(newQuickListItem((BpElement) GWT.create(FileUploadForm.class), "Upload", "",
				"style-app-3").asWidget());
		this.add(newQuickListItem((BpElement) GWT.create(FileDownloadForm.class), "Download", "",
				"style-app-2").asWidget());
		this.add(newQuickListItem((BpElement) GWT.create(QueueSelector.class), "Test", "",
				"style-app-4").asWidget());
		
		this.getElement().setId("menuContainer");
	}

	private BpMenuButton newQuickListItem(BpElement element, String caption, String desc,
			String classStyleName) {

		BpElementInformation info = new BpElementInformationImpl(caption, desc, classStyleName);
		
//		GWT.log(bpDesktop.toString());
		element.setBpDesktop(bpDesktop);
//		GWT.log("--------");
//		GWT.log("--------");
//		GWT.log(element.asWidget().toString());
//		GWT.log("--------");
//		GWT.log(bpDesktop.getWorkflow().getElement().toString());
//		GWT.log("--------");
//		GWT.log("--------");
		
//		((Window) element.asWidget()).setContainer(bpDesktop.getWorkflow().getElement());
		
		BpMenuButton button = new BpMenuButtonImpl(element, windowManager);
		button.setFixed(true);
		button.setButtonStyle(info.getStyle());
		button.setCaption(info.getCaption());
		element.setBpMenuButton(button);

		return button;
	}

	@Override
	public BpWindowManager getWindowManager() {
		return windowManager;
	}

	@Override
	public void setWindowManager(BpWindowManager windowManager) {
		this.windowManager = windowManager;
	}

	@Override
	public BpDesktop getBpDesktop() {
		return bpDesktop;
	}

	@Override
	public void setBpDesktop(BpDesktop bpDesktop) {
		this.bpDesktop = bpDesktop;
	}

}
