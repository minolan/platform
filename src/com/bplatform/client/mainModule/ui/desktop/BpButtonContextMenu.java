package com.bplatform.client.mainModule.ui.desktop;

public interface BpButtonContextMenu {
	public void show();
	public void hide();
}
