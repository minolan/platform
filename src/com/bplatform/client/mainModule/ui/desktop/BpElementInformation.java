package com.bplatform.client.mainModule.ui.desktop;

public interface BpElementInformation {

	public abstract String getCaption();

	public abstract void setCaption(String caption);

	public abstract String getDescryption();

	public abstract void setDescryption(String descryption);

	public abstract String getStyle();

	public abstract void setStyle(String style);

}