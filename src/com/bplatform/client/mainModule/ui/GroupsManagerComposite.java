package com.bplatform.client.mainModule.ui;

import java.util.Map;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.bpShared.platform.RpcCallback;
import com.bplatform.client.mainModule.services.GroupsService;
import com.bplatform.client.mainModule.services.GroupsServiceAsync;
import com.bplatform.client.mainModule.ui.widgets.GroupsTree;
import com.bplatform.client.mainModule.ui.widgets.RpcWaitingWindow;
import com.bplatform.client.mainModule.ui.resources.UserPropMenuImages;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.dom.ScrollSupport.ScrollMode;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.ContentPanel.ContentPanelAppearance;
import com.sencha.gxt.widget.core.client.Dialog.PredefinedButton;
import com.sencha.gxt.widget.core.client.FramedPanel.FramedPanelAppearance;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.HideEvent.HideHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;

public class GroupsManagerComposite extends ResizeComposite {
	
	// private Button addGroupsButton = new Button("Add Group");
	// private Button removeGroupButton = new Button("Remove Group");
	private GroupsTree checkBoxTree = new GroupsTree();
	private SplitLayoutPanel splitPanelFull = new SplitLayoutPanel();
	private VerticalLayoutContainer splitPanelLeft = new VerticalLayoutContainer();
	//Temp
	private VerticalLayoutContainer splitPanelRight;
	
	ContentPanel contentPanelTitleLeft = new ContentPanel(GWT.<ContentPanelAppearance> create(FramedPanelAppearance.class));
	ContentPanel contentPanelTitleRight = new ContentPanel(GWT.<ContentPanelAppearance> create(FramedPanelAppearance.class));
	private ScrollPanel scrollPanelLeft = new ScrollPanel();
	
	private TextButton addGroupButton = new TextButton("Add");
	private TextButton refreshGroupButton = new TextButton("Refresh");
	private TextButton removeGroupButton = new TextButton("Remove");
	
//	private Widget groupsTree;
	
	private GroupsServiceAsync groupsPropertiesSvc = GWT
			.create(GroupsService.class);
		
	public GroupsManagerComposite() {

//		groupsTree = checkBoxTree.groupsWidget(null, true, false);
		//Fill Left Scroll Panel
		scrollPanelLeft.add(checkBoxTree.groupsWidget(null, true, false));
		scrollPanelLeft.setHeight("100%");
	
		//Fill Left Content Panel
		contentPanelTitleLeft.add(scrollPanelLeft);
		contentPanelTitleLeft.setHeaderVisible(false);
		// contentPanelTitleLeft.setHeadingText("Groups Tree");
		
		//Fill Left Button Panel 
	    ButtonBar buttonBar = new ButtonBar();
	    buttonBar.setMinButtonWidth(70);
	    buttonBar.getElement().setMargins(0);
	    addGroupButton.setIcon(UserPropMenuImages.INSTANCE.add());
	    refreshGroupButton.setIcon(UserPropMenuImages.INSTANCE.connect());
	    removeGroupButton.setIcon(UserPropMenuImages.INSTANCE.delete());
	    buttonBar.add(addGroupButton);
	    buttonBar.add(removeGroupButton);
	    buttonBar.add(refreshGroupButton);
	    
	    //Fill Left Panel
	    splitPanelLeft.setScrollMode(ScrollMode.NONE);
	    splitPanelLeft.add(buttonBar, new VerticalLayoutData (1, 30));
	    splitPanelLeft.add(contentPanelTitleLeft, new VerticalLayoutData (1, 1));

		//Temp: Fill Right Panel
		// splitPanelRight.add(contentPanelTitleRight, new VerticalLayoutData (1, 1));
		// splitPanelRight.add(buttonBar, new VerticalLayoutData (1, 30));
		
		//Fill Main Panel
		splitPanelFull.setSize("100%", "100%");
		splitPanelFull.addWest(splitPanelLeft, 235);
		
		//Temp: must uncomment
		splitPanelRight = new VerticalLayoutContainer();
//		splitPanelRight.add(viewPropPanel, new VerticalLayoutData (1, 1));
//		splitPanelFull.add(splitPanelRight);
		
		if (checkBoxTree.getSelectedItem() == null) {
			startPage();
		}
		
		checkBoxTree.getTree().getSelectionModel().addSelectionHandler(
				new SelectionHandler<BpGroupDTO>() {
					@Override
					public void onSelection(SelectionEvent<BpGroupDTO> event) {
						
						VerticalLayoutContainer newSplitPanelRight = new VerticalLayoutContainer();
						GroupManagerForm groupAdminForm = new GroupManagerForm();
						newSplitPanelRight.add(groupAdminForm.getButtonBar(false), new VerticalLayoutData (1, 30));
						newSplitPanelRight.add(groupAdminForm.asWidget(checkBoxTree, false), new VerticalLayoutData (1, 1));
						addRightSplitPanel(newSplitPanelRight);
						
					}
				});
		
		addGroupButton.addSelectHandler(new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				GroupManagerForm groupAdminForm = new GroupManagerForm();
//				groupAdminForm.setUserGroupId(null);
				groupAdminForm.setNotEditable(false);
				VerticalLayoutContainer newSplitPanelRight = new VerticalLayoutContainer();
				newSplitPanelRight.add(groupAdminForm.getButtonBar(true), new VerticalLayoutData (1, 30));
				newSplitPanelRight.add(groupAdminForm.asWidget(checkBoxTree, true), new VerticalLayoutData (1, 1));
				addRightSplitPanel(newSplitPanelRight);
			}

		});
		
		removeGroupButton.addSelectHandler(new SelectHandler() {
			
			@Override
			public void onSelect(SelectEvent event) {

				if (checkBoxTree.getSelectedItem() == null) {
					
					com.google.gwt.user.client.Window.alert("Please select group!");
					
				} else {
					
				      final ConfirmMessageBox box = new ConfirmMessageBox("Confirm", "Are you sure you want to delete item?");
				      
				      box.addHideHandler(new HideHandler() {
				          public void onHide(HideEvent event) {
				            if (box.getHideButton() == box.getButtonById(PredefinedButton.YES.name())) {
				            	deleteGroup(checkBoxTree.getSelectedItem().getId());
				            } else if (box.getHideButton() == box.getButtonById(PredefinedButton.NO.name())){
				              // perform NO action
				            }
				          }
				        });
				      
				      box.show();
				      
				}
			}
		});
		
		refreshGroupButton.addSelectHandler(new SelectHandler() {
			
			@Override
			public void onSelect(SelectEvent event) {
				
				startPage();
				checkBoxTree.refreshStore();
				splitPanelFull.forceLayout();
			}
		});
		
//		splitPanelFull.forceLayout();
		initWidget(splitPanelFull);
				
	}
	
	/**
	 * GWT delete Group
	 * @param groupId
	 */
	public void deleteGroup (String groupId) {
		final RpcWaitingWindow rpcWait = new RpcWaitingWindow();
		rpcWait.showWindow();
			
		groupsPropertiesSvc.deleteGroup(groupId,
				new RpcCallback<Map<String, String>>(rpcWait.getBox()) {

					@Override
					public void onSuccess(Map<String, String> result) {
						rpcWait.deleteWindow();	
						if (result.containsKey("ok"))
							{
									checkBoxTree.getStore().remove(checkBoxTree.getSelectedItem());
//									checkBoxTree.refreshStore();
									startPage();
									splitPanelFull.forceLayout();
									com.google.gwt.user.client.Window.alert(result.get("ok"));
							} else if (result.containsKey("no")) {
								com.google.gwt.user.client.Window.alert(result.get("no"));
							} else {
								com.google.gwt.user.client.Window.alert("Some things going wrong!");
							}
//						checkBoxTree.refreshStore();					
					}
				});
	}
	
	public void startPage () {
		ContentPanel startPagePanel = new ContentPanel();
		startPagePanel.setHeadingText("Group properties");
		startPagePanel.setHeaderVisible(false);
		startPagePanel.setSize("100%", "100%");
		HTML text = new HTML("Please, select group");
		text.setHeight("100%");
		startPagePanel.add(text, new MarginData(10));
		
		VerticalLayoutContainer newSplitPanelRight = new VerticalLayoutContainer();
		newSplitPanelRight.add(new HTML(""), new VerticalLayoutData (1, 30));
		newSplitPanelRight.add(startPagePanel, new VerticalLayoutData (1, 1));
		addRightSplitPanel(newSplitPanelRight);
	}
	
	public void addRightSplitPanel (VerticalLayoutContainer newSplitPanelRight) {
		if (splitPanelRight != null) {
			if (splitPanelRight.isAttached()) {
				System.out.println ("isAttached already");
				splitPanelRight.removeFromParent();
			}
			splitPanelRight = newSplitPanelRight;
			splitPanelRight.setScrollMode(ScrollMode.AUTO);
			splitPanelFull.add(splitPanelRight);
		}
	}

}
