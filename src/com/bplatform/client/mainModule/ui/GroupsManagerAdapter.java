package com.bplatform.client.mainModule.ui;

import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

public class GroupsManagerAdapter extends BpElementWidget {
	
	public  GroupsManagerAdapter() {
		window.setMaximizable(true);
		window.setMinimizable(true);
		window.setHeadingText("Groups Manager");
		window.setWidth(700);
		window.setHeight(400);
		
		window.add((Widget) GWT.create(GroupsManagerComposite.class));
	}
	
}
