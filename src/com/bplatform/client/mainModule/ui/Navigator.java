package com.bplatform.client.mainModule.ui;

import com.bplatform.client.mainModule.ui.desktop.impl.BpElementWidget;
import com.bplatform.client.mainModule.ui.widgets.ToolButton;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.sencha.gxt.core.client.dom.ScrollSupport.ScrollMode;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.FlowLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;

public class Navigator extends BpElementWidget {
	
	private BorderLayoutContainer borderContainer = new BorderLayoutContainer();
	
	public Navigator() {
		super();
		FlowLayoutContainer vp = new FlowLayoutContainer();
		vp.setScrollMode(ScrollMode.AUTOY);
		
		borderContainer.setNorthWidget(toolBar(), new BorderLayoutData(50));
		borderContainer.setCenterWidget(vp);
		
		
		
		window.add(borderContainer);
		
		
	}
	
	
	private FlowPanel toolBar() {
		FlowPanel panel = new FlowPanel();
		panel.setStyleName("toolBar");

		ToolButton btnViewSettings = new ToolButton();
		ToolButton btnShowList = new ToolButton();
		ToolButton btnShowIcons = new ToolButton();
		ToolButton btnShowTile = new ToolButton();

		ToolButton btnHome = new ToolButton();

		btnViewSettings.addStyleName("btnViewSettings");
		btnShowIcons.addStyleName("btnShowIcons");
		btnShowTile.addStyleName("btnShowTile");
		btnShowList.addStyleName("btnShowList");

		btnShowList.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				borderContainer.setStyleName("view-list");

			}
		});

		btnShowTile.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				borderContainer.setStyleName("view-tile");

			}
		});

		btnShowIcons.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				borderContainer.setStyleName("view-icon");

			}
		});

		btnHome.addStyleName("btnHome");

		TextBox searchBox = new TextBox();

		searchBox.setStyleName("toolSearchBox");

		panel.add(btnHome);
		panel.add(searchBox);

		panel.add(btnViewSettings);

		panel.add(btnShowList);
		panel.add(btnShowIcons);
		panel.add(btnShowTile);

		return panel;
	}
	
}
