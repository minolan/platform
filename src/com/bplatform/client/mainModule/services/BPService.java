package com.bplatform.client.mainModule.services;

import java.util.HashMap;
import java.util.Map;

import com.bplatform.shared.exceptions.NoCustomPropertiesException;
import com.bplatform.shared.exceptions.NoLocalPropException;
import com.bplatform.shared.exceptions.NoSetNewPropExeption;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

//import com.google.gwt.user.client.rpc.ServiceDefTarget;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("../restricted/bpservice")
public interface BPService extends RemoteService {

	public  class Util {
		private static BPServiceAsync instance;

		public static BPServiceAsync getInstance() {
			if (instance == null) {
				instance = GWT.create(BPService.class);
				// ((ServiceDefTarget)
				// instance).setServiceEntryPoint(GWT.getHostPageBaseURL() +
				// "somepath");
				GWT.log("loading rpc", null);
			}
			return instance;
		}
	}

	String startSrv(String input) throws IllegalArgumentException;

	HashMap<String, String> getLocalPropMap(String local, String localResourse) throws NoLocalPropException, Exception;

	HashMap<String, String> getCustomProp(String customPropName) throws NoCustomPropertiesException, Exception;

	Map<String, Map<String, Map<String, String>>> getAllLocalPropMap()  throws NoLocalPropException, Exception;

	boolean setNewProp(String fileName, HashMap<String, String> res) throws NoSetNewPropExeption;

}
