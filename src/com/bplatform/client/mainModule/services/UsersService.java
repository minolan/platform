package com.bplatform.client.mainModule.services;

import java.util.List;
import java.util.Set;

import com.bplatform.client.bpShared.DTO.BpUserGroupFoldersDTO;
import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.sencha.gxt.data.shared.loader.FilterPagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

@RemoteServiceRelativePath("userProperties")
public interface UsersService extends RemoteService {
	BpUserDTO getUserProperties(String login);
	List<BpUserGroupFoldersDTO> getGroupsList() throws Exception;
	String setUserProperties(BpUserDTO setUserProp) throws Exception;
	Set<String> getUsersListFromGroup(String bpGroupId);
	PagingLoadResult<BpUserDTO> getUsers(FilterPagingLoadConfig config);
	List<BpUserDTO> getUsers();
}
