package com.bplatform.client.mainModule.services;

import java.util.Map;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("groupProperties")
public interface GroupsService extends RemoteService {
	
	/**
	 * Get Group Properties
	 */
	BpGroupDTO getGroupProperties(String groupId);
	
	/**
	 * ADD or Update Group
	 */
	BpGroupDTO addOrUpdateGroup(BpGroupDTO setGroupProp) throws Exception;
	
	/**
	 * DELETE group
	 */
	Map<String, String> deleteGroup(String groupId) throws Exception;
}
