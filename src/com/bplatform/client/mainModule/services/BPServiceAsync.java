package com.bplatform.client.mainModule.services;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface BPServiceAsync {

	void startSrv(String input, AsyncCallback<String> callback);

	void getLocalPropMap(String local, String localResourse, AsyncCallback<HashMap<String, String>> callback);

	void getCustomProp(String customPropName, AsyncCallback<HashMap<String, String>> callback);
	
	void getAllLocalPropMap(AsyncCallback <Map<String, Map<String, Map<String, String>>>>  callback);

	void setNewProp(String fileName, HashMap<String, String> res, AsyncCallback<Boolean> callback);

	
	
}
