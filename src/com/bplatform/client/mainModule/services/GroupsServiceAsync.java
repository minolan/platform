package com.bplatform.client.mainModule.services;

import java.util.Map;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GroupsServiceAsync {

	/**
	 * Get Group Properties
	 */
	void getGroupProperties(String groupId, AsyncCallback<BpGroupDTO> callback);
	
	/**
	 * ADD or Update Group
	 */
	void addOrUpdateGroup(BpGroupDTO setGroupProp,
			AsyncCallback<BpGroupDTO> callback);

	/**
	 * DELETE group
	 */
	void deleteGroup(String groupId, AsyncCallback<Map<String, String>> callback);
	
}
