package com.bplatform.client.mainModule.services;
import java.util.List;
import java.util.Set;

import com.bplatform.client.bpShared.DTO.BpUserGroupFoldersDTO;
import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.loader.FilterPagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

public interface UsersServiceAsync {
	void getUserProperties(String login, AsyncCallback<BpUserDTO> callback)
					throws IllegalArgumentException;
	
	void getGroupsList(AsyncCallback<List<BpUserGroupFoldersDTO>> callback)
			throws IllegalArgumentException;

	void setUserProperties(BpUserDTO setUserProp,
			AsyncCallback<String> callback);
	
	void getUsersListFromGroup(String bpGroupId, AsyncCallback<Set<String>> callback)
			throws IllegalArgumentException;
	
	void getUsers(FilterPagingLoadConfig config, AsyncCallback<PagingLoadResult<BpUserDTO>> callback);
	void getUsers(AsyncCallback<List<BpUserDTO>> callback);
}
