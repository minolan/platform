package com.bplatform.client.mainModule;

import com.bplatform.client.mainModule.ui.desktop.BpDesktop;
import com.bplatform.client.mainModule.ui.desktop.impl.BpDesktopImpl;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class BPlatform implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		BpDesktop desktop = new BpDesktopImpl();
		RootPanel.get("bm_desktop").add((Widget) desktop);
		RootLayoutPanel.get().addDomHandler(new ContextMenuHandler() {

			@Override public void onContextMenu(ContextMenuEvent event) {
				event.preventDefault();
				event.stopPropagation();
			}
		}, ContextMenuEvent.getType());
		
		// BpElement beMsgTest = GWT.create(AlertMessageBox.class);
		// BpMenuButton bmMsgTest = new BpMenuButton(beMsgTest);
		// bmMsgTest.asWidget().setTitle("Test Message");
		// menuContainer.add(bmMsgTest.asWidget());

		// RootPanel.get().add(be.asWidget());

		// userForm = new UserAdminForm();
		// RootPanel.get().add(userForm.asWidget());
		// userForm.showWindow();
	}
}
