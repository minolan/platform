package com.bplatform.client.bpShared.DTO;

import java.io.Serializable;
import java.util.Set;

import com.sencha.gxt.data.shared.TreeStore;

public class BpPermissionDTO extends BpGroupDTO implements Serializable {

	private static final long serialVersionUID = 1288374583152889774L;

	private String code;
	private Set<BpUserDTO> users;
	private Set<BpPermissionGroupDTO> permissionGroups;

	public void setCode(String code) {
		this.code = code;
	}

	public void setUsers(Set<BpUserDTO> users) {
		this.users = users;
	}

	public void setPermissionGroups(Set<BpPermissionGroupDTO> permissionGroups) {
		this.permissionGroups = permissionGroups;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCode() {
		return code;
	}

	public Set<BpUserDTO> getUsers() {
		return users;
	}

	public Set<BpPermissionGroupDTO> getPermissionGroups() {
		return permissionGroups;
	}

}
