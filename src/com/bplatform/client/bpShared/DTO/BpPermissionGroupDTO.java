package com.bplatform.client.bpShared.DTO;

import java.util.Set;

public class BpPermissionGroupDTO extends BpGroupDTO {

	private static final long serialVersionUID = -5111561629463822891L;

	private String code;
	private Set<BpUserGroupDTO> userGroups;
	private Set<BpPermissionDTO> permissions;

	public String getCode() {
		return code;
	}

	public Set<BpUserGroupDTO> getUserGroups() {
		return userGroups;
	}

	public Set<BpPermissionDTO> getPermissions() {
		return permissions;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setUserGroups(Set<BpUserGroupDTO> userGroups) {
		this.userGroups = userGroups;
	}

	public void setPermissions(Set<BpPermissionDTO> permissions) {
		this.permissions = permissions;
	}

}
