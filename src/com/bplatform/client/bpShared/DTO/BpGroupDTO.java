package com.bplatform.client.bpShared.DTO;

import java.io.Serializable;
import java.util.List;

import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.data.shared.TreeStore.TreeNode;

public class BpGroupDTO implements Serializable{

	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int autoId;
	private String name;
	private String id;
	private String parentId;
	private int nestedLevel;
	private String resultOfTransaction;
	
	public BpGroupDTO() {

	}

	public BpGroupDTO(int autoId, String name) {
		this.autoId = autoId;
		this.name = name;
	}
	
	public BpGroupDTO(int autoId, String name, String id) {
		this.autoId = autoId;
		this.name = name;
		this.id = id;
	}
	
	public BpGroupDTO(String name, String id, String parentId) {
		this.name = name;
		this.parentId = parentId;
		this.id = id;
	}

	public int getAutoId() {
		return autoId;
	}

	public void setAutoId(int autoId) {
		this.autoId = autoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BpGroupDTO getData() {
		return this;
	}
	
	public int getNestedLevel() {
		return nestedLevel;
	}
	
	public void setNestedLevel(int nestedLevel) {
		this.nestedLevel = nestedLevel;
	}

	public List<BpGroupDTO> getChildren() {
		return null;
	}

	@Override
	public String toString() {
//		return name != null ? name : super.toString();
		StringBuffer bpGroupDTOInfo = new StringBuffer("BpGroupDTO Info: [ Name: ");
		bpGroupDTOInfo.append(name);
		bpGroupDTOInfo.append("; GroupId: ");
		bpGroupDTOInfo.append(id);
		bpGroupDTOInfo.append("; ParentId: ");
		bpGroupDTOInfo.append(parentId);
		bpGroupDTOInfo.append("; ResultOfTransaction: ");
		bpGroupDTOInfo.append(resultOfTransaction);
		bpGroupDTOInfo.append(" ]");
		return bpGroupDTOInfo.toString();
		
	}
	
	public String getId () {
		return id;
	}
	
	public void setId (String id) {
		this.id = id;
	}
	
	public String getParentId () {
		return parentId;
	}
	
	public void setParentId (String parentId) {
		this.parentId = parentId;
	}
	
	public String getResultOfTransaction () {
		return resultOfTransaction;
	}

	public void setResultOfTransaction (String resultOfTransaction) {
		this.resultOfTransaction = resultOfTransaction;
	}
}