/**
 * Should be removed if not used
 * Need for showing users in Groups Tree
 * @author RomanL
 */

package com.bplatform.client.bpShared.DTO;

public class BpUserGroupDTO extends BpGroupDTO {
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String genre;
	  private String author;

	  protected BpUserGroupDTO() {

	  }

	  public BpUserGroupDTO(int id, String name, String genre, String author) {
	    super(id, name);
	    this.genre = genre;
	    this.author = author;
	  }

	  public String getGenre() {
	    return genre;
	  }

	  public void setGenre(String genre) {
	    this.genre = genre;
	  }

	  public String getAuthor() {
	    return author;
	  }

	  public void setAuthor(String author) {
	    this.author = author;
	  }
}
