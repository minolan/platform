package com.bplatform.client.bpShared.DTO;

import java.util.List;

@SuppressWarnings("serial")
public class BpUserGroupFoldersDTO extends BpGroupDTO {
		  private List<BpGroupDTO> children;

		  public BpUserGroupFoldersDTO() {

		  }

		  public BpUserGroupFoldersDTO(int id, String name) {
		    super(id, name);
		  }

		  public BpUserGroupFoldersDTO(int id, String name, String groupId) {
			    super(id, name, groupId);
			  }
		  
		  public List<BpGroupDTO> getChildren() {
		    return children;
		  }

		  public void setChildren(List<BpGroupDTO> children) {
		    this.children = children;
		  }

		  public void addChild(BpGroupDTO child) {
		    getChildren().add(child);
		  }
}
