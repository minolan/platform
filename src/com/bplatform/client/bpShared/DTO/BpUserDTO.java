package com.bplatform.client.bpShared.DTO;

import java.io.Serializable;
import java.util.Set;

public class BpUserDTO implements Serializable{
	
	private static final long serialVersionUID = 5715054194445285928L;
	
	private String id;
	private String login;
	private String password;
	private String passwordRetype;
	private String fio;
	private String email;
	private Set<BpGroupDTO> groups;
	private Set<BpPermissionDTO> permissions;
	
	
	public BpUserDTO() {
	}
	
	public BpUserDTO(String id, String login, String password, String fio, String email) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.fio = fio;
		this.email = email;
	}
	
	public Set<BpGroupDTO> getGroups() {
		return groups;
	}
	public void setGroups(Set<BpGroupDTO> groups) {
		this.groups = groups;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}	
	
	public String getPasswordRetype() {
		return passwordRetype;
	}
	public void setPasswordRetype(String passwordRetype) {
		this.passwordRetype = passwordRetype;
	}	
	
	public String getEmail () {
		return email;
	}

	public void setEmail (String email) {
		this.email = email;
	}
	
	public String getFio() {
		return fio;
	}
	public void setFio(String fio) {
		this.fio = fio;
	}
	
	public void setPermissions(Set<BpPermissionDTO> permissions) {
		this.permissions = permissions;
	}
	
	@Override
	public String toString() {
		return "BpUserDTO [login=" + login + ", password=" + password + ", fio="
				+ fio + ", email=" + email + "]";
	}
}
