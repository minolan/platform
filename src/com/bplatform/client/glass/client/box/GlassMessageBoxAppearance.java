/**
 * Sencha GXT 3.0.1 - Sencha for GWT
 * Copyright(c) 2007-2012, Sencha, Inc.
 * licensing@sencha.com
 *
 * http://www.sencha.com/products/gxt/license/
 */
package com.bplatform.client.glass.client.box;

import com.bplatform.client.glass.client.window.GrayWindowAppearance;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;


public class GlassMessageBoxAppearance extends GrayWindowAppearance {

	
	
	public interface GrayMessageBoxResources extends GrayWindowResources,
			ClientBundle {

		@Source({ "com/sencha/gxt/theme/base/client/panel/ContentPanel.css",
				"com/sencha/gxt/theme/gray/client/window/GrayWindow.css" })
		@Override
		GrayWindowStyle style();

	}

	public GlassMessageBoxAppearance() {
		super((GrayMessageBoxResources) GWT
				.create(GrayMessageBoxResources.class));
		GWT.log("GrayMessageBoxAppearance");
	}
	
	
}
