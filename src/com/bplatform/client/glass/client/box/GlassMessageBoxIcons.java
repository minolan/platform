package com.bplatform.client.glass.client.box;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;


public interface GlassMessageBoxIcons extends ClientBundle {
	@ImageOptions(repeatStyle = RepeatStyle.None)
	ImageResource error();

	@ImageOptions(repeatStyle = RepeatStyle.None)
	ImageResource info();

	@ImageOptions(repeatStyle = RepeatStyle.None)
	ImageResource question();

	@ImageOptions(repeatStyle = RepeatStyle.None)
	ImageResource warning();
}
