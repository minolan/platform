package com.bplatform.server;

import java.util.HashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.features.AppPropertiesReader;
import com.bplatform.server.features.PropertiesFacade;
import com.bplatform.server.features.WorkflowFacade;
import com.bplatform.server.user.SessionContext;

/**
 * Main class aggregating entry points to all the functionality of the platform.
 * The @Inject used to inject other @ApplicationScoped facades.
 * To inject @SessionScoped facade it must be serializable bean (default constructor is necessary).
 * </br></br>
 * To use facade in GWT RPC service it (service class) must be annotated with @WebServlet. Then <b>@Inject private MainFacade appFacade;</b> in it. 
 * </br>
 * <p><b>IMPORTANT</b>
 * Any variable inside an @ApplicationScope bean becomes <u>like</u> <i>synchronized static</i> because of proxied nature of this bean.
 * Don't really sure about synchronization of variables but it must be so because of consistency feature.</br>
 * In the same time all methods are <u>like</u> <i>static</i> but NOT synchronized as they can be executed simultaneously. //This was tested, trust me!
 * 
 * @author Minolan
 */
@ApplicationScoped
public class MainFacade {
	
	private HashMap<String,SessionContext> loggedUsersSessions = new HashMap<String,SessionContext>();
	
	@Inject
	private SessionContext sessionContext;
	
	@Inject
	private AppPropertiesReader appProperties;
	
	@Inject
	private PropertiesFacade propertiesFacade;
	
	@Inject
	private WorkflowFacade workflowFacade;

	@Inject
	private HibernateFacade hibernateFacade;
	
	
	public MainFacade() {}
	
	
	/**
	 *@return   LocaleFacade
	 *
	 **/
	public PropertiesFacade getPropertiesFacade (){
		return propertiesFacade;	
	}

	
	/**
	 * For DB operations
	 * @return
	 */
	public HibernateFacade getHibernateFacade() {
		return hibernateFacade;
	}
	
	
	/**
	 * 
	 * @return user session context
	 */
	public SessionContext getSessionContext() {
		return sessionContext;
	}
	
	
	/**
	 * TODO make a facade, this is mock for now.	
	 * @return
	 */
	public AppPropertiesReader getAppProperties() {
		return appProperties;
	}

	
	/**
	 * Used to update application-available user session context.
	 * TODO investigate Weld Events for this shit
	 * @param login
	 * @param context
	 */
	public void setLoggedUserContext(String login, SessionContext context) {
		loggedUsersSessions.put(login, context);
	}
	
	/**
	 * Used to get specified user session context anywhere in application. 
	 * @param login
	 * @return if no context associated this given login - returns <b>null</b>
	 */
	public SessionContext getLoggedUserContext(String login) {
		if (loggedUsersSessions.containsKey(login)) {
			return loggedUsersSessions.get(login);
		} else {
			return null;
		}
	}

	/**
	 * Returns initialized facade for all workflow logics presented in system.
	 * Read WorkflowFacade jdoc for more info on realization.
	 * @return
	 */
	public WorkflowFacade getWorkflowFacade() {
		return workflowFacade;
	}
	
}
