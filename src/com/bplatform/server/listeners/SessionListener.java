package com.bplatform.server.listeners;

import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {
	
	private static ConcurrentHashMap<String, Object> appContextUserStorage = new ConcurrentHashMap<String, Object>();

	public static ConcurrentHashMap<String, Object> getAppContextUserStorage() {
		return appContextUserStorage;
	}

	public static void setAppContextUserStorage(ConcurrentHashMap<String, Object> appContextUserStorage) {
		SessionListener.appContextUserStorage = appContextUserStorage;
	}

	@Override
	public void sessionCreated(HttpSessionEvent httpSessionEvent) {
		HttpSession session = httpSessionEvent.getSession();

		appContextUserStorage.put(session.getId(), new Object());
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
		HttpSession session = httpSessionEvent.getSession();

		appContextUserStorage.remove(session.getId());		
	}

}
