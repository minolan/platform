package com.bplatform.server.DAO;

import java.io.File;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.Entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.APIs.BpPermissionAPI;
import com.bplatform.server.DAO.APIs.BpPermissionGroupAPI;
import com.bplatform.server.DAO.APIs.BpTokenAPI;
import com.bplatform.server.DAO.APIs.BpUserAPI;
import com.bplatform.server.DAO.APIs.BpUserGroupAPI;
import com.bplatform.server.DAO.APIs.JoinedBpUserToBpPermissionAPI;
import com.bplatform.server.DAO.models.BpDefaultEntity;

@ApplicationScoped
public class HibernateFacade {
		
	private SessionFactory sessionFactory;
	
	@Inject @Any Instance<BpDefaultEntity> entitiesToMap;
	
	@Inject
	private MainFacade appFacade;
	
	@Inject
	private BpPermissionAPI bpPermissionAPI;
	@Inject
	private BpPermissionGroupAPI bpPermissionGroupAPI;
	@Inject
	private BpTokenAPI bpTokenAPI;
	@Inject 
	private BpUserAPI bpUserAPI;
	@Inject
	private BpUserGroupAPI bpUserGroupAPI;
	@Inject
	private JoinedBpUserToBpPermissionAPI joinedBpUserToBpPermissionAPI;
	
	
	
	private static int ins = 0;
	
	public HibernateFacade() {
		super();
		ins++;
		System.out.println(" ----- HIBERNATE INSTANCE = "+ins+" -----");
	}
	
	/**
	 * A SessionFactory is set up once for an application
	 * @throws Exception
	 */
	@PostConstruct
	private void setUp() throws Exception {
		try {
			File conf = new File(appFacade.getAppProperties().getRootPath()+File.separator+"web-inf"+File.separator+"config"+File.separator+"hibernate.cfg.xml");
			Configuration configuration = new Configuration().configure(conf.getAbsoluteFile());
			Instance<BpDefaultEntity> entities = getEntitiesToMap();
			for (BpDefaultEntity entity:entities) {
				if (entity.getClass().isAnnotationPresent(Entity.class)) {
					configuration.addAnnotatedClass(entity.getClass());
				}
			}
			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Exception e) {
			//secure not serializable exception comes out of Weld otherwise, printStackTrace is for debug purposes
			e.printStackTrace();
			throw e;
		}
	}
	
	@PreDestroy
	private void tearDown() throws Exception {
		if (sessionFactory != null ) {
			sessionFactory.close();
		}
	}
	
	public Session getHibernateSession() {
		return sessionFactory.openSession();
	}

	public BpUserAPI getBpUserAPI() {
		return bpUserAPI;
	}
	
	
	
	public BpPermissionAPI getBpPermissionAPI() {
		return bpPermissionAPI;
	}

	public BpPermissionGroupAPI getBpPermissionGroupAPI() {
		return bpPermissionGroupAPI;
	}

	public BpUserGroupAPI getBpUserGroupAPI() {
		return bpUserGroupAPI;
	}

	public BpTokenAPI getBpTokenAPI(){
		return bpTokenAPI;
	}
	
	public JoinedBpUserToBpPermissionAPI getJoinedBpUserToBpPermissionAPI() {
		return joinedBpUserToBpPermissionAPI;
	}


	/**
	 * By this injection we get an iterable Instance of all classes extended from DefaultEntity.
	 * Note that DefaultEntity is not an @Entity, but a @MappedSuperclass
	 * @return
	 */
	private Instance<BpDefaultEntity> getEntitiesToMap() {
		return entitiesToMap;
	}
	
	public SessionFactory getSessionFactory(){
		return sessionFactory;
	}
	
	
}
