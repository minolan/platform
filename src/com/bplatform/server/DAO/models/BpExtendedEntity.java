package com.bplatform.server.DAO.models;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Historiable draft for entities. 
 * @author Minolan
 */

@MappedSuperclass
public class BpExtendedEntity extends BpDefaultEntity{
	
	@Nonnull
	@Temporal(TemporalType.TIMESTAMP)
	protected Date created_date = new Date();
	
	@Nonnull
	@ManyToOne(fetch=FetchType.EAGER)
	protected BpUser created_by;
	
	@Nonnull
	@Temporal(TemporalType.TIMESTAMP)
	protected Date modified_date = new Date();
	
	@Nonnull
	@ManyToOne(fetch=FetchType.EAGER)
	protected BpUser modified_by;
	
	@PreUpdate
	public void setLastUpdate() {  this.modified_date = new Date(); }

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}


	public Date getModified_date() {
		return modified_date;
	}

	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}


	public BpUser getCreated_by() {
		return created_by;
	}


	public void setCreated_by(BpUser created_by) {
		this.created_by = created_by;
	}


	public BpUser getModified_by() {
		return modified_by;
	}


	public void setModified_by(BpUser modified_by) {
		this.modified_by = modified_by;
	}

	
	
}
