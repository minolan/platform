package com.bplatform.server.DAO.models;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

//TODO how to write query for getting by name

@MappedSuperclass
public class BpTree extends BpDefaultEntity {
	
	@Column(nullable = false, length = 32)
	protected String name;
	protected String parentId;
	protected String tCode;
	protected String connectedTo;
	
	/**
	 * If someone creates new instance without specifying needed fields - it must be his/her own error, we must not allow to save such a retarded entity to DB!  
	 */
	public BpTree(){}
	
	public BpTree(String name){
		this.name = name;
	}
	
	public BpTree(String id, String name, String parentID, String tCode,
			String connectedTo) {
		this.id = id;
		this.name = name;
		this.parentId = parentID;
		this.tCode = tCode;
		this.connectedTo = connectedTo;
	}
	
	public BpTree(BpTree tree){
		id = new String(tree.getId());
		name = new String(tree.getName());
		parentId = new String(tree.getParentId());
		tCode = new String(tree.getTCode());
		connectedTo = new String(tree.getConnectedTo());
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentID(String parentId) {
		this.parentId = parentId;
	}
	public String getTCode() {
		return tCode;
	}
	public void setTCode(String tCode) {
		this.tCode = tCode;
	}
	public String getConnectedTo() {
		return connectedTo;
	}
	public void setConnectedTo(String conectedTo) {
		this.connectedTo = conectedTo;
	}

	@Override
	public String toString() {
		return "BpTree [ID=" + id + ", name=" + name + ", parentId=" + parentId
				+ ", tCode=" + tCode + ", connectedTo=" + connectedTo + "]";
	}
	
	@Override
	public boolean equals(Object o){
		if(o == null){
			return false;
		}
		try{
			BpTree tree = (BpTree)o;
			if( 
					( id == null && tree.getId() == null  || id.equals(tree.getId()) ) 
					&& ( parentId == null && tree.getParentId() == null || parentId.equals(parentId)) )  {//remove it then
			if( /*id.equals(tree.getID()) && parentID.equals(tree.getParentID()) 
					&& */((name ==  null && tree.getName() == null) || name.equalsIgnoreCase(tree.getName()))
					&& ((tCode == null && tree.getTCode() == null) || tCode.equalsIgnoreCase(tree.getTCode()))					
					&& ((connectedTo == null && tree.getConnectedTo() == null) || connectedTo.equalsIgnoreCase(tree.getConnectedTo()))){
				return true;
			}
			}//remove it then
			return false;
		}catch(ClassCastException e){
			return false;
		}
	}

}
