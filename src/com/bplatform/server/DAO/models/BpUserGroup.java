package com.bplatform.server.DAO.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/*@NamedQueries({ 
	@NamedQuery(name = "getIdBpUserGroupsByUser", query = "FROM BpUserGroup group.id WHERE :user in elements group.users")
// note: in the HQL should be used class name																							
// and property name of the mapped @Entity instead of the actual table name and
// column name
 })*/
/**
 * 
 * Class for storing information about groups. Each group corresponds to some user's role in system and aggregates users.
 * 
 * @author Anastasia Smakovska
 * 
 */
@Entity
@Table(name = "BP_USER_GROUP",
uniqueConstraints = {@UniqueConstraint(columnNames={"name"})})
public class BpUserGroup extends BpTree {

	@Column(length = 32)
	private String code;

	// Annotations for generating new table for storing m to n relations in the
	// database

	// private Set<com.bplatform.server.DAO.models.BpUser> users = new HashSet<com.bplatform.server.DAO.models.BpUser>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
	@JoinTable(name = "BP_USERS_TO_BP_USER_GROUPS", joinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
	private Set<com.bplatform.server.DAO.models.BpUser> users = new HashSet<com.bplatform.server.DAO.models.BpUser>();
	
	public Set<com.bplatform.server.DAO.models.BpUser> getUsers() {
		return users;
	}

	public void setUsers(Set<com.bplatform.server.DAO.models.BpUser> users) {
		if(users == null){
			this.users = new HashSet<com.bplatform.server.DAO.models.BpUser>();
		}else{
			this.users = users;
		}
		
	}
	
	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
	@JoinTable(name = "BP_PERMISSION_GROUPS_TO_BP_USER_GROUPS", joinColumns = @JoinColumn(name = "user_group_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "permission_group_id", referencedColumnName = "id"))
	private Set<com.bplatform.server.DAO.models.BpPermissionGroup> permissionGroups = new HashSet<com.bplatform.server.DAO.models.BpPermissionGroup>();
	
	/**
	 * If someone creates new instance without specifying needed fields - it must be his/her own error, we must not allow to save such a retarded entity to DB!  
	 */
	public BpUserGroup() {}
	
	public BpUserGroup(String name) {
		super(name);
	}

	public BpUserGroup(String name, String parentId) {
		this.name = name;
		this.parentId = parentId;
	}
	
	//not interesting
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<com.bplatform.server.DAO.models.BpPermissionGroup> getPermissionGroups() {
		return permissionGroups;
	}

	public void setPermissionGroups(
			Set<com.bplatform.server.DAO.models.BpPermissionGroup> permissionGroups) {
		this.permissionGroups = permissionGroups;
	}

	public boolean addBpUser(BpUser user) {
		return users.add(user);
	}

	public boolean addBpPermissionGroup(BpPermissionGroup permissionGroup) {
		return permissionGroups.add(permissionGroup);
	}

	@Override
	public String toString() {
		return "BpUserGroup [code=" + code +", getName()="
				+ getName() + ", getParentId()=" + getParentId()
				+ ", getConnectedTo()=" + getConnectedTo() + ", getId()="
				+ getId() + "]";
	}
	

}
