package com.bplatform.server.DAO.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * To store multiple schemes of workflow processes.
 * @author Minolan
 */
@NamedQueries({
	@NamedQuery(name = "getSchemes", query = "FROM BpWorkflowScheme" )
})
@Entity
@Table(name = "BP_WORKFLOW_SCHEMES")
public class BpWorkflowScheme extends BpDefaultEntity {
	
	@Column(nullable = false, length = 50)
	protected String schemeName;
	
	/**
	 * Default value == true
	 */
	protected Boolean enabled = true;

	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
}