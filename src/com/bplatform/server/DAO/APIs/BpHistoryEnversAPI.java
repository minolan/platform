package com.bplatform.server.DAO.APIs;

import java.util.HashSet;
import java.util.Set;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpStatefulEntity;
import com.bplatform.server.DAO.models.BpWorkflowNode;
import com.bplatform.server.DAO.models.BpWorkflowScheme;
import com.bplatform.shared.exceptions.BpProcessExecutionException;
import com.bplatform.shared.exceptions.DataBaseException;

/**
 * Book book = new Book();
 * book.setAuthor( session.byId( Author.class ).getReference( authorId ) );
 * session.refresh( book ); //reload an entity instance and it's collections
 * @author Minolan
 */
@Model
public class BpHistoryEnversAPI {
	
	public BpHistoryEnversAPI() {}

	@Inject
	private MainFacade appFacade;
	
	@Inject
	private HibernateFacade hibFacade;
	
	public MainFacade getAppFacade() {
		return appFacade;
	}

	public void setAppFacade(MainFacade appFacade) {
		this.appFacade = appFacade;
	}

	public HibernateFacade getHibFacade() {
		return hibFacade;
	}

	public void setHibFacade(HibernateFacade hibFacade) {
		this.hibFacade = hibFacade;
	}

	/**
	 * BpProcessExecutionException is thrown in case when entity is blocked already.
	 * @param entity
	 * @param sess
	 * @return
	 * @throws BpProcessExecutionException
	 * @throws DataBaseException
	 */
	public <E extends BpStatefulEntity> E blockEntity(E entity, Session sess) throws BpProcessExecutionException, DataBaseException {
		boolean close = false;
		if (sess==null) {
			sess = hibFacade.getHibernateSession();
			close = true;
		}
		try {
			sess.refresh(entity);
			if (entity.getBlockedBy()==null) {
				entity.setBlockedBy(appFacade.getSessionContext().getBpUser());
				saveOrUpdateBpStEntity(entity, sess);
			} else {
				throw new BpProcessExecutionException(entity.getId(),"Already blocked");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (close) {
				sess.close();
			}
		}
		return entity;
	}
	
	/**
	 * Unblock blocked entity.
	 * @param entity
	 * @param sess
	 * @return
	 * @throws DataBaseException
	 * @throws BpProcessExecutionException - in a case when somehow entity is not blocked (must be not possible in normal conditions)
	 */
	public <E extends BpStatefulEntity> E unblockEntity(E entity, Session sess) throws DataBaseException, BpProcessExecutionException {
		boolean close = false;
		if (sess==null) {
			sess = hibFacade.getHibernateSession();
			close = true;
		}
		try {
			sess.refresh(entity);
			if (entity.getBlockedBy()!=null) {
				entity.setBlockedBy(null);
				saveOrUpdateBpStEntity(entity, sess);
			} else {
				throw new BpProcessExecutionException(entity.getId(),"Not blocked");
			}
		} catch (BpProcessExecutionException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (close) {
				sess.close();
			}
		}
		return entity;
	}
	
	/**
	 * If Hibernate session is given it will not be closed.
	 * @param nextNode
	 * @param currEntity
	 * @param session - optional
	 * @return
	 * @throws DataBaseException
	 */
	public <E extends BpStatefulEntity> E setState(BpWorkflowNode nextNode, E currEntity, Session sess) throws DataBaseException {
		boolean close = false;
		if (sess==null) {
			sess = hibFacade.getHibernateSession();
			close = true;
		}
		Transaction transaction = null;
		try {
			transaction = sess.getTransaction();
			transaction.begin();
			currEntity.setState(nextNode);
			transaction.commit();
			transaction = null;
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (transaction != null) {
				transaction.rollback();
			}
			if (close) {
				sess.close();
			}
		}
		return currEntity;
	}
	
	/**
	 * If Hibernate session is given it will not be closed. 
	 * @param entity
	 * @param sess
	 * @throws DataBaseException
	 * @throws ConstraintViolationException
	 */
	public <E extends BpStatefulEntity> void deleteBpStEntity(E entity, Session sess) throws DataBaseException, ConstraintViolationException {
		boolean close = false;
		if (sess==null) {
			sess = hibFacade.getHibernateSession();
			close = true;
		}
		Transaction transaction = null;
		try {
			transaction = sess.getTransaction();
			transaction.begin();
			sess.delete(entity);
			transaction.commit();
			transaction = null;
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (transaction != null) {
				transaction.rollback();
			}
			if (close) {
				sess.close();
			}
		}
	}
	
	/**
	 * 
	 * @param entity
	 * @param sess
	 * @throws ConstraintViolationException
	 * @throws DataBaseException
	 */
	public <E extends BpStatefulEntity> void saveOrUpdateBpStEntity(E entity, Session sess) throws ConstraintViolationException, DataBaseException{
		boolean close = false;
		if (sess==null) {
			sess = hibFacade.getHibernateSession();
			close = true;
		}
		Transaction transaction = null;
		try {
			transaction = sess.getTransaction();
			transaction.begin();
			sess.saveOrUpdate(entity);
			transaction.commit();
			transaction = null;
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (transaction != null) {
				transaction.rollback();
			}
			if (close) {
				sess.close();
			}
		}
	}
	
	/**
	 * Get BpStatefulEntity successor defined by entClass.
	 * If Hibernate session is given it will not be closed. 
	 * @param id
	 * @param entClass
	 * @param sess
	 * @return
	 * @throws DataBaseException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <E extends BpStatefulEntity> E getBpStEntityById(String id, Class entClass, Session sess) throws DataBaseException {
		boolean close = false;
		if (sess==null) {
			sess = hibFacade.getHibernateSession();
			close = true;
		}
		E ent = null;
		try {
			ent = (E) sess.get(entClass, id);
			sess.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (close) {
				sess.close();
			}
		}
		return ent;
	}
	
	
	/**
	 * ConstraintViolationException in case of some entity has connection with this one. 
	 * @param wNode
	 * @throws DataBaseException
	 * @throws ConstraintViolationException
	 */
	public void deleteNode(BpWorkflowNode wNode) throws DataBaseException, ConstraintViolationException {
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.delete(wNode);
			transaction.commit();
			transaction = null;
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (transaction != null) {
				transaction.rollback();
			}
			hbSession.close();
		}
	}
	
	public void saveOrUpdateNode(BpWorkflowNode wNode) throws ConstraintViolationException, DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.saveOrUpdate(wNode);
			transaction.commit();
			transaction = null;
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (transaction != null) {
				transaction.rollback();
			}
			hbSession.close();
		}
	}
	
	public void saveOrUpdateWorkflowScheme(BpWorkflowScheme wScheme) throws ConstraintViolationException, DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.saveOrUpdate(wScheme);
			transaction.commit();
			transaction = null;
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			if (transaction != null) {
				transaction.rollback();
			}
			hbSession.close();
		}
	}
	
	public BpWorkflowNode getNodeById(String id) throws DataBaseException {
		BpWorkflowNode node = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			node = (BpWorkflowNode)hbSession.get(BpWorkflowNode.class, id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			hbSession.close();
		}
		return node;
	}
	
	/**
	 * 
	 * @param wfs
	 * @return
	 * @throws DataBaseException
	 */
	public Set<BpWorkflowNode> getWorkflowNodesSet(BpWorkflowScheme wfs) throws DataBaseException {
		Set<BpWorkflowNode> nodes = new HashSet<BpWorkflowNode>();
		Session hbSession = hibFacade.getHibernateSession();
		try {
			nodes = new HashSet<BpWorkflowNode>(hbSession.getNamedQuery("getNodesOfWorkflow").setEntity("scheme", wfs).list());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			hbSession.close();
		}
		return nodes;
	}
	
	
	public BpWorkflowNode getSchemeById(String id) throws DataBaseException {
		BpWorkflowNode node = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			node = (BpWorkflowNode)hbSession.get(BpWorkflowNode.class, id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			hbSession.close();
		}
		return node;
	}
	
	/**
	 * 
	 * @return
	 * @throws DataBaseException
	 */
	public Set<BpWorkflowScheme> getSchemes() throws DataBaseException {
		Set<BpWorkflowScheme> schemes = null;
		Session hbSession = appFacade.getHibernateFacade().getHibernateSession();
		try {
			schemes = new HashSet<BpWorkflowScheme>(hbSession.getNamedQuery("getSchemes").list());
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException(e.getLocalizedMessage());
		} finally {
			hbSession.close();
		}
		return schemes;
	}
}
