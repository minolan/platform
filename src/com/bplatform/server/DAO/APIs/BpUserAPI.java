package com.bplatform.server.DAO.APIs;

import java.util.HashSet;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpPermission;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.BpUserGroup;
import com.bplatform.server.DAO.models.JoinedBpUserToBpPermission;
import com.bplatform.server.features.PropertiesFacade;
import com.bplatform.shared.exceptions.DataBaseException;

/**
 * 
 * @author Anastasia Smakovska
 * 
 */

@ApplicationScoped
public class BpUserAPI {
	
	@Inject
	private HibernateFacade hibFacade;
	
	@Inject
	private PropertiesFacade propFacade;

	public BpUserAPI() {
	}


	public void delete(BpUser user) throws DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			// Remove all records, joined with this user
			JoinedBpUserToBpPermissionAPI joinedAPI = hibFacade
					.getJoinedBpUserToBpPermissionAPI();
			List<JoinedBpUserToBpPermission> joinedList = joinedAPI
					.getByUser(user);
			for (JoinedBpUserToBpPermission joined : joinedList) {
				joinedAPI.delete(joined);
			}
			hbSession.delete(user);
			transaction.commit();
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("user cannot be deleted from the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
	}

	public void saveOrUpdate(BpUser user) throws DataBaseException, ConstraintViolationException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		//Set default locale if neither locale was set
		if(user.getLocale() == null){
			user.setLocale(propFacade.getDefaultLocale());
		}
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			// This operation cascades to associated instances if the
			// association is mapped with cascade="save-update".
			hbSession.saveOrUpdate(user);
			transaction.commit();
			hbSession.flush();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to save user with the same login, null value of field or too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("user cannot be saved or updated in the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
	}

	public BpUser getById(String id) throws DataBaseException{
		BpUser user = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			user = (BpUser) hbSession.get(BpUser.class, id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("user with this id cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return user;
	}
	
	public List<BpUser> getAllUsers() throws DataBaseException{
		List<BpUser> users = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			users = hbSession.createCriteria(BpUser.class).list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("users list cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return users;
	}
	
	public List<BpUser> getAllUsersOrderByASC(String field) throws DataBaseException{
		List<BpUser> users = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			users = hbSession.createCriteria(BpUser.class)
					.addOrder(Order.asc(field))
					.list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("users list cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return users;
	}
	
	public List<BpUser> getAllUsersOrderByDESC(String field) throws DataBaseException{
		List<BpUser> users = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			users = hbSession.createCriteria(BpUser.class)
					.addOrder(Order.desc(field))
					.list();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("users list cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return users;
	}

	public BpUser getByLogin(String login) throws DataBaseException{
		BpUser user = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			user = (BpUser) hbSession.getNamedQuery("getBpUserByLogin")
					.setString("login", login).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();			
			throw new DataBaseException("user with this login cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return user;
	}
	
	public void initializeBpUserCollectionsId(BpUser user){
		Session hbSession = hibFacade.getHibernateSession();
		try {
		} catch (Exception e) {
			e.printStackTrace();			
			throw new DataBaseException("user with this login cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	
	@Deprecated
	public BpUser getByLoginAll(String login) throws DataBaseException{
		BpUser user = null;
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			user = (BpUser) hbSession.createCriteria(BpUser.class).add(Restrictions.eq("login", login)).uniqueResult();
			transaction.commit();
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("user with this login cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return user;
	}

	public List<BpUserGroup> initializeBpUserGroups(BpUser user){
		List<BpUserGroup> userGroups;
		Session hbSession = hibFacade.getHibernateSession();
		try{
			userGroups = (List<BpUserGroup>)hbSession.createCriteria(BpUserGroup.class, "userGroup").createAlias("userGroup.users", "user").add(Restrictions.eq("user.id", user.getId())).list();
			user.setBpUserGroups(new HashSet<BpUserGroup>(userGroups));
		}catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("BpUserGroups cannot be recieved from the database by unresolved issue");
		} finally{
			hbSession.close();
		}
		return userGroups;
	}
	
	public List<JoinedBpUserToBpPermission> initializeJoinedBpPermissions(BpUser user){
		List<JoinedBpUserToBpPermission> joined;
		Session hbSession = hibFacade.getHibernateSession();
		try{
			joined = (List<JoinedBpUserToBpPermission>)hbSession.createCriteria(JoinedBpUserToBpPermission.class, "joined").createAlias("joined.bpUser", "user").add(Restrictions.eq("user.id", user.getId())).list();
			user.setJoinedPermissions(new HashSet<JoinedBpUserToBpPermission>(joined));
		}catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("BpPermissions cannot be recieved from the database by unresolved issue");
		} finally{
			hbSession.close();
		}
		return joined;
	}
	
}
