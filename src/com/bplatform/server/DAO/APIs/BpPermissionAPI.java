package com.bplatform.server.DAO.APIs;

import java.util.HashSet;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpPermission;
import com.bplatform.server.DAO.models.BpPermissionGroup;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.BpUserGroup;
import com.bplatform.server.DAO.models.JoinedBpUserToBpPermission;
import com.bplatform.shared.exceptions.DataBaseException;

/**
 * 
 * @author Anastasia Smakovska
 *
 * 4 ����. 2013
 *
 */
@ApplicationScoped
public class BpPermissionAPI {
	
	@Inject
	private HibernateFacade hibFacade;
	
	public BpPermissionAPI(){}

	public void delete(BpPermission permission) throws DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			// Remove all records, joined with this permission
			JoinedBpUserToBpPermissionAPI joinedAPI = hibFacade.getJoinedBpUserToBpPermissionAPI();
			List<JoinedBpUserToBpPermission> joinedList = joinedAPI.getByPermission(permission);
			for(JoinedBpUserToBpPermission joined : joinedList){
				joinedAPI.delete(joined);
			}
			hbSession.delete(permission);
			transaction.commit();
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("permission cannot be deleted from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
	}

	public void save(BpPermission permission) throws DataBaseException, ConstraintViolationException {
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			//This operation cascades to associated instances if the association is mapped with cascade="save-update".
			hbSession.save(permission);
			transaction.commit();
			hbSession.flush();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to save permission with null value of field or too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("permission cannot be saved to in the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	
	public void update(BpPermission permission) throws DataBaseException, ConstraintViolationException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			//This operation cascades to associated instances if the association is mapped with cascade="save-update".
			hbSession.update(permission);
			transaction.commit();
			hbSession.flush();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to update permission with null value of field or too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("permission cannot be updated to in the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	
	public BpPermission getById(String id) throws DataBaseException {
		BpPermission permission = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			permission = (BpPermission)hbSession.get(BpPermission.class, id);
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("permission cannot be recieved from the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
		return permission;
	}

	public List<BpPermissionGroup> initializeBpPermissionGroups(BpPermission permission){
		List<BpPermissionGroup> permissionGroups;
		Session hbSession = hibFacade.getHibernateSession();
		try{
			permissionGroups = (List<BpPermissionGroup>)hbSession.createCriteria(BpPermissionGroup.class, "permissionGroup").createAlias("permissionGroup.permissions", "permission").add(Restrictions.eq("permission.id", permission.getId())).list();
			permission.setBpPermissionsGroups(new HashSet<BpPermissionGroup>(permissionGroups));
		}catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("BpUserGroups cannot be recieved from the database by unresolved issue");
		} finally{
			hbSession.close();
		}
		return permissionGroups;
	}
	
	public List<JoinedBpUserToBpPermission> initializeJoinedBpUsers(BpPermission permission){
		List<JoinedBpUserToBpPermission> joined;
		Session hbSession = hibFacade.getHibernateSession();
		try{
			joined = (List<JoinedBpUserToBpPermission>)hbSession.createCriteria(JoinedBpUserToBpPermission.class, "joined").createAlias("joined.bpPermission", "permission").add(Restrictions.eq("user.id", permission.getId())).list();
			permission.setJoinedUsers(new HashSet<JoinedBpUserToBpPermission>(joined));
		}catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("BpPermissions cannot be recieved from the database by unresolved issue");
		} finally{
			hbSession.close();
		}
		return joined;
	}
	
	
}

	
