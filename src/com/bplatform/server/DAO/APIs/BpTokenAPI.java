package com.bplatform.server.DAO.APIs;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpToken;
import com.bplatform.shared.exceptions.DataBaseException;

@ApplicationScoped
public class BpTokenAPI {

	@Inject
	private HibernateFacade hibFacade;

	public BpTokenAPI() {
	}


	public void delete(BpToken token) throws DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.delete(token);
			transaction.commit();
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("token cannot be deleted from the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	
	public void save(BpToken token) throws DataBaseException, ConstraintViolationException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.save(token);
			transaction.commit();
			hbSession.flush();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to save token with null value of field or too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("token cannot be saved to the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
	}

	public void update(BpToken token) throws DataBaseException, ConstraintViolationException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.update(token);
			transaction.commit();
			hbSession.flush();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to update token with null value of field or too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("token cannot be updated in the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
	}

	public BpToken getById(String id) throws DataBaseException{
		BpToken token = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			token = (BpToken) hbSession.get(BpToken.class, id);
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("user with this id cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return token;
	}

	public List<BpToken> getByUsername(String username) throws DataBaseException{
		List<BpToken> tokens = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			tokens = (List<BpToken>) hbSession.createCriteria(BpToken.class).add(Restrictions.eq("username", username)).list();
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();			
			throw new DataBaseException("token with this username cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return tokens;
	}
	
	public List<BpToken> getBySeries(String series) throws DataBaseException{
		List<BpToken> tokens = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			tokens = (List<BpToken>) hbSession.createCriteria(BpToken.class).add(Restrictions.eq("series", series)).list();
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();			
			throw new DataBaseException("token with this username cannot be recieved from database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return tokens;
	}

}
