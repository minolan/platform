package com.bplatform.server.DAO.APIs;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpPermission;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.JoinedBpUserToBpPermission;
import com.bplatform.shared.exceptions.DataBaseException;

/**
 * 
 * @author Anastasia Smakovska
 *
 * 4 ����. 2013
 *
 */
@ApplicationScoped
public class JoinedBpUserToBpPermissionAPI {

	@Inject
	private HibernateFacade hibFacade;
	
	
	
	public JoinedBpUserToBpPermissionAPI(){}
	
	/*
	 Hibernate draft:
	 
	 Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			//do something
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			hbSession.close();
		}
	 */
	
	
	public void delete(JoinedBpUserToBpPermission joined) throws DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.delete(joined);
			transaction.commit();
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("joined record cannot be deleted from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	

	public void saveOrUpdate(JoinedBpUserToBpPermission joined) throws DataBaseException, ConstraintViolationException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			//This operation cascades to associated instances if the association is mapped with cascade="save-update".
			hbSession.saveOrUpdate(joined);
			transaction.commit();
			hbSession.flush();
		} catch(ConstraintViolationException e){
			throw e; // trying to save or update joined record with null value or some field is too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("joined record cannot be saved or updated to the database by unresolved issue");
		}  finally {
			hbSession.close();
		}
	}
	
	public JoinedBpUserToBpPermission getById(String id) throws DataBaseException{
		JoinedBpUserToBpPermission joined = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			joined = (JoinedBpUserToBpPermission)hbSession.get(JoinedBpUserToBpPermission.class, id);
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("joined record cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return joined;
	}
	
	List<JoinedBpUserToBpPermission> getByPermission(BpPermission permission){
		Session hbSession = hibFacade.getHibernateSession();
		List<JoinedBpUserToBpPermission> result;
		try{
			result = hbSession.getNamedQuery("getJoinedByPermission").setParameter("bpPermission", permission).list();
		} catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("joined records cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return result;
	}
	
	List<JoinedBpUserToBpPermission> getByUser(BpUser user){
		Session hbSession = hibFacade.getHibernateSession();
		List<JoinedBpUserToBpPermission> result;
		try{
			result = hbSession.getNamedQuery("getJoinedByUser").setParameter("bpUser", user).list();
		} catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("joined records cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return result;
	}
	
}
