package com.bplatform.server.DAO.APIs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bplatform.server.DAO.DBDriver;
import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpTree;

/**
 * 
 * @author Anastasia Smakovska
 *
 */
public class BpTreeAPI {
	private Connection connection = null;
	
	@Inject
	private HibernateFacade hibFacade;
	
	public BpTreeAPI() {

		try {
			connection = DBDriver.getConnection();
		} catch (SQLException e) {
			System.out.println("Connection to database wasn't set.");
			e.printStackTrace();
		}

	}
	
	/**
	 * 
	 * Creates a new table with title <b>name</b>_bp_tree.  
	 * 
	 * @param prefixTableName is the prefix name of new table.
	 * @throws SQLException
	 */
	public void createTree(String prefixTableName) throws SQLException{
		
		//create string  with sql statement where specified table name
		StringBuilder sqlBuilder = new StringBuilder("CREATE  TABLE `");
		sqlBuilder = sqlBuilder.append(prefixTableName);
		sqlBuilder = sqlBuilder.append("_bp_tree` (`id` BIGINT NOT NULL , `name` VARCHAR(45) NOT NULL , `parent_id` BIGINT NULL DEFAULT 0 , `t_code` VARCHAR(45) NULL DEFAULT NULL , `connected_to` VARCHAR(45) NULL DEFAULT NULL ,  PRIMARY KEY (`id`) );");
		
		Statement statement = connection.createStatement();
		statement.execute(sqlBuilder.toString());
	}

	/**
	 * 
	 * Deletes a new table with title <b>prefixTableName</b>_bp_tree.  
	 * 
	 * @param prefixTableName is the prefix name of existed table.
	 * @throws SQLException
	 */
	public void deleteTree(String prefixTableName) throws SQLException{
		
		//create string  with sql statement where specified table name
		StringBuilder sqlBuilder = new StringBuilder("drop table `");
		sqlBuilder = sqlBuilder.append(prefixTableName);
		sqlBuilder = sqlBuilder.append("_bp_tree`;");
		
		Statement statement = connection.createStatement();
		statement.execute(sqlBuilder.toString());
	}

	/**
	 * 
	 * Reads from table <b>prefixTableName</b>_bp_tree all names.
	 * 
	 * @param prefixTableName is the prefix name of existed table where current search occurs.
	 * @return LinkedList of names
	 */
	public List<String> getListOfTrees(String prefixTableName) {
		
		List<String> res = new LinkedList<String>();
		
		try{
			//create string  with sql query where specified table name
			StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM `");
			sqlBuilder = sqlBuilder.append(prefixTableName);
			sqlBuilder = sqlBuilder.append("_bp_tree`;");
			
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sqlBuilder.toString());
			
			//for probable future needs 
			//long ID, parentID;
			String name; //, tCode, connectedTo;
			while(rs.next()){
				//ID = rs.getLong(1);
				name = rs.getString(2); 
				//parentID = rs.getLong(3);
				//tCode = rs.getString(4);
				//connectedTo = rs.getString(5);
				res.add(name);
			}
		}catch(SQLException e){
			System.out.println("Impossible to get records from database");
		}
		
		return res;

	}

	/**
	 * 
	 * Adds to database information about node according to passed variables.
	 * 
	 * @param prefixTableName is the prefix name of existed table where current search occurs.
	 * @param name 
	 * @param parentID
	 * @param tCode
	 * @param connectedTo
	 * 
	 * @return ID of the new added to database record.
	 */
	public String addToTree(String prefixTableName, String name, String parentID, String tCode, String connectedTo) {
		
		//String ID = random.nextLong();
		String ID = "";
		//end of change
		
		try{
			//create statement according to mentioned requirements and executes it
			PreparedStatement statement = createSpecifiedStatement(prefixTableName, ID, name, "0", tCode, connectedTo);
			statement.executeUpdate();
		}catch(SQLException e){
			System.out.println("Impossible to add a record to database");
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 
	 * Sets new data to record <b>bpTree</b> in table entitled <b>prefixTableName</b>_bp_tree. Chosen from database record have the same ID as <b>bpTree</b>.     
	 * 
	 * @param prefixTableName is the prefix name of existed table where current search occurs
	 * @param bpTree is the tree for update
	 * @return copy of <b>bpTree</b> if a new data was set to database record or null in case when impossible to set the new data
	 * 
	 */
	public BpTree setToTree(String prefixTableName, BpTree bpTree) {
		BpTree node = null;
		//create string  with sql command where specified table name
		StringBuilder sqlBuilder = new StringBuilder("UPDATE `");
		sqlBuilder = sqlBuilder.append(prefixTableName);
		sqlBuilder = sqlBuilder.append("_bp_tree` SET `name` =  ?, `parent_id` = ?, `t_code` = ?, `connected_to` = ? WHERE `id` = ?");
		
		try{
			//create a new statement, add parameters, and execute it 
			PreparedStatement statement = connection.prepareStatement(sqlBuilder.toString());
			
			statement.setString(1, bpTree.getName());
			statement.setString(2, bpTree.getParentId());
			statement.setString(3, bpTree.getTCode());
			statement.setString(4, bpTree.getConnectedTo());
			statement.setString(5, bpTree.getId());
			
			statement.executeUpdate();
			//make a copy for return
			node = new BpTree(bpTree);
		}catch(SQLException e){
			System.out.println("Record can't be changed");
			e.printStackTrace();
		}
		return node;
	}

	/**
	 * Sets new data to record according to passed parameters in table entitled <b>prefixTableName</b>_bp_tree. Chosen from database record have the same ID as passed <b>ID</b>.
	 * 
	 * @param prefixTableName is the prefix name of existed table where current search occurs
	 * @param ID
	 * @param name
	 * @param parentID
	 * @param tCode
	 * @param connectedTo
	 * @return copy of <b>bpTree</b> if a new data was set to database record or null in case when impossible to set the new data
	 */
	public BpTree setToTree(String prefixTableName, String ID, String name,
			String parentID, String tCode, String connectedTo) {
		
		BpTree node = null;
		//create string  with sql command where specified table name
		StringBuilder sqlBuilder = new StringBuilder("UPDATE `");
		sqlBuilder = sqlBuilder.append(prefixTableName);
		sqlBuilder = sqlBuilder.append("_bp_tree` SET `name` =  ?, `parent_id` = ?, `t_code` = ?, `connected_to` = ? WHERE `id` = ?");
		
		try{
			//create a new statement, add parameters, and execute it 
			PreparedStatement statement = connection.prepareStatement(sqlBuilder.toString());
			
			statement.setString(1, name);
			statement.setString(2, parentID);
			statement.setString(3, tCode);
			statement.setString(4, connectedTo);
			statement.setString(5, ID);
			statement.executeUpdate();
			//construct a new object according to passed parametrs
			node = new BpTree(ID, name, parentID, tCode, connectedTo);
		}catch(SQLException e){
			System.out.println("Record can't be changed");
			e.printStackTrace();
		}
		return node;
	}
	
	/**
	 * 
	 * Creates list of BpTree with id contained <b>listID</b>
	 * 
	 * @param prefixTableName is the prefix name of existed table where current search occurs
	 * @param listID contains the list of IDs of BpTree objects, which must be selected from database
	 *
	 * @return the list of BpTree with mentioned IDs
	 */
	public List<BpTree> getFromTree(String prefixTableName, List<Long> listID){
		List<BpTree> res = new LinkedList<BpTree>();
		BpTree node;
		//create string  with sql query where specified table name
		StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM `");
		sqlBuilder = sqlBuilder.append(prefixTableName);
		sqlBuilder = sqlBuilder.append("_bp_tree` WHERE `id` = ?;");
		try{
			PreparedStatement statement = connection.prepareStatement(sqlBuilder.toString());
			ResultSet rs;
			for(long id : listID){
				//create statement with each id
				statement.setDouble(1, id);
				//execute and get query result item
				rs = statement.executeQuery();
				while(rs.next()){
					//construct a new tree node and add it to result list
					node = new BpTree(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
					res.add(node);
				}
			}
		}catch(SQLException e){
			System.out.println("Impossible to get records from database");
			e.printStackTrace();
		}
		return res;
	}
	
	/**
	 * 
	 * Creates list of BpTree with tCode variable equals contained <b>tCode</b>
	 * 
	 * @param prefixTableName is the prefix name of existed table where current search occurs
	 * @param tCode
	 * 
	 * @return the list of BpTree with mentioned <b>tCode</b>
	 */
	public List<BpTree> getFromTree(String prefixTableName, String tCode){
		List<BpTree> res = new LinkedList<BpTree>();
		
		//create string  with sql query where specified table name
		StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM `");
		sqlBuilder = sqlBuilder.append(prefixTableName);
		sqlBuilder = sqlBuilder.append("_bp_tree` WHERE `t_code` = ?;");
		
		try{
			//praparing statement with mentioned tCode
			PreparedStatement statement = connection.prepareStatement(sqlBuilder.toString());
			statement.setString(1, tCode);
			ResultSet rs = statement.executeQuery();
	 
			BpTree node; 
			String ID, parentID;
			String name, connectedTo;
			while(rs.next()){
				ID = rs.getString(1);
				name = rs.getString(2);
				parentID = rs.getString(3);
				//no necessity to get tCode
				//tCode = rs.getString(4);
				connectedTo = rs.getString(5);
				//construct a new tree node and add it to result list
				node = new BpTree(ID, name, parentID, tCode, connectedTo);
				res.add(node);
			}
			
		}catch(SQLException e){
			System.out.println("Impossible to get records from database");
			e.printStackTrace();
		}
		
		return res;
	}
	/**
	 * Creates prepared statement of INSERT command  to <b>prefixTableName</b>_bp_tree table according to parameters 
	 * 
	 * @param prefixTableName is the prefix name of existed table where current search occurs
	 * @param ID
	 * @param name
	 * @param parentID
	 * @param tCode
	 * @param connectedTo
	 * @return completed prepared statement
	 * @throws SQLException
	 */
	private PreparedStatement createSpecifiedStatement(String treeName, String ID,  String name, String parentID, String tCode,	String connectedTo) throws SQLException{
		
		StringBuilder sqlBuilder = new StringBuilder("INSERT INTO `");
		sqlBuilder = sqlBuilder.append(treeName);
		sqlBuilder = sqlBuilder.append("_bp_tree` (`id`, `name`, `parent_id`, `t_code` ,`connected_to`) VALUES (?, ?, ?, ?, ?);");
		
		PreparedStatement statement = connection.prepareStatement(sqlBuilder.toString());
		//fulfil prerape statement
		statement.setString(1, ID);
		statement.setString(2, name);
		statement.setString(3, parentID);
		statement.setString(4, tCode);
		statement.setString(5, connectedTo);
		
		return statement;
	}
	
	
	public <T extends BpTree> String saveAnyTreeNode(T node) {
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			//This operation cascades to associated instances if the association is mapped with cascade="save-update".
			hbSession.saveOrUpdate(node);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		} finally {
			hbSession.close();
		}
		return node.getId();
	}
}