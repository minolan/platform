package com.bplatform.server.DAO.APIs;

import java.util.HashSet;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpPermission;
import com.bplatform.server.DAO.models.BpPermissionGroup;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.BpUserGroup;
import com.bplatform.shared.exceptions.DataBaseException;

/**
 * 
 * @author Anastasia Smakovska
 *
 * 4 ����. 2013
 *
 */
@ApplicationScoped
public class BpPermissionGroupAPI {

	@Inject
	private HibernateFacade hibFacade;
	
	public BpPermissionGroupAPI(){}

	public void delete(BpPermissionGroup permissionGroup) throws DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			hbSession.delete(permissionGroup);
			transaction.commit();
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("permission group cannot be deleted from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
	}

	public void save(BpPermissionGroup permissionGroup) throws ConstraintViolationException, DataBaseException {
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			//This operation cascades to associated instances if the association is mapped with cascade="save-update".
			hbSession.save(permissionGroup);
			transaction.commit();
			hbSession.flush();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; //trying to save permission group with null value of field or too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("permission group cannot be saved to in the data base by unresolved issue");
		} finally {
			hbSession.close();
		}
	}
	
	public void update(BpPermissionGroup permissionGroup) throws ConstraintViolationException, DataBaseException{
		Session hbSession = hibFacade.getHibernateSession();
		Transaction transaction = null;
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();
			//This operation cascades to associated instances if the association is mapped with cascade="save-update".
			hbSession.update(permissionGroup);
			transaction.commit();
			hbSession.flush();
		} catch(ConstraintViolationException e){
			transaction.rollback();
			throw e; // trying to update permission group with null value of field or too long string
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DataBaseException("permission group cannot be updated to in the data base by unresolved issue");
		}finally {
			hbSession.close();
		}
	}
	
	public BpPermissionGroup getById(String id) throws DataBaseException {
		BpPermissionGroup permissionGroup = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			permissionGroup = (BpPermissionGroup)hbSession.get(BpPermissionGroup.class, id);
			hbSession.flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataBaseException("permission group cannot be recieved from the database by unresolved issue");
		} finally {
			hbSession.close();
		}
		return permissionGroup;
	}

	public List<BpUserGroup> initializeBpUserGroups(BpPermissionGroup permissionGroup){
		List<BpUserGroup> userGroups;
		Session hbSession = hibFacade.getHibernateSession();
		try{
			userGroups = (List<BpUserGroup>)hbSession.createCriteria(BpUserGroup.class, "userGroup").createAlias("user.permissionGroups", "permissionGroup").add(Restrictions.eq("permissionGroup.id", permissionGroup.getId())).list();
			permissionGroup.setBpUserGroups(new HashSet<BpUserGroup>(userGroups));
		}catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("BpUserGroups cannot be recieved from the database by unresolved issue");
		} finally{
			hbSession.close();
		}
		return userGroups;
	}

	public List<BpPermission> initializeBpPermissions(BpPermissionGroup permissionGroup){
		List<BpPermission> permissions;
		Session hbSession = hibFacade.getHibernateSession();
		try{
			permissions = (List<BpPermission>)hbSession.createCriteria(BpPermission.class, "permission").createAlias("permission.bpPermissionGroup", "permisionGroup").add(Restrictions.eq("permissionGroup.id", permissionGroup.getId())).list();
			permissionGroup.setPermissions(new HashSet<BpPermission>(permissions));
		}catch(Exception e){
			e.printStackTrace();
			throw new DataBaseException("BpUserGroups cannot be recieved from the database by unresolved issue");
		} finally{
			hbSession.close();
		}
		return permissions;
	}
	
}
