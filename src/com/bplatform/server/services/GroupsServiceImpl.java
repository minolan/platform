package com.bplatform.server.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.mainModule.services.GroupsService;
import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpUserGroup;
import com.bplatform.shared.exceptions.DuplicateNameException;
import com.bplatform.shared.exceptions.NameValidationException;
import com.bplatform.shared.validators.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * 
 * RPC Implementation Own User Groups
 * 
 * @author RomanL
 */
@WebServlet
public class GroupsServiceImpl extends RemoteServiceServlet implements
		GroupsService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private HibernateFacade hibFacade;

	/**
	 * Get Group Properties
	 */
	@Override
	public BpGroupDTO getGroupProperties(String groupId) {
		BpGroupDTO bpGroupDTO = new BpGroupDTO();
		if (groupId != null) {
			BpUserGroup bpUserGroup = hibFacade.getBpUserGroupAPI().getById(groupId);
			
			if (bpUserGroup == null) {
				throw new NullPointerException("Group with ID:" + groupId + ", don't exist!");
			} else {
				bpGroupDTO.setId(bpUserGroup.getId());
				bpGroupDTO.setName(bpUserGroup.getName());
				bpGroupDTO.setParentId(bpUserGroup.getParentId());
			}
			
		} else {
			throw new NullPointerException("Group ID is NULL!");
		}

		return bpGroupDTO;
	}

	/**
	 * ADD or Update Group
	 */
	@Override
	public BpGroupDTO addOrUpdateGroup (BpGroupDTO bpGroupDTO) throws Exception {

		String resultOfTransaction = null;
		BpGroupDTO retBpGroupDTO = new BpGroupDTO();
		
		try {
						
			if (FieldVerifier.isValidName(bpGroupDTO.getName())) {
				
				if (bpGroupDTO.getId() == null) {
					dublicateGroupNameChecker (bpGroupDTO);
					BpUserGroup bpUserGroup = new BpUserGroup(bpGroupDTO.getName(), bpGroupDTO.getParentId());
					hibFacade.getBpUserGroupAPI().save(bpUserGroup);
					
					// TODO: optimize return Entity
					BpUserGroup retBpUserGroup = hibFacade.getBpUserGroupAPI().getGroupByName(bpUserGroup.getName());
					retBpGroupDTO = new BpGroupDTO(retBpUserGroup.getName(), retBpUserGroup.getId(), retBpUserGroup.getParentId());
					resultOfTransaction = "Group was successfully added!";
					
					System.out.println("Add new group, Name is: " + bpGroupDTO.getName() + " ParenId is: " + bpGroupDTO.getParentId() +
							" and groupId is:" + bpGroupDTO.getId());
				} else {
					dublicateGroupNameChecker (bpGroupDTO);
					resultOfTransaction = updateGroup(bpGroupDTO);
					System.out.println("Update group, with Name: " + bpGroupDTO.getName() + " and ParenId: " + bpGroupDTO.getParentId() + 
							" and groupId is:" + bpGroupDTO.getId());
				}
								
			} else {
				
				throw new NameValidationException("Name is: "
						+ bpGroupDTO.getName() + ". Parent group is: "
						+ bpGroupDTO.getParentId());
			
			}

		} catch (Exception e) {
			e.printStackTrace();
			resultOfTransaction = "Group was not added!";
			throw e;
		}

//		return resultOfTransaction;
		retBpGroupDTO.setResultOfTransaction(resultOfTransaction);
		return retBpGroupDTO;
	}


	/**
	 * DELETE group
	 */
	@Override
	public Map<String, String> deleteGroup(String groupId) throws Exception {
		BpUserGroup bpUserGroup = null;
		String resultOfTransaction = null;
		Map<String, String> resultOfTransactionMap = new HashMap<String, String>();
		
		if (groupId != null) {
			List<BpUserGroup> groupChildList = hibFacade.getBpUserGroupAPI().getChildrenGroups(groupId);
			
			if (groupChildList.isEmpty()) {
				Set<String> usersSetFromGroup = hibFacade.getBpUserGroupAPI().getByIdUserList(groupId);
				
				if (usersSetFromGroup.isEmpty()) {
					bpUserGroup = hibFacade.getBpUserGroupAPI().getById(groupId);
//						if (bpUserGroup.getParentId() != null) {
//							hibFacade.getBpUserGroupAPI().delete(bpUserGroup);
//							resultOfTransaction = "Group was delete successfully!";
//							resultOfTransactionMap.put("ok", "Group was delete successfully!");
//						} else {
//							resultOfTransaction = "You can't delete Root Category!";
//							resultOfTransactionMap.put("no", "You can't delete Root Category!");
//						}
					hibFacade.getBpUserGroupAPI().delete(bpUserGroup);
					resultOfTransaction = "Group was delete successfully!";
					resultOfTransactionMap.put("ok", "Group was delete successfully!");
				} else {
					resultOfTransaction = "Group contains next user(s): " + usersSetFromGroup.toString() +".\nPlease, unassign them previously";
					resultOfTransactionMap.put("no", "Group contains next user(s): " + usersSetFromGroup.toString() +".\nPlease, unassign them previously");				}
				
			} else {
				resultOfTransaction = "Group contains children groups! Please, delete them previously";
				resultOfTransactionMap.put("no", "Group contains children groups! Please, delete them previously");				
			}
			
		} else {
			resultOfTransaction = "Group ID is NULL!";
			resultOfTransactionMap.put("no", "Group ID is NULL!");	
		}

		return resultOfTransactionMap;
	}
	
	private void dublicateGroupNameChecker(BpGroupDTO bpGroupDTO) throws DuplicateNameException {
		BpUserGroup tempBpGroupName = hibFacade.getBpUserGroupAPI().getGroupByName(bpGroupDTO.getName());
		if (tempBpGroupName != null && !tempBpGroupName.getId().equals(bpGroupDTO.getId())) {
			throw new DuplicateNameException();
		}
	}
	
	private String updateGroup (BpGroupDTO bpGroupDTO) {
		
		BpUserGroup bpUserGroup = hibFacade.getBpUserGroupAPI().getById(bpGroupDTO.getId());
		String resultOfTransaction = null;
		boolean parentIdEquals = false;
		
		if (bpGroupDTO.getName() == null || bpUserGroup.getName() == null) {
			resultOfTransaction = "Something going wrong!";
			return resultOfTransaction;
		}
		
		if (bpGroupDTO.getParentId() == null || bpUserGroup.getParentId() == null) {
			parentIdEquals = (bpGroupDTO.getParentId() == bpUserGroup.getParentId());
		} else {
			parentIdEquals = bpGroupDTO.getParentId().equals(bpUserGroup.getParentId());
		}
		
		
		if (bpGroupDTO.getName().equals(bpUserGroup.getName()) && parentIdEquals) {
			resultOfTransaction = "Nothing changes!";
		} else {
			bpUserGroup.setName(bpGroupDTO.getName());
			bpUserGroup.setParentID(bpGroupDTO.getParentId());					
			hibFacade.getBpUserGroupAPI().update(bpUserGroup);
			resultOfTransaction = "Group was successfully updated!";
			System.out.println("Update group, with Name: " + bpGroupDTO.getName() + " and ParenId: " + bpGroupDTO.getParentId());
		}
		
		return resultOfTransaction;
		
	}

}