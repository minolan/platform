package com.bplatform.server.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bplatform.client.bpShared.DTO.BpGroupDTO;
import com.bplatform.client.bpShared.DTO.BpUserGroupFoldersDTO;
import com.bplatform.client.bpShared.DTO.BpUserGroupDTO;
import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.bplatform.client.mainModule.services.UsersService;
import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.BpUserGroup;
import com.bplatform.shared.exceptions.EmailValidationException;
import com.bplatform.shared.exceptions.EmptyGroupListException;
import com.bplatform.shared.exceptions.NameValidationException;
import com.bplatform.shared.exceptions.PasswordNotMatchException;
import com.bplatform.shared.exceptions.PasswordValidationException;
import com.bplatform.shared.validators.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.sencha.gxt.data.shared.SortDir;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import com.sencha.gxt.data.shared.loader.FilterPagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;

/**
 * 
 * RPC Implementation Own User Groups
 * 
 * @author RomanL
 */
@WebServlet
public class UsersServiceImpl extends RemoteServiceServlet implements
		UsersService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	private MainFacade appFacade;

	@Inject
	private HibernateFacade hibFacade;

	private static int autoId = 0;
	BpUserGroupFoldersDTO root;
	BpUserGroupFoldersDTO bpGroup;
	List<String> filedsList;
	
	public UsersServiceImpl () {
		filedsList = new ArrayList<>();
		filedsList.add("email");
		filedsList.add("fio");
		filedsList.add("login");
	}

	// Start get user properties
	@Override
	public BpUserDTO getUserProperties(String login) {
		BpUserDTO userDTO = null;
		BpUser bpUser = null;

		if (login == null) {
			userDTO = appFacade.getSessionContext().getSessionUser();
			bpUser = hibFacade.getBpUserAPI().getByLogin(userDTO.getLogin());
		} else {
			bpUser = hibFacade.getBpUserAPI().getByLogin(login);
		}

		// Get User's info
		BpUserDTO bpUserDTO = new BpUserDTO();
		bpUserDTO.setLogin(bpUser.getLogin());
		bpUserDTO.setEmail(bpUser.getEmail());
		bpUserDTO.setPassword(bpUser.getPassword());
		bpUserDTO.setFio(bpUser.getFio());

		// Get binding user's to groups
		Set<BpGroupDTO> setBpGroupDTO = new HashSet<BpGroupDTO>();
		if (bpUser.getBpUserGroups() != null
				&& bpUser.getBpUserGroups().isEmpty() == false) {
			for (BpUserGroup bpUserGroup : bpUser.getBpUserGroups()) {
				BpGroupDTO bpGroupDTO = new BpGroupDTO();
				bpGroupDTO.setId(bpUserGroup.getId());
				bpGroupDTO.setName(bpUserGroup.getName());
				setBpGroupDTO.add(bpGroupDTO);
			}
			bpUserDTO.setGroups(setBpGroupDTO);
		} else {
			System.out.println("User does not consist in any of groups!");
		}
		return bpUserDTO;
	}
	// End get user properties
	
	// Get paging list of groups
	public PagingLoadResult<BpUserDTO> getUsers(FilterPagingLoadConfig config) {
		List<BpUserDTO> userListAllDTO = new ArrayList();
		List<BpUser> userListAll = null;
		
		if (!config.getSortInfo().isEmpty() && config.getSortInfo() != null) {
			for(SortInfo si: config.getSortInfo()) {
				if (filedsList.contains(si.getSortField())) {
					if (si.getSortDir().name() == "ASC") {
						userListAll = hibFacade.getBpUserAPI().getAllUsersOrderByASC(si.getSortField()); 
					} else if (si.getSortDir().name() == "DESC") {
						userListAll = hibFacade.getBpUserAPI().getAllUsersOrderByDESC(si.getSortField());
					} else {
						userListAll = hibFacade.getBpUserAPI().getAllUsers();
					}
				} else {
					userListAll = hibFacade.getBpUserAPI().getAllUsers();
				}
			}
		} else {
			userListAll = hibFacade.getBpUserAPI().getAllUsers();
		}
		
		if (!config.getFilters().isEmpty()) {
			
			userListAll = getFilteredUsers(config.getFilters(), userListAll);
//			for (FilterConfig fc: config.getFilters()) {
//				
//				System.out.println("-----------------------" + fc.getComparison() + "------------------------------");
//				System.out.println("-----------------------" + fc.getField() + "------------------------------");
//				System.out.println("-----------------------" + fc.getType() + "------------------------------");
//				System.out.println("-----------------------" + fc.getValue() + "------------------------------");
//				
//				List<BpUser> userListFiltered = new ArrayList(); 
//				for (BpUser bpUser: userListAll) {
//					if (stringMatcher (bpUser.getLogin(), fc.getValue())) {
//						userListFiltered.add(bpUser);
//					}
//				}
//				userListAll = userListFiltered;
//			}
			
		}
				
		for (BpUser user: userListAll) {
			BpUserDTO userDTO = new BpUserDTO(user.getId(), user.getLogin(), user.getPassword(), user.getFio(), user.getEmail());
			userListAllDTO.add(userDTO);
			
		}
				
		ArrayList<BpUserDTO> sublist = new ArrayList<BpUserDTO>();
		int start = config.getOffset();
		int limit = userListAllDTO.size();
		if (config.getLimit() > 0) {
		limit = Math.min(start + config.getLimit(), limit);
		}
		for (int i = config.getOffset(); i < limit; i++) {	
			sublist.add(userListAllDTO.get(i));
		}       
		return new PagingLoadResultBean<BpUserDTO>(sublist, userListAllDTO.size(), config.getOffset());
//				<BpUserDTO>(sublist, config.getOffset(), userListAllDTO.size());
	}
	
	//TempSolution
	@Override
	public List<BpUserDTO> getUsers() {
		// TODO Auto-generated method stub
		List<BpUser> userListAll = hibFacade.getBpUserAPI().getAllUsers();
		List<BpUserDTO> userListAllDTO = new ArrayList();
		for (BpUser user: userListAll) {
			BpUserDTO userDTO = new BpUserDTO(user.getId(), user.getLogin(), user.getPassword(), user.getFio(), user.getEmail());
			userListAllDTO.add(userDTO);
			
		} 
		return userListAllDTO;
	}

	// Start save User Properties
	@Override
	public String setUserProperties(BpUserDTO setUserProp) throws Exception {

		String resultOfTransaction = null;

		BpUser bpu;

		Transaction transaction = null;
		Session hbSession = hibFacade.getHibernateSession();
		try {
			transaction = hbSession.getTransaction();
			transaction.begin();

			bpu = (BpUser) hbSession.getNamedQuery("getBpUserByLogin")
					.setString("login", setUserProp.getLogin()).uniqueResult();

			if (FieldVerifier.isValidMail(setUserProp.getEmail().toLowerCase())) {
				bpu.setEmail(setUserProp.getEmail().toLowerCase());
			} else {
				throw new EmailValidationException();
			}

			if (!FieldVerifier.isValidPassword(setUserProp.getPassword())) {
				throw new PasswordValidationException();
			} else if (!setUserProp.getPassword().equals(
					setUserProp.getPasswordRetype())) {
				throw new PasswordNotMatchException();
			} else {
				bpu.setPassword(setUserProp.getPassword());
			}

			if (FieldVerifier.isValidName(setUserProp.getFio())) {
				bpu.setFio(setUserProp.getFio());
			} else {
				throw new NameValidationException();
			}

			Set<BpUserGroup> bpugSet = new HashSet<BpUserGroup>();
			BpUserGroup bpug;
			for (BpGroupDTO bpugDTO : setUserProp.getGroups()) {
//				BpUserGroup bpug = new BpUserGroup(bpugDTO.getGroupId(), bpugDTO.getName());
				bpug = new BpUserGroup(bpugDTO.getName());
				
				bpugSet.add(bpug);
			}

			bpu.setBpUserGroups(bpugSet);

			hbSession.saveOrUpdate(bpu);
			transaction.commit();
			resultOfTransaction = "Профиль пользователя успешно обновлен";
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
				// resultOfTransaction = "Профиль пользователя не обновлен";
			}
			throw e;
		} finally {
			hbSession.close();
		}

		return resultOfTransaction;
	}
	// End save User Properties

	// Begin Get All Groups
	@Override
	public List<BpUserGroupFoldersDTO> getGroupsList() throws Exception {

		// Session hbSession =
		// appFacade.getHibernateFacade().getHibernateSession();

		List<BpUserGroupFoldersDTO> groupListDTO = new ArrayList<BpUserGroupFoldersDTO>();
		try {
//			List<BpUserGroup> bpUG = new ArrayList<BpUserGroup>();

			List<BpUserGroup> bpUserGroup = hibFacade.getBpUserGroupAPI().getChildrenGroups(null);
//			List<BpUserGroup> bpUserGroup = hibFacade.getBpUserGroupAPI().getAllGroups();
			
			if (bpUserGroup == null || bpUserGroup.isEmpty()) {
				throw new EmptyGroupListException("There is no one group. Please add at least one group");
			}
			
//			for (BpUserGroup i : bpUserGroup) {
//				if (i.getParentId() == null) {
//					bpUG.add(i);
//				}
//			}

			for (BpUserGroup gL : bpUserGroup) {
				root = makeFolder("Root", gL.getId());
				BpUserGroupFoldersDTO bpGroup = makeFolder(gL.getName(),
						gL.getId());
				List<BpGroupDTO> bpGroupChildren = new ArrayList<BpGroupDTO>();
				bpGroupChildren.add(bpGroup);
				root.setChildren(bpGroupChildren);

				bpGroup = getChildrenGroupList(gL.getId(), bpGroup);

				groupListDTO.add(root);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

		return groupListDTO;
	}

	// End Get All Groups

	// Begin Collect Users from Group
	@Override
	public Set<String> getUsersListFromGroup(String bpGroupId) {
		Set<String> bpUserDTOSet = hibFacade.getBpUserGroupAPI()
				.getByIdUserList(bpGroupId);
		return bpUserDTOSet;
	}
	// End Collect Users from Group

	// Begin groups tree methods
	public BpUserGroupFoldersDTO getChildrenGroupList(String id,
			BpUserGroupFoldersDTO bpGroup) {
		
		try {
		List<BpUserGroup> groupChildList = hibFacade.getBpUserGroupAPI()
				.getChildrenGroups(id);

		for (BpUserGroup gChL : groupChildList) {

			BpUserGroupFoldersDTO bpGroupUpper = makeFolder(gChL.getName(),
					gChL.getId());
			getChildrenGroupList(gChL.getId(), bpGroupUpper);
			bpGroup.addChild(bpGroupUpper);
		}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return bpGroup;
	}

	private BpUserGroupFoldersDTO makeFolder(String name, String id) {
		BpUserGroupFoldersDTO theReturn = new BpUserGroupFoldersDTO(++autoId,
				name, id);
		theReturn.setChildren((List<BpGroupDTO>) new ArrayList<BpGroupDTO>());
		return theReturn;
	}
	
	private boolean stringMatcher (String src, String search) {
		boolean compare = src.matches("(?i).*" + search + ".*");
		return compare;
	}
	
	private List<BpUser> getFilteredUsers(List<FilterConfig> fcl, List<BpUser> usersListAll) {
		List<BpUser> userListFiltered;
		for (FilterConfig fc: fcl) {
				
//				System.out.println("-----------------------" + fc.getComparison() + "------------------------------");
//				System.out.println("-----------------------" + fc.getField() + "------------------------------");
//				System.out.println("-----------------------" + fc.getType() + "------------------------------");
//				System.out.println("-----------------------" + fc.getValue() + "------------------------------");
				
				userListFiltered = new ArrayList(); 
				switch (fc.getField())
				{
				case "login":
					for (BpUser bpUser: usersListAll) {
						if (stringMatcher (bpUser.getLogin(), fc.getValue())) {
							userListFiltered.add(bpUser);
						}
					}
//					System.out.println("-------------+++Login");
					break;
				case "email":
					for (BpUser bpUser: usersListAll) {
						if (stringMatcher (bpUser.getEmail(), fc.getValue())) {
							userListFiltered.add(bpUser);
						}
					}
//					System.out.println("-------------+++Email");
					break;
				case "fio":
					for (BpUser bpUser: usersListAll) {
						if (stringMatcher (bpUser.getFio(), fc.getValue())) {
							userListFiltered.add(bpUser);
						}
					}
//					System.out.println("-------------+++FIO");
					break;
				default:
					userListFiltered = usersListAll;
//					System.out.println("-------------+++Default");
				}
				usersListAll = userListFiltered;
			}
		return usersListAll;
	}

	// End groups tree methods

	// Should be removed if not used: need for showing users in Groups Tree ===>
	private BpUserGroupDTO makeMusic(String name,
			BpUserGroupFoldersDTO author, BpUserGroupFoldersDTO genre) {
		return makeMusic(name, author.getName(), genre.getName());
	}

	private BpUserGroupDTO makeMusic(String name, String author,
			String genre) {
		return new BpUserGroupDTO(++autoId, name, genre, author);
	}
	// End remove <===

}
