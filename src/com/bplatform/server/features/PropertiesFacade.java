package com.bplatform.server.features;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.bplatform.server.MainFacade;
import com.bplatform.shared.exceptions.NoCustomPropertiesException;

@ApplicationScoped
public class PropertiesFacade {
	private String[] localesName = null;
	private String[] localeResource = null;
	private List<String> allLocalesListPath = new ArrayList<String>();
	private List<String> allCustomList = new ArrayList<String>();
	private List<String> localePropLisFullName = new ArrayList<String>();
	private String defaultLocale = null;

	private ResourceBundle defaultLocalProp = null;
	/**
	 * locales-locales.properties localMap-BP_Language_resource.properties
	 * customPropMap- customPropName.properties
	 */
	private PropertyResourceBundle locales = null;
	private HashMap<String, HashMap<String, ResourceBundle>> localeMap = null;
	private HashMap<String, ResourceBundle> customPropMap = null;

	@Inject
	private MainFacade appFacade;
	
	private String localesPath;
	private String localesPropertiesFilename = "locales.properties";

	/**
	 * 
	 * ���������� ����� ���� ������� ��� ���� �������� hashmap<S1,S2,R> S1-����
	 * ������, S2-������,R- ResourceBundle ��� ���� �������� . ��������������
	 * ������� ������ � defaultLocalProp , ������ ��������� ������� � localPropLis
	 * , ������ ��������� ������� localPropLis, ����� ��������� ���������
	 * 
	 * 
	 */
	@PostConstruct
	public void initLocales() {

		localeMap = new HashMap<String, HashMap<String, ResourceBundle>>();
		
		StringBuilder localesPathBuilder = new StringBuilder(appFacade.getAppProperties().getRootPath());
		localesPathBuilder.append(File.separator);
		localesPathBuilder.append("WEB-INF");
		localesPathBuilder.append(File.separator);
		localesPathBuilder.append("config");
		localesPathBuilder.append(File.separator);
		localesPath = localesPathBuilder.toString();
		
		InputStream is = null;

		try {
			File conf = new File(localesPath + localesPropertiesFilename);
			is = new FileInputStream(conf);
			locales = new PropertyResourceBundle(is);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			defaultLocale = locales.getString("defaultLocale");
			String localesNameTemp = locales.getString("locales");
			String localeResourceTemp = locales.getString("localeResource");
			localesName = localesNameTemp.split(",");
			localeResource = localeResourceTemp.split(",");
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			StringBuilder builder = new StringBuilder(localesPath);
			File confDefault = new File(builder.append("BP_").append(defaultLocale).append(".properties").toString());
			if (confDefault.exists()) {
				InputStream isF = new FileInputStream(confDefault);
				defaultLocalProp = new PropertyResourceBundle(isF);
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		StringBuilder localeResouceFilenameBuilder;
		String localeResouceFilename;
		for (String locName : localesName) {
			for (String resource : localeResource) {

				ResourceBundle local = null;
				try {
					
					localeResouceFilenameBuilder = new StringBuilder("BP_");
					localeResouceFilenameBuilder.append(locName);
					localeResouceFilenameBuilder.append('_');
					localeResouceFilenameBuilder.append(resource);
					localeResouceFilenameBuilder.append(".properties");
					localeResouceFilename = localeResouceFilenameBuilder.toString();
					
					//File conf = new File(appFacade.getAppProperties().getRootPath() + File.separator + "restricted" + File.separator + "config" + File.separator + "BP_" + locName + "_" + resource + ".properties");
					File conf = new File(localesPath + localeResouceFilename);
					if (conf.exists()) {
						localePropLisFullName.add(localeResouceFilename);
						allLocalesListPath.add(conf.getAbsolutePath());
						InputStream isF = new FileInputStream(conf);
						local = new PropertyResourceBundle(isF);

						HashMap<String, ResourceBundle> temp = new HashMap<String, ResourceBundle>();
						temp.put(resource, local);
						localeMap.put(locName, temp);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

	}

	/**
	 * 
	 * ���������� ����� HashMap<���_�����, ResourceBundle> ���� ���������
	 * ��������� (��� ��� �������� ������� ���������� �� � BP_)
	 * 
	 * 
	 */
	public HashMap<String, ResourceBundle> getCustomBundle() {

		
		// HashMap<String, ResourceBundle> customFileMap = new HashMap<String,
		// ResourceBundle>();

		File[] fList = null;
		File dirF = new File(localesPath);
		//File dirF = new File(appFacade.getAppProperties().getRootPath() + File.separator + "restricted" + File.separator + "config" + File.separator);
		fList = dirF.listFiles();

		for (File e : fList) {
			for (String fLoc : allLocalesListPath) {
				ResourceBundle customBundle = null;
				if (!e.getAbsolutePath().equals(fLoc)) {
					try {
						InputStream isF = new FileInputStream(e);
						customBundle = new PropertyResourceBundle(isF);
						customPropMap.put(e.getName(), customBundle);
						allCustomList.add(e.getName());
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}

				}

			}
		}
		return customPropMap;

	}

	/**
	 * ���������� ����� ��������� � ������ ���������� (��������� ������ �������
	 * � � �������� �� ���������� ������ localMap, ���� ����� ������ ��
	 * ���������� �� �������- ���� �� �������������� )
	 * 
	 * @param fileName
	 *            ��� ����� �������� ���� ������_������
	 * @param res
	 *            ����� �������� ��� ��������
	 */
	public void saveToFile(String fileName, HashMap<String, String> res) {

		HashMap<String, String> oldLoc = new HashMap<String, String>();
		String[] fileNameValue = fileName.split("_");
		ResourceBundle oldRes = localeMap.get(fileNameValue[0]).get(fileNameValue[1]);

		Enumeration bundleKeys = oldRes.getKeys();
		while (bundleKeys.hasMoreElements()) {
			String key = (String) bundleKeys.nextElement();
			String value = oldRes.getString(key);
			oldLoc.put(key, value);
		}

		if (!res.entrySet().containsAll(oldLoc.entrySet()) && !oldLoc.entrySet().containsAll(res.entrySet())) {
			
			StringBuilder filenameBuilder = new StringBuilder(localesPath);
			filenameBuilder.append("BP_");
			filenameBuilder.append(fileName);
			filenameBuilder.append(".properties");
			
			//File file = new File(appFacade.getAppProperties().getRootPath() + File.separator + "restricted" + File.separator + "config" + File.separator + "BP_" + fileName + ".properties");
			File file = new File(filenameBuilder.toString());
			FileWriter fw = null;
			try {
				fw = new FileWriter(file);
				Set<String> keySet = res.keySet();
				for (String key : keySet) {
					String value = res.get(key);
					fw.write(key + "=" + value + "\r\n");

				}
				fw.flush();
				ResetLocales();
			} catch (IOException e1) {
				e1.printStackTrace();
			} finally {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * ������������ ������ getLocales()
	 */
	public void ResetLocales() {
		initLocales();
	}

	/**
	 * 
	 * returns default locale resources of locales.properties
	 * 
	 * 
	 */
	public ResourceBundle getDefaultLocalProp() {
		return defaultLocalProp;
	}

	/**
	 * 
	 * ���������� (ResourceBundle) �������� ��� ��������� ���� � ������ �� �����
	 * BP_����_������.properties, ���� ����� �������� �� ���������� ����������
	 * ������� ������
	 * 
	 * @param locale
	 * @param localeResourse
	 * @return
	 */
	public ResourceBundle getLocalePropMapForLocAndResours(String locale, String localeResourse) {
		
		StringBuilder resourceBuilder = new StringBuilder("BP_");
		resourceBuilder.append(locale);
		resourceBuilder.append(' ');
		resourceBuilder.append(localeResourse);
		resourceBuilder.append(".properties");
		//String resourse = "BP_" + local + "_" + localResourse + ".properties";
		String resourse = resourceBuilder.toString();
		
		for (String l : localePropLisFullName) {
			if (l.equals(resourse)) {
				HashMap<String, ResourceBundle> localResourseMap = localeMap.get(locale);
				ResourceBundle localResourseTemp = localResourseMap.get(localeResourse);

				return localResourseTemp;
			}
		}

		return defaultLocalProp;

	}

	/**
	 * ���������� (ResourceBundle) �������� ��� ��������� ��������
	 * (��������.properties) customPropName ,���� ����� �������� �� ����������
	 * ���������� NoCustomPropertiesException
	 * 
	 * @param customPropName
	 *            - ��� ����� ��������� ��������
	 * @return
	 */
	public ResourceBundle getCustomProp(String customPropName) {
		String resource = customPropName + ".properties";
		getCustomBundle();

		for (String loca : allCustomList) {
			if (resource.equals(loca)) {
				ResourceBundle resourceMap = customPropMap.get(customPropName);
				return resourceMap;
			}
		}

		try {
			throw new NoCustomPropertiesException();
		} catch (NoCustomPropertiesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	public String getDefaultLocale() {
		return defaultLocale;
	}

	/**
	 * 
	 * @return
	 */
	public PropertyResourceBundle getLocalesProperties() {

		return locales;
	}

	public HashMap<String, HashMap<String, ResourceBundle>> getLocaleMap() {

		return localeMap;
	}

	public HashMap<String, ResourceBundle> getCustomPropMap() {
		// getCustomBundle();
		return customPropMap;
	}

}
