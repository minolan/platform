package com.bplatform.server.features;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;

import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.HibernateFacade;
import com.bplatform.server.DAO.models.BpPermission;
import com.bplatform.server.DAO.models.BpPermissionGroup;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.BpUserGroup;
import com.bplatform.server.DAO.models.BpWorkflowNode;
import com.bplatform.server.DAO.models.BpWorkflowScheme;
import com.bplatform.server.DAO.models.JoinedBpUserToBpPermission;
import com.bplatform.server.DAO.models.MockDocument;
import com.bplatform.server.security.BpAuthentication;
import com.bplatform.server.security.BpAuthenticationProvider;
import com.bplatform.shared.exceptions.BpProcessExecutionException;

@Model
public class DebugFeatures {
	
	@Inject
	private MainFacade appFacade;
	
	/**
	 * Edit for your own purposes
	 */
	public void doPrepareDebug() {
		prepareDB();
		//testAPI();
		//testAPI2();
		login();
//		testEncryption();
		testWF();
		//testFetching();
	}
	
	/**
	 * For DB logic
	 */
	private void prepareDB(){
		String login = "admin";
		String password = login;
		String email = "admin@admin.com";
		PasswordEncoder encoder = new Md5PasswordEncoder();
		System.out.println();
		BpUser admin = new BpUser(login, encoder.encodePassword(password, email), email, "Admin Adminych");
		BpUserGroup rootFolder = new BpUserGroup("Root", null);
		try{
			appFacade.getHibernateFacade().getBpUserAPI().saveOrUpdate(admin);
			appFacade.getHibernateFacade().getBpUserGroupAPI().save(rootFolder);
			admin.addBpUserGroup(rootFolder);
			rootFolder.addBpUser(admin);
			//appFacade.getHibernateFacade().getBpTokenAPI().save(new BpToken(admin));
			//appFacade.getHibernateFacade().getBpTokenAPI().save(new BpToken(admin));
			//appFacade.getHibernateFacade().getBpUserAPI().saveOrUpdate(admin2);
			appFacade.getHibernateFacade().getBpUserAPI().saveOrUpdate(admin);
			appFacade.getHibernateFacade().getBpUserAPI().saveOrUpdate(admin);
		}catch(ConstraintViolationException e){
			System.out.println("Well done: exception was caught!");
		}
		
	}
// 	private void prepareDB() {
//		BpUser admin = new BpUser("admin","admin", "admin@admin.com", "Admin Adminych");
//		BpUser user = new BpUser("admin", "user", "user", "user");
//		appFacade.getBpUserAPI().saveOrUpdate(admin); 
//		try{
//			appFacade.getBpUserAPI().saveOrUpdate(user);
//		}catch(ConstraintViolationException e){
//			System.out.println("Well done: exception was caught!");
//		}
//		
//		user = new BpUser(null, "user", "user", "user");
//		try{
//			appFacade.getBpUserAPI().saveOrUpdate(user);
//		}catch(ConstraintViolationException e){
//			System.out.println("Well done: column 'login' cannot be null");
//		}
//		if(null == appFacade.getBpUserAPI().getByLogin("blablabla")){
//			System.out.println("Well done: user with this name doesn't exists");
//		}
//	}
	
	
// 	private void prepareDB2() {
//		BpUser admin = new BpUser("admin","admin", "admin@admin.com", "Admin Adminych");
//		BpUser user = new BpUser("admin", "user", "user", "user");
//		appFacade.getBpUserAPI().saveOrUpdate(admin); 
//		try{
//			appFacade.getBpUserAPI().saveOrUpdate(user);
//		}catch(ConstraintViolationException e){
//			System.out.println("Well done: exception was caught!");
//		}
//		
//		user = new BpUser(null, "user", "user", "user");
//		try{
//			appFacade.getBpUserAPI().saveOrUpdate(user);
//		}catch(ConstraintViolationException e){
//			System.out.println("Well done: column 'login' cannot be null");
//		}
//		if(null == appFacade.getBpUserAPI().getByLogin("blablabla")){
//			System.out.println("Well done: user with this name doesn't exists");
//		}
//	}
	
	/**
	 * Login user
	 */
	private void login() {
		BpUserDTO admin = new BpUserDTO();
		admin.setLogin("admin");
		appFacade.getSessionContext().setSessionUser(admin);
		appFacade.getSessionContext().setSessionUserModel(appFacade.getHibernateFacade().getBpUserAPI().getByLogin("admin"));
	}
	
	/**
	 * For testing DAO APIs
	 */
	private void testAPI(){
		HibernateFacade hibernateFacade = appFacade.getHibernateFacade();
		BpPermission permission = new BpPermission("permission 1");
		BpPermissionGroup permissionGroup = new BpPermissionGroup("group 1");
		hibernateFacade.getBpPermissionAPI().save(permission);
		hibernateFacade.getBpPermissionGroupAPI().save(permissionGroup);
		// test 1: permission group and permission were saved
		permission.addBpPermissionGroup(permissionGroup);
		permissionGroup.addBpPermission(permission);
		hibernateFacade.getBpPermissionAPI().update(permission);
		hibernateFacade.getBpPermissionGroupAPI().update(permissionGroup);
		//test 2: connection between them was updated
		hibernateFacade.getBpPermissionGroupAPI().delete(permissionGroup);
		//test 3: group record was removed, and in the m-to-n table also
		hibernateFacade.getBpPermissionAPI().delete(permission);
	}
	
	/**
	 * For testing relation between BpUser and BpPermission
	 */
	private void testAPI2(){
		HibernateFacade hibernateFacade = appFacade.getHibernateFacade();
		BpUser user = new BpUser("admin1", "admin1", "admin1@admin1.com", "Moguchiy Admin");
		hibernateFacade.getBpUserAPI().saveOrUpdate(user);
		BpPermission permission = new BpPermission("pesmission 2");
		hibernateFacade.getBpPermissionAPI().save(permission);
		JoinedBpUserToBpPermission joined = new JoinedBpUserToBpPermission(user, permission, true, false);
		hibernateFacade.getJoinedBpUserToBpPermissionAPI().saveOrUpdate(joined);
		//relation was added to DB
		hibernateFacade.getBpPermissionAPI().delete(permission);
		//permission and all connected relation were removed
		hibernateFacade.getBpUserAPI().delete(user);
		//permission and all connected relation were removed
	}
	
	private void testWF() {
		HibernateFacade hibernateFacade = appFacade.getHibernateFacade();
		Session hs = hibernateFacade.getHibernateSession();
		
		//create new workflow 1-2-3 1-3 3-1
		BpWorkflowScheme testWF = new BpWorkflowScheme();
		testWF.setSchemeName("TestWF");
		appFacade.getWorkflowFacade().getWfAPI().saveOrUpdateWorkflowScheme(testWF);
		hs.update(testWF);
		
		//hs.update(testWF);
		BpWorkflowNode n1 = new BpWorkflowNode();
		n1.setEnabled(true);
		n1.setEntryNode(true);
		n1.setStatusName("state 1");
		n1.setConditionsCheckClass("com.bplatform.server.features.workflow.checkers.BpWfNode1toNode2Checker");
		n1.setScheme(testWF);
		n1.setSuccessors(new HashSet<BpWorkflowNode>());
		n1.setAllowedFor(null);
				
		BpWorkflowNode n2 = new BpWorkflowNode();
		n2.setEnabled(true);
		n2.setEntryNode(true);
		n2.setStatusName("state 2");
		n2.setConditionsCheckClass("com.bplatform.server.features.workflow.checkers.BpWfNode1toNode2Checker");
		n2.setScheme(testWF);
		n2.setSuccessors(new HashSet<BpWorkflowNode>());
		n2.setAllowedFor(null);
		
		BpWorkflowNode n3 = new BpWorkflowNode();
		n3.setEnabled(true);
		n3.setEntryNode(true);
		n3.setStatusName("state 3");
		n3.setConditionsCheckClass("com.bplatform.server.features.workflow.checkers.BpWfNode1toNode2Checker");
		n3.setScheme(testWF);
		n3.setSuccessors(new HashSet<BpWorkflowNode>());
		n3.setAllowedFor(null);
		
		appFacade.getWorkflowFacade().getWfAPI().saveOrUpdateNode(n1);
		appFacade.getWorkflowFacade().getWfAPI().saveOrUpdateNode(n2);
		appFacade.getWorkflowFacade().getWfAPI().saveOrUpdateNode(n3);
		
		n1.getSuccessors().add(n2); //1-2
		n1.getSuccessors().add(n3); //1-3
		
		n2.getSuccessors().add(n3); //2-3
		
		n3.getSuccessors().add(n1); //3-1
		
		appFacade.getWorkflowFacade().getWfAPI().saveOrUpdateNode(n1);
		appFacade.getWorkflowFacade().getWfAPI().saveOrUpdateNode(n2);
		appFacade.getWorkflowFacade().getWfAPI().saveOrUpdateNode(n3);
		
		//TODO need to store persistent BpUser model in session, see the hell of the code in the next line
		MockDocument md = new MockDocument();
		md.setCreated_by(appFacade.getSessionContext().getBpUser());
		md.setSomeTestField("blah!");
		md.setState(n1);
		md.setBlockedBy(null);
		Transaction transaction = null;
		try {
			transaction = hs.getTransaction();
			transaction.begin();
			hs.saveOrUpdate(md);
			transaction.commit();
			hs.flush();
		} catch(ConstraintViolationException e){
			throw e; 
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
			}
		}  finally {
			hs.close();
		}
		
		
		HashMap<String,BpWorkflowNode> states = new HashMap<String,BpWorkflowNode>();
		MockDocument md2 = appFacade.getWorkflowFacade().getWfAPI().getBpStEntityById(md.getId(), md.getClass(), null);
		try {
			HashSet<BpWorkflowNode> tmp = (HashSet<BpWorkflowNode>) appFacade.getWorkflowFacade().getNextNodes(md2);
			for (BpWorkflowNode st:tmp) {
				states.put(st.getStatusName(), st);
			}
		} catch (BpProcessExecutionException e) {
			e.printStackTrace();
		}
		
		if (!states.isEmpty()&&states.containsKey(n2.getStatusName())) { 
			try {
				appFacade.getWorkflowFacade().changeState(n2, md, false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * For testing relation between BpUser and BpUserGroup
	 */
	private void testAPI3(){
		String resultOfTransaction = null;

		BpUser bpu;
		HibernateFacade hibFacade = appFacade.getHibernateFacade();
		
		Transaction transaction = null;
		Session hbSession = hibFacade.getHibernateSession();

		try {
			transaction = hbSession.getTransaction();
			transaction.begin();

			bpu = (BpUser) hbSession.getNamedQuery("getBpUserByLogin")
					.setString("login", "admin").uniqueResult();
			
			bpu.setEmail("test@test.com");
			bpu.setPassword("password");
			bpu.setFio("Admin Odminovich");
			
			Set<BpUserGroup> bpugSet = new HashSet<BpUserGroup>();
			
			BpUserGroup bpug = new BpUserGroup("Admin");
			bpugSet.add(bpug);
			
			bpu.setBpUserGroups(bpugSet);

			hbSession.saveOrUpdate(bpu);
			transaction.commit();
			resultOfTransaction = "Профиль пользователя успешно обновлен";
		} catch (Exception e) {
			e.printStackTrace();
			if (transaction != null) {
				transaction.rollback();
				resultOfTransaction = "Профиль пользователя не обновлен";
			}
		} finally {
			hbSession.close();
		}
	}
	
	private void testFetching() {

		HibernateFacade hibernateFacade = appFacade.getHibernateFacade();
		BpPermission permission = new BpPermission("permission 3");
		BpPermissionGroup permissionGroup = new BpPermissionGroup("group 2");
		permissionGroup.addBpPermission(permission);
		hibernateFacade.getBpPermissionAPI().save(permission);
		hibernateFacade.getBpPermissionGroupAPI().save(permissionGroup);
		BpUser user = new BpUser("admin2", "admin2", "admin1@admin1.com",
				"Moguchiy Admin");
		BpUserGroup rootFolder = new BpUserGroup("Root 2");
		rootFolder.addBpPermissionGroup(permissionGroup);
		rootFolder.addBpUser(user);
		JoinedBpUserToBpPermission joined = new JoinedBpUserToBpPermission(
				user, permission, true, false);
		// user.addJoinedPermission(joined);
		// permission.addJoinedUser(joined);
		hibernateFacade.getBpUserAPI().saveOrUpdate(user);
		hibernateFacade.getJoinedBpUserToBpPermissionAPI().saveOrUpdate(joined);
		hibernateFacade.getBpUserGroupAPI().save(rootFolder);

		System.out.println("---------------------");
		BpUserGroup loaded = hibernateFacade.getBpUserGroupAPI()
				.getGroupByName("Root 2");
		//LazyInitializationException
		//Set<BpUser> users = loaded.getUsers();
		/*for(BpUser tmpUser : users){
			System.out.println(tmpUser.getLogin());
		}*/
		System.out.println("----------REQUEST-----------");
		hibernateFacade.getBpUserGroupAPI().initializeBpPermissionGroups(loaded);
		System.out.println("----------REQUEST2----------");
		hibernateFacade.getBpUserAPI().initializeJoinedBpPermissions(user);
		for(JoinedBpUserToBpPermission tmpJoined : user.getJoinedPermissions()){
			System.out.println(tmpJoined.getBpUser().toString());
		}
		System.out.println("----------UG-----------");
		hibernateFacade.getHibernateSession().refresh(loaded);
		System.out.println("---------------------");
		
	}
	
}
