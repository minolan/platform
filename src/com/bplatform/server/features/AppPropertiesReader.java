package com.bplatform.server.features;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AppPropertiesReader {
	
	/**
	 * Absolute path to application war (included)
	 * @return
	 */
	public String getRootPath() {
		return System.getProperty("user.dir");
	}
	
	/**
	 * If debug mode - return true
	 * @return
	 */
	public boolean isDebugMode() {
		//TODO use Properties facade
		return true;
	}

}
