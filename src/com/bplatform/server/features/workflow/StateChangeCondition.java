package com.bplatform.server.features.workflow;

import javax.enterprise.inject.Model;

import org.hibernate.Session;

import com.bplatform.server.DAO.models.BpStatefulEntity;

@Model
public interface StateChangeCondition {
	
	/**
	 * Use the session provided as the parameter. Do NOT make any changes here!
	 * @param thisEntity
	 * @param inThisState
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public <E extends BpStatefulEntity> boolean allowedToBe(E thisEntity, Session session) throws Exception;
	
}
	