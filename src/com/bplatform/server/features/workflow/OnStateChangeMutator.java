package com.bplatform.server.features.workflow;

import javax.enterprise.inject.Model;

import org.hibernate.Session;

import com.bplatform.server.DAO.models.BpStatefulEntity;

@Model
public interface OnStateChangeMutator {
	
	/**
	 * Use the session provided as the parameter. Do NOT commit any changes here!
	 * @param thisEntity
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public <E extends BpStatefulEntity> E onChange(E thisEntity, Session session) throws Exception;

}
