package com.bplatform.server.features;

import javax.enterprise.context.ApplicationScoped;

/**
 * API to work with audited entities.
 * @author Minolan
 */
@ApplicationScoped
public class HistoryFacade {

}
