package com.bplatform.server.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.bplatform.server.util.AttachManager;
import com.bplatform.shared.constants.PlatformServerConstants;

public class UploadFileServlet extends HttpServlet {
	private static final long serialVersionUID = -4241148173486105025L;
	private static final Long MAX_LENGHT = 10000000L;

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {
			if (req.getContentLength() > MAX_LENGHT) {
				resp.getWriter().write(PlatformServerConstants.EXCEPTION.FILE_IS_TO_LARGE);
				return;
			}

			String fileName = null;
			byte[] content = null;

			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			List<FileItem> items = upload.parseRequest(req);

			for (FileItem item : items) {
				if (PlatformServerConstants.ATTACHINFO.UPLOAD.equals(item.getFieldName())) {
					content = item.get();
				}

				if (content.length > MAX_LENGHT) {
					throw new Exception(PlatformServerConstants.EXCEPTION.FILE_IS_TO_LARGE);
				}

				if (null != item.getName()) {
					String[] split = item.getName().split("[\\\\/]");
					if (split.length > 0) {
						fileName = split[split.length - 1];
						fileName = fileName.replace(" ", "_");
					} else {
						fileName = "undefined.tmp";
					}
				}

			}
/*
 * Save to ...
 */
			AttachManager.saveAttach(content, fileName);

		} catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
