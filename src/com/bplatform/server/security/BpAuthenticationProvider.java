package com.bplatform.server.security;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.ValidationException;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.bplatform.server.MainFacade;
import com.bplatform.server.DAO.APIs.BpUserAPI;
import com.bplatform.server.DAO.models.BpPermission;
import com.bplatform.server.DAO.models.BpUser;
import com.bplatform.server.DAO.models.JoinedBpUserToBpPermission;
import com.bplatform.shared.exceptions.BpAuthenticationException;
import com.bplatform.shared.exceptions.BpAuthenticationException.Reason;
import com.bplatform.shared.validators.FieldVerifier;


public class BpAuthenticationProvider implements AuthenticationProvider {

	@Inject
	private MainFacade appFacade;
	
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		String username = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();
		
		//TODO internationalize exceptions
		if(username == null ){
			throw new UsernameNotFoundException("Username must be set");
		}
		if(password == null){
			throw new BadCredentialsException("Password must be set");
		}
		
		username = escapeHtml(username);
		password = escapeHtml(password);
		if(!FieldVerifier.isValidLogin(username)){
			throw new ValidationException("Username must be valid");
		}
		if(!FieldVerifier.isValidPassword(password)){
			throw new ValidationException("Password must be valid");
		}
		
		BpUserAPI api = appFacade.getHibernateFacade().getBpUserAPI();
		BpUser bpUser = api.getByLogin(username); 
		if(bpUser == null){
			throw new UsernameNotFoundException("User not found");
		}
	
		PasswordEncoder encoder = new Md5PasswordEncoder();
		if(!encoder.isPasswordValid(bpUser.getPassword(), password, bpUser.getEmail())){
			throw new BadCredentialsException("Invalid password");
		}
		
		Set<JoinedBpUserToBpPermission> joinedPermissions = bpUser.getJoinedPermissions();
		HashSet<BpPermission> permissions = new HashSet<BpPermission>();
		for(JoinedBpUserToBpPermission joined : joinedPermissions){
			if(joined.isPermitted()){
				permissions.add(joined.getBpPermission());
			}
		}
		
		//new BpAuthentication(permissions, authentication, "", true, true, username, password);
		appFacade.getSessionContext().setSessionUserModel(bpUser);
		
		return null;
	}

	//TODO may be improvement will be required
	@Override
	public boolean supports(Class<?> authentication) {	
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
	
	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}
	

}
