package com.bplatform.server.security;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.Authentication;

import com.bplatform.server.DAO.models.BpPermission;

/**
 * 
 * @author Anastasia Smakovska
 *
 * 18 ���. 2013
 */
public class BpAuthentication implements Authentication{

	private static final long serialVersionUID =-3488695958637392251L;
	
	private HashSet<BpPermission> grantedAuthorities;
	private Authentication authentication;
	
	// Some useless fields for future probable storing or tracking authentication
	//private String name;
	
	// If different ways of authentication is implemented, field will store exact way
	private String details;
	
	// Required field for Spring Security architecture, but in case of bPlatform 							  
	private boolean authenticated = false;
	
	// Is user was authenticated using remember-me services or not
	private boolean fullyAuthenticated = false;
	
	// BpUser login
	private String principal;
	
	// BpUser password
	private String credentials;
	
	public BpAuthentication(HashSet<BpPermission> grantedAuthorities,
			Authentication authentication, String details,
			boolean authenticated, boolean fullyAuthenticated,
			String principal, String credentials) {
		super();
		this.grantedAuthorities = grantedAuthorities;
		this.authentication = authentication;
		this.details = details;
		this.authenticated = authenticated;
		this.fullyAuthenticated = fullyAuthenticated;
		this.principal = principal;
		this.credentials = credentials;
	}

	@Override
	public String getName() {
		//return name;
		return this.getClass().getSimpleName();
	}

	@Override
	public Collection<BpPermission> getAuthorities() {
		return grantedAuthorities;
	}

	@Override
	public String getCredentials() {
		return credentials;
	}

	@Override
	public String getDetails() {
		return details;
	}

	@Override
	public String getPrincipal() {
		return principal;
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	@Override
	public void setAuthenticated(boolean authenticated){
		this.authenticated = authenticated; 
	}

	public boolean isFullyAuthenticated(){
		return fullyAuthenticated;
	}

	public void setGrantedAuthorities(HashSet<BpPermission> grantedAuthorities) {
		this.grantedAuthorities = grantedAuthorities;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setFullyAuthenticated(boolean fullyAuthenticated) {
		this.fullyAuthenticated = fullyAuthenticated;
	}
	
}
