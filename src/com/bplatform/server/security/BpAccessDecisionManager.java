package com.bplatform.server.security;

import java.util.Collection;

import javax.inject.Inject;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;

import com.bplatform.server.DAO.HibernateFacade;
 
/**
  * 
  * @author Anastasia Smakovska
  *
  * 17 ���. 2013
  */
public class BpAccessDecisionManager implements AccessDecisionManager {

	
	@Inject HibernateFacade hibFacade;
	
	
	@Override
	public void decide(Authentication bpAuthentication, Object arg1,
			Collection<ConfigAttribute> arg2) throws AccessDeniedException,
			InsufficientAuthenticationException {
		
		
	}

	@Override
	public boolean supports(ConfigAttribute arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
