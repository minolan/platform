package com.bplatform.server.user;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.bplatform.client.bpShared.DTO.BpUserDTO;
import com.bplatform.server.DAO.HibernateSession;
import com.bplatform.server.DAO.models.BpUser;

@Named
@SessionScoped
public class SessionContext implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HttpSession httpSession;
	
	/**
	 * Current http session ID
	 */
	private String sessionId;
	
	/**
	 * platform user associated this current session
	 */
	private BpUser bpUser;
	
	/**
	 * platform userDTO for sending information to client
	 */
	private BpUserDTO bpUserDTO;
	
	public HttpSession getHttpSession() {
		return httpSession;
	}

	public BpUser getBpUser() {
		return bpUser;
	}

	public BpUserDTO getBpUserDTO() {
		return bpUserDTO;
	}

	public Map<String, HibernateSession> getSessions() {
		return sessions;
	}

	/**
	 * user created hib.sessions
	 */
	private Map<String, HibernateSession> sessions = new ConcurrentHashMap<String, HibernateSession>();
	
	public void addHibernateSession(HibernateSession hSession) {
		sessions.put(hSession.getHibernateSessionId(), hSession);
		System.out.println("Hibernate session added to user session scope");
	}
	
	public void setSessionUser(BpUserDTO bpUserDTO) {
		this.bpUserDTO = bpUserDTO;
	}
	
	public void setSessionUserModel(BpUser bpUser) {
		this.bpUser = bpUser;
		System.out.println("User set in session");
	}
	
	public BpUserDTO getSessionUser() {
		return this.bpUserDTO;
	}
	
	public SessionContext() {}
	
	/**
	 * Used to inject a HttpSession (created automatically if not exist) in SessionScoped bean.
	 * @param httpSession
	 */
	@Inject
	public SessionContext(HttpSession httpSession) {
        super();
        this.httpSession = httpSession;
        //System.out.println(this.httpSession.getId()); 
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
}
